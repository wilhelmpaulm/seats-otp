-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2015 at 06:44 AM
-- Server version: 5.6.15-log
-- PHP Version: 5.6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `seats`
--

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE IF NOT EXISTS `dashboard` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(45) DEFAULT 'pending' COMMENT 'type \n- restaurant\n- mood\n- location\n- promo',
  `type` varchar(45) DEFAULT 'restaurant',
  `order` int(11) DEFAULT NULL,
  `id_raw_restaurant` text,
  `moods` longtext,
  `promo_title` text,
  `promo_description` text,
  `promo_location` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(45) DEFAULT 'active',
  `type` varchar(45) DEFAULT 'favorite',
  `id_raw_user` text,
  `id_raw_restaurant` text,
  `id_user` int(11) DEFAULT NULL,
  `comments` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(45) DEFAULT 'pending',
  `type` varchar(45) DEFAULT 'feedback',
  `id_raw_user` text,
  `id_raw_reservation` text,
  `id_raw_restaurant` text,
  `id_user` int(11) DEFAULT NULL,
  `taste` double DEFAULT '0',
  `ambiance` double DEFAULT '0',
  `value` double DEFAULT '0',
  `cleanliness` double DEFAULT '0',
  `service` double DEFAULT '0',
  `general` double DEFAULT '0',
  `comments` longtext,
  `review` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE IF NOT EXISTS `template` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(45) DEFAULT 'active',
  `type` varchar(45) DEFAULT 'user',
  `id_raw_user` text,
  `id_raw_user_temp` text,
  `code` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `designation` varchar(45) DEFAULT NULL,
  `email` text,
  `mobile` varchar(45) DEFAULT NULL,
  `birthdate` datetime DEFAULT '1960-01-01 00:00:00',
  `gender` varchar(45) DEFAULT NULL,
  `password` text,
  `verified` varchar(45) DEFAULT 'false',
  `points` double DEFAULT '0',
  `picture` text,
  `fb_id` text,
  `fb_link` text
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `created_at`, `updated_at`, `status`, `type`, `id_raw_user`, `id_raw_user_temp`, `code`, `first_name`, `middle_name`, `last_name`, `designation`, `email`, `mobile`, `birthdate`, `gender`, `password`, `verified`, `points`, `picture`, `fb_id`, `fb_link`) VALUES
(2, '2015-08-10 07:28:13', '2015-08-10 07:33:19', 'active', 'user', '55c85402e4b0dab9ae4da428', '55c85478e4b0dab9ae4da429', '4797', NULL, NULL, NULL, NULL, NULL, '+639279655572', NULL, NULL, NULL, 'false', 0, NULL, NULL, NULL),
(3, '2015-08-11 04:19:48', '2015-08-11 04:19:50', 'active', 'user', '55c978b7e4b0dab9ae4da42a', '55c978b7e4b0dab9ae4da42a', NULL, 'lolamama', NULL, 'fransfurt', NULL, 'shang@gmail.com', '+631212121', NULL, NULL, NULL, 'false', 0, NULL, NULL, NULL),
(4, '2015-08-11 04:21:36', '2015-08-11 04:44:00', 'active', 'user', '55c97953e4b0dab9ae4da42d', '55c97953e4b0dab9ae4da42d', '5531', 'bboo', NULL, 'lala', NULL, NULL, '+631254541', NULL, NULL, NULL, 'false', 0, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
