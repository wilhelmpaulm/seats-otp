<div id="_db" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">DATABASE</label>
        <div class="well">
            <p>notes on the new database design</p>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                <a href="http://52.74.54.234:7777/public/db/seats-db.pdf">{URL}/public/db/seats-db.pdf</a><br/>
                <a href="http://52.74.54.234:7777/public/db/seats-db.sql">{URL}/public/db/seats-db.sql</a><br/>
            </code>
        </div>
        <label>NOTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>table names are plural</td>
                    </tr>
                    <tr>
                        <td>id = int , the new primary key</td>
                    </tr>
                    <tr>
                        <td>id_raw = text , temporary for migration purposes</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">

        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-12">
        <div class="">
            <label>restaurant_details</label>
            <div class="">
                <table class="table table-bordered table-striped well">
                    <thead>
                        <tr>
                            <td>column</td>
                            <td>type</td>
                            <td>options</td>
                            <td>notes</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>type</td>
                            <td><small>string</small></td>
                            <td>
                                <small>
                                    payment_options, cuisines, additional_details, tags, occasions, images, specialties
                                </small>
                            </td>
                            <td>
                                <small>

                                </small>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <label>promotion_details</label>
            <div class="">
                <table class="table table-bordered table-striped well">
                    <thead>
                        <tr>
                            <td>column</td>
                            <td>type</td>
                            <td>options</td>
                            <td>notes</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>type</td>
                            <td><small>string</small></td>
                            <td>
                                <small>
                                    day, blocked_dates
                                </small>
                            </td>
                            <td>
                                <small>
                                    
                                </small>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>