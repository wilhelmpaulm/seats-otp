<div id="_ratings" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET ALL RATINGS</label>
        <div class="well">
            <p>get them ratings</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/ratings
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>ratings are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>user does not exist</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "1",
                "created_at": "2015-08-12 03:47:41",
                "updated_at": null,
                "status": "pending",
                "type": "feedback",
                "id_raw_user": "55c85402e4b0dab9ae4da428",
                "id_raw_reservation": "00000022222",
                "id_raw_restaurant": "000001111",
                "id_user": "2",
                "taste": "2",
                "ambiance": null,
                "value": "4",
                "cleanliness": "5",
                "service": "2",
                "general": "3",
                "comments": "comment",
                "review": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET ALL RESTAURANT RATINGS</label>
        <div class="well">
            <p>get restaurant ratings</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/restaurants/{restaurantId}/ratings
                <hr/>
                {URL}/restaurants/550a202ee4b0f1bb4378c14d/ratings
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>ratings are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>restaurant does not exist</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "1",
                "created_at": "2015-08-12 03:47:41",
                "updated_at": null,
                "status": "pending",
                "type": "feedback",
                "id_raw_user": "55c85402e4b0dab9ae4da428",
                "id_raw_reservation": "00000022222",
                "id_raw_restaurant": "000001111",
                "id_user": "2",
                "taste": "2",
                "ambiance": null,
                "value": "4",
                "cleanliness": "5",
                "service": "2",
                "general": "3",
                "comments": "comment",
                "review": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET ALL RESTAURANT RATINGS</label>
        <div class="well">
            <p>get the average of a restaurant's ratings</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/restaurants/{restaurantId}/ratings/sum
                <hr/>
                {URL}/restaurants/54c9a287e4b006e8e6ce9a7a/ratings/sum
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>average of ratings is</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>restaurant does not exist</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id_raw_restaurant": "54c9a287e4b006e8e6ce9a7a",
                "taste": 2,
                "ambiance": 4,
                "value": 2,
                "cleanliness": 5,
                "service": 2,
                "general": 3
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET USER RATINGS</label>
        <div class="well">
            <p>get a single user's ratings ratings</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/ratings
                <hr/>
                {URL}/users/+639279655572/ratings
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>user's ratings are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            [
                {
                    "id": "1",
                    "created_at": "2015-08-12 10:17:21",
                    "updated_at": null,
                    "status": "pending",
                    "type": "feedback",
                    "id_raw_user": "55c85402e4b0dab9ae4da428",
                    "id_raw_reservation": "00000022222",
                    "id_raw_restaurant": "000001111 ",
                    "id_user": "2",
                    "taste": "2",
                    "ambiance": null,
                    "value": "4",
                    "cleanliness": "5",
                    "service": "2",
                    "general": "3",
                    "comments": "comment",
                    "review": null
                }
            ]
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">ADD RATING</label>
        <div class="well">
            <p>add ratings</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/ratings/add
                <hr/>
                {URL}/users/+639279655572/ratings/add?id_raw_restaurant=000001111
                &id_raw_reservation=00000022222&taste=2&=1&value=4
                &cleanliness=5&service=2&general=3&comments=comment
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>id_raw_restaurant</td>
                        <td><small>string</small></td>
                        <td><code>000001111</code></td>
                    </tr>
                    <tr>
                        <td>id_raw_reservation</td>
                        <td><small>string</small></td>
                        <td><code>00000022222</code></td>
                    </tr>
                    <tr>
                        <td>taste</td>
                        <td><small>int</small></td>
                        <td><code>1</code> to <code>5</code></td>
                    </tr>
                    <tr>
                        <td>value</td>
                        <td><small>int</small></td>
                        <td><code>1</code> to <code>5</code></td>
                    </tr>
                    <tr>
                        <td>cleanliness</td>
                        <td><small>int</small></td>
                        <td><code>1</code> to <code>5</code></td>
                    </tr>
                    <tr>
                        <td>service</td>
                        <td><small>int</small></td>
                        <td><code>1</code> to <code>5</code></td>
                    </tr>
                    <tr>
                        <td>general</td>
                        <td><small>int</small></td>
                        <td><code>1</code> to <code>5</code></td>
                    </tr>
                    <tr>
                        <td>comments</td>
                        <td><small>string</small></td>
                        <td><code>comment</code></td>
                    </tr>

                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>rating is returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>action failed</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "2",
                "created_at": "2015-08-12 03:50:14",
                "updated_at": null,
                "status": "pending",
                "type": "feedback",
                "id_raw_user": "55c85402e4b0dab9ae4da428",
                "id_raw_reservation": "00000022222",
                "id_raw_restaurant": "000001111",
                "id_user": "2",
                "taste": "2",
                "ambiance": null,
                "value": "4",
                "cleanliness": "5",
                "service": "2",
                "general": "3",
                "comments": "comment",
                "review": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message: : "action failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>