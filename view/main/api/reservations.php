<div id="_reservations" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">USER RESERVATION HISTORY</label>
        <div class="well">
            <p>get user reservation history</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/reservations
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>success</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>user does not exist</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div  >
                <small>
                    <pre class="well" >
{
    "response": {
        "status": 0,
        "startRow": 0,
        "endRow": 12,
        "totalRows": 13,
        "data": [
            {...},
            {...},
            {...},
            {
                "id": "55c9c850e4b0dab9ae4da4c8",
                "createdBy": null,
                "createdDate": 1439287376205,
                "lastModifiedBy": null,
                "lastModifiedDate": 1439287967294,
                "version": 7,
                "dinerId": "55c99d0ee4b0dab9ae4da42f",
                "diner": {
                    "id": "55c99d0ee4b0dab9ae4da42f",
                    "createdBy": null,
                    "createdDate": 1439276302822,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1439287376149,
                    "version": 23,
                    "firstName": "wilhelm",
                    "lastName": "martinez",
                    "companyName": null,
                    "position": null,
                    "restaurantId": "550a202ee4b0f1bb4378c14d",
                    "webUserId": "55c99d0ee4b0dab9ae4da42e",
                    "birthday": null,
                    "anniversary": null,
                    "note": null,
                    "phones": [
                        {
                            "label": "Mobile",
                            "value": "9279655572",
                            "countryCode": "+63",
                            "isPrimary": true
                        }
                    ],
                    "emails": [
                        {
                            "label": "Email",
                            "value": "wilhelmpaulm@gmail.com",
                            "isPrimary": true
                        }
                    ],
                    "addresses": null,
                    "tagIds": null,
                    "relationships": null,
                    "allergies": null,
                    "salutation": null,
                    "lastVisitDateTime": null,
                    "primaryMobileNumber": "+639279655572",
                    "groupId": "54eeb59be4b0dcdb5bfd4448",
                    "otherRestaurantIds": [],
                    "zipCode": null,
                    "status": "active",
                    "walkin": false,
                    "newsletter": false
                },
                "timeSlotId": 1930,
                "note": "",
                "tagIds": null,
                "tables": null,
                "date": 1439251200000,
                "mode": 2,
                "numberOfSeats": 2,
                "status": 4,
                "templateSlotId": null,
                "overrideTemplateSlotId": "55c9c850e4b0dab9ae4da4c7",
                "seatedTime": null,
                "completedTime": null,
                "waitingCreatedTime": null,
                "restaurantId": "550a202ee4b0f1bb4378c14d",
                "restaurant": {
                    "id": "550a202ee4b0f1bb4378c14d",
                    "createdBy": null,
                    "createdDate": 1426726958538,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1429696144131,
                    "version": 43,
                    "name": "Edsa Beverage Design Studio",
                    "localName": null,
                    "dressCode": 1,
                    "diningStyle": 1,
                    "cuisines": null,
                    "neighborhood": "Greenhills",
                    "crossStreet": null,
                    "menu": null,
                    "price": "500",
                    "website": "http://www.edsa-bdg.com/",
                    "hoursOfOperation": "MON-SAT 8am - 12mn SUN 10am - 7pmBreakfast served at 8am Cocktail bar opens MON-SAT 4:30pm",
                    "paymentOptions": [
                        1,
                        2,
                        3
                    ],
                    "acceptWalkins": null,
                    "additionalInfo": null,
                    "publicTransit": null,
                    "parking": null,
                    "parkingDetails": "Parking lot",
                    "catering": null,
                    "specialEventAndPromotions": null,
                    "address": "Clmc Building, 209 Edsa Barangay Wack Wack, Mandaluyong 1550 Metro Manila, Philippines",
                    "localAddress": null,
                    "city": "Mandaluyong",
                    "localCity": null,
                    "country": "PH",
                    "postalCode": "56",
                    "specialties": "All day breakfast available. Food munu is designed to compliment our drinks. Hand crafted sodas from scratch. #YKW Roasters Specialty Coffee. Craft beer. Better Common Cocktails",
                    "chef": "",
                    "description": "<strong>EDSA Beverage Design Group pioneers the pursuit of the noble beverage.<br /><br /></strong>EDSA Beverage Design Group (EDSA BDG) pioneers the pursuit of the noble beverage. The group develops coffee, cocktails, sodas, and beer using the best and freshest ingredients available in the market &mdash; no compromises &mdash; resulting in only the best-tasting array of liquid cuisine. These beverages are designed, refined and showcased at EDSA Beverage Design Studio. EDSA BDG offers fully curated beverage programs for both established and start-up businesses.",
                    "phone": "26319035",
                    "email": "drinkwithus@edsa-bdg.com",
                    "latitude": 14.595923,
                    "longitude": 121.058854,
                    "images": [
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner1_20150422_6142.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner11_20150422_7403.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner2_20150422_9007.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner10_20150422_4661.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner4_20150422_3933.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner9_20150422_2975.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner8_20150422_3479.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner7_20150422_3673.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner5_20150422_8007.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner6_20150422_1301.jpg"
                    ],
                    "publicTransport": "",
                    "corkageDetails": "",
                    "eventFacilities": "Private rooms available",
                    "additionalDetails": [
                        5,
                        20,
                        8,
                        10,
                        15
                    ],
                    "fullPhoneNumber": "+63 26319035",
                    "featured": true,
                    "featuredDate": null,
                    "gmtMinutesOffset": -480,
                    "groupId": "54eeb59be4b0dcdb5bfd4448",
                    "planType": "covers",
                    "reservationEmail": null,
                    "facebookImage": null,
                    "tzid": "Asia/Manila",
                    "hoiioNumber": null,
                    "ipadAppVersion": null,
                    "logoUrl": null,
                    "chefRestaurant": null,
                    "config": null,
                    "listingUrl": null,
                    "replyTo": null,
                    "checkSize": null,
                    "restaurantSlug": "edsa-beverage-design-studio",
                    "ccCheckStartTimestamp": null,
                    "ccCheckEndTimestamp": null,
                    "ccCheckCovers": null,
                    "ccCheckCoversMessage": null,
                    "premiumbooking": null,
                    "accessPoints": null,
                    "street": "",
                    "thumbnail": "https://tabledb-seats.s3.amazonaws.com/seats/rest/thumb_550a202ee4b0f1bb4378c14d.jpg",
                    "enableNewletter": false,
                    "disabledNoPromotion": false,
                    "gmtminutesOffset": -480
                },
                "restaurantReservationNote": null,
                "floorplanId": null,
                "configId": "558407c1e4b030d36bf049b8",
                "shiftId": 5,
                "quotedTime": null,
                "campaignId": "organic",
                "pending": null,
                "verificationKey": "10e2bc17-e505-4031-95c1-c71dbeb5111d",
                "widgetId": "54728d3355bda4ff32479560",
                "widget": {
                    "id": "54728d3355bda4ff32479560",
                    "createdBy": null,
                    "createdDate": null,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1427167911407,
                    "version": 4,
                    "domainName": "https://www.facebook.com/seatsph",
                    "restaurantId": "",
                    "vendorType": "SINGLE",
                    "chargeable": true,
                    "description": "Seats.com.ph",
                    "partnerCode": "seats"
                },
                "resetWaitingCreatedTime": null,
                "resetCompletedTime": null,
                "resetSeatedTime": null,
                "hasSlotChanged": false,
                "modifyingDiner": false,
                "statusChanged": false,
                "spendingAmount": null,
                "turnoverTime": null,
                "promotion": null,
                "promotionId": null,
                "adultSeats": 1,
                "childSeats": 1,
                "flexDayConfigId": null,
                "flexTableIds": null,
                "flexIsJoinedTable": null,
                "messageSent": null,
                "emailSent": null,
                "paymentTransactionId": null,
                "staffName": null,
                "staffId": null,
                "originalBookedDate": null,
                "originalTimeslot": null,
                "historyTimeNotes": null,
                "waitingList": false
            }
        ],
        "totalPages": null,
        "exception": null
    }
}
                    </pre>
                </small>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "user does not exist"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET USER RESERVATION</label>
        <div class="well">
            <p>get user single reservation</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/reservations/{reservationId}
                <hr/>
                {URL}/users/+639279655572/reservations/55c9c850e4b0dab9ae4da4c8
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>success</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div >
                <small>
                    <pre class="well" >
{
    "response": {
        "status": 0,
        "startRow": 0,
        "endRow": 12,
        "totalRows": 13,
        "data": [
            {
                "id": "55c9c850e4b0dab9ae4da4c8",
                "createdBy": null,
                "createdDate": 1439287376205,
                "lastModifiedBy": null,
                "lastModifiedDate": 1439287967294,
                "version": 7,
                "dinerId": "55c99d0ee4b0dab9ae4da42f",
                "diner": {
                    "id": "55c99d0ee4b0dab9ae4da42f",
                    "createdBy": null,
                    "createdDate": 1439276302822,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1439287376149,
                    "version": 23,
                    "firstName": "wilhelm",
                    "lastName": "martinez",
                    "companyName": null,
                    "position": null,
                    "restaurantId": "550a202ee4b0f1bb4378c14d",
                    "webUserId": "55c99d0ee4b0dab9ae4da42e",
                    "birthday": null,
                    "anniversary": null,
                    "note": null,
                    "phones": [
                        {
                            "label": "Mobile",
                            "value": "9279655572",
                            "countryCode": "+63",
                            "isPrimary": true
                        }
                    ],
                    "emails": [
                        {
                            "label": "Email",
                            "value": "wilhelmpaulm@gmail.com",
                            "isPrimary": true
                        }
                    ],
                    "addresses": null,
                    "tagIds": null,
                    "relationships": null,
                    "allergies": null,
                    "salutation": null,
                    "lastVisitDateTime": null,
                    "primaryMobileNumber": "+639279655572",
                    "groupId": "54eeb59be4b0dcdb5bfd4448",
                    "otherRestaurantIds": [],
                    "zipCode": null,
                    "status": "active",
                    "walkin": false,
                    "newsletter": false
                },
                "timeSlotId": 1930,
                "note": "",
                "tagIds": null,
                "tables": null,
                "date": 1439251200000,
                "mode": 2,
                "numberOfSeats": 2,
                "status": 4,
                "templateSlotId": null,
                "overrideTemplateSlotId": "55c9c850e4b0dab9ae4da4c7",
                "seatedTime": null,
                "completedTime": null,
                "waitingCreatedTime": null,
                "restaurantId": "550a202ee4b0f1bb4378c14d",
                "restaurant": {
                    "id": "550a202ee4b0f1bb4378c14d",
                    "createdBy": null,
                    "createdDate": 1426726958538,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1429696144131,
                    "version": 43,
                    "name": "Edsa Beverage Design Studio",
                    "localName": null,
                    "dressCode": 1,
                    "diningStyle": 1,
                    "cuisines": null,
                    "neighborhood": "Greenhills",
                    "crossStreet": null,
                    "menu": null,
                    "price": "500",
                    "website": "http://www.edsa-bdg.com/",
                    "hoursOfOperation": "MON-SAT 8am - 12mn SUN 10am - 7pmBreakfast served at 8am Cocktail bar opens MON-SAT 4:30pm",
                    "paymentOptions": [
                        1,
                        2,
                        3
                    ],
                    "acceptWalkins": null,
                    "additionalInfo": null,
                    "publicTransit": null,
                    "parking": null,
                    "parkingDetails": "Parking lot",
                    "catering": null,
                    "specialEventAndPromotions": null,
                    "address": "Clmc Building, 209 Edsa Barangay Wack Wack, Mandaluyong 1550 Metro Manila, Philippines",
                    "localAddress": null,
                    "city": "Mandaluyong",
                    "localCity": null,
                    "country": "PH",
                    "postalCode": "56",
                    "specialties": "All day breakfast available. Food munu is designed to compliment our drinks. Hand crafted sodas from scratch. #YKW Roasters Specialty Coffee. Craft beer. Better Common Cocktails",
                    "chef": "",
                    "description": "<strong>EDSA Beverage Design Group pioneers the pursuit of the noble beverage.<br /><br /></strong>EDSA Beverage Design Group (EDSA BDG) pioneers the pursuit of the noble beverage. The group develops coffee, cocktails, sodas, and beer using the best and freshest ingredients available in the market &mdash; no compromises &mdash; resulting in only the best-tasting array of liquid cuisine. These beverages are designed, refined and showcased at EDSA Beverage Design Studio. EDSA BDG offers fully curated beverage programs for both established and start-up businesses.",
                    "phone": "26319035",
                    "email": "drinkwithus@edsa-bdg.com",
                    "latitude": 14.595923,
                    "longitude": 121.058854,
                    "images": [
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner1_20150422_6142.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner11_20150422_7403.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner2_20150422_9007.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner10_20150422_4661.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner4_20150422_3933.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner9_20150422_2975.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner8_20150422_3479.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner7_20150422_3673.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner5_20150422_8007.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/550a202ee4b0f1bb4378c14d/ebdgbanner6_20150422_1301.jpg"
                    ],
                    "publicTransport": "",
                    "corkageDetails": "",
                    "eventFacilities": "Private rooms available",
                    "additionalDetails": [
                        5,
                        20,
                        8,
                        10,
                        15
                    ],
                    "fullPhoneNumber": "+63 26319035",
                    "featured": true,
                    "featuredDate": null,
                    "gmtMinutesOffset": -480,
                    "groupId": "54eeb59be4b0dcdb5bfd4448",
                    "planType": "covers",
                    "reservationEmail": null,
                    "facebookImage": null,
                    "tzid": "Asia/Manila",
                    "hoiioNumber": null,
                    "ipadAppVersion": null,
                    "logoUrl": null,
                    "chefRestaurant": null,
                    "config": null,
                    "listingUrl": null,
                    "replyTo": null,
                    "checkSize": null,
                    "restaurantSlug": "edsa-beverage-design-studio",
                    "ccCheckStartTimestamp": null,
                    "ccCheckEndTimestamp": null,
                    "ccCheckCovers": null,
                    "ccCheckCoversMessage": null,
                    "premiumbooking": null,
                    "accessPoints": null,
                    "street": "",
                    "thumbnail": "https://tabledb-seats.s3.amazonaws.com/seats/rest/thumb_550a202ee4b0f1bb4378c14d.jpg",
                    "enableNewletter": false,
                    "disabledNoPromotion": false,
                    "gmtminutesOffset": -480
                },
                "restaurantReservationNote": null,
                "floorplanId": null,
                "configId": "558407c1e4b030d36bf049b8",
                "shiftId": 5,
                "quotedTime": null,
                "campaignId": "organic",
                "pending": null,
                "verificationKey": "10e2bc17-e505-4031-95c1-c71dbeb5111d",
                "widgetId": "54728d3355bda4ff32479560",
                "widget": {
                    "id": "54728d3355bda4ff32479560",
                    "createdBy": null,
                    "createdDate": null,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1427167911407,
                    "version": 4,
                    "domainName": "https://www.facebook.com/seatsph",
                    "restaurantId": "",
                    "vendorType": "SINGLE",
                    "chargeable": true,
                    "description": "Seats.com.ph",
                    "partnerCode": "seats"
                },
                "resetWaitingCreatedTime": null,
                "resetCompletedTime": null,
                "resetSeatedTime": null,
                "hasSlotChanged": false,
                "modifyingDiner": false,
                "statusChanged": false,
                "spendingAmount": null,
                "turnoverTime": null,
                "promotion": null,
                "promotionId": null,
                "adultSeats": 1,
                "childSeats": 1,
                "flexDayConfigId": null,
                "flexTableIds": null,
                "flexIsJoinedTable": null,
                "messageSent": null,
                "emailSent": null,
                "paymentTransactionId": null,
                "staffName": null,
                "staffId": null,
                "originalBookedDate": null,
                "originalTimeslot": null,
                "historyTimeNotes": null,
                "waitingList": false
            }
        ],
        "totalPages": null,
        "exception": null
    }
}
                    </pre>
                </small>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <small>
                    <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "query failed"
    }
}
                    </pre>
                </small>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">ADD USER RESERVATION</label>
        <div class="well">
            <p>add reservation</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/reservations/add
                <hr/>
                {URL}/users/+639279655572/reservations/add
                <hr/>
                {URL}/users/+639279655572/reservations/add
                ?<?= htmlspecialchars('adult_seats=2&child_seats=0
                &restaurant_id=54d436abe4b006e8e6ceb1a2
                &timeslot_id=1300&shift_id=3&notes=asdasd') ?>
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>restaurant_id</td>
                        <td><small>string</small></td>
                        <td><code>550a202ee4b0f1bb4378c14d</code></td>
                    </tr>
                    <tr>
                        <td>date</td>
                        <td><small>date</small></td>
                        <td><code>2015-07-12</code></td>
                    </tr>
                    <tr>
                        <td>timeslot_id</td>
                        <td><small>int</small></td>
                        <td><code>1830</code></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            this is from the <strong >restaurant availability</strong> API
                        </td>
                    </tr>
                    <tr>
                        <td>shift_id</td>
                        <td><small>int</small></td>
                        <td><code>5</code></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            this is usually <code>5</code> also from <strong >restaurant availability</strong> API
                        </td>
                    </tr>
                    <tr>
                        <td>promotion_id</td>
                        <td><small>string</small></td>
                        <td><code>55ded2d867ebfa5815000029</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">default is <code>null</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">use promotions API to get promotion IDs</td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            this can be retrieved from promotions API
                        </td>
                    </tr>
                    <tr>
                        <td>notes</td>
                        <td><small>string</small></td>
                        <td><code>""</code></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            this is the restaurant message
                        </td>
                    </tr>
                    <tr>
                        <td>adult_seats</td>
                        <td><small>int</small></td>
                        <td><code>1</code></td>
                    </tr>
                    <tr>
                        <td>child_seats</td>
                        <td><small>int</small></td>
                        <td><code>1</code></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>success</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>reservation failed</td>

                    </tr>
                    <tr>
                        <td></td>
                        <td><td>user does not exist</td></td>

                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <small>
                    <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "55e92182e4b0dab9ae4da581",
                "createdBy": null,
                "createdDate": null,
                "lastModifiedBy": null,
                "lastModifiedDate": null,
                "version": 0,
                "dinerId": "55e91c2fe4b0dab9ae4da551",
                "diner": {
                    "id": "55e91c2fe4b0dab9ae4da551",
                    "createdBy": null,
                    "createdDate": 1441340463480,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1441341825995,
                    "version": 24,
                    "firstName": "wilhelm",
                    "lastName": "martinez",
                    "companyName": null,
                    "position": null,
                    "restaurantId": "54d436abe4b006e8e6ceb1a2",
                    "webUserId": "55c99d0ee4b0dab9ae4da42e",
                    "birthday": null,
                    "anniversary": null,
                    "note": null,
                    "phones": [
                        {
                            "label": "Mobile",
                            "value": "9279655572",
                            "countryCode": "+63",
                            "isPrimary": true
                        }
                    ],
                    "emails": [
                        {
                            "label": "Email",
                            "value": "wilhelmpaulm@gmail.com",
                            "isPrimary": true
                        }
                    ],
                    "addresses": null,
                    "tagIds": null,
                    "relationships": null,
                    "allergies": null,
                    "salutation": null,
                    "lastVisitDateTime": null,
                    "primaryMobileNumber": "+639279655572",
                    "groupId": "54d436abe4b006e8e6ceb1a1",
                    "otherRestaurantIds": [],
                    "zipCode": null,
                    "status": "active",
                    "walkin": false,
                    "newsletter": false
                },
                "restaurantId": "54d436abe4b006e8e6ceb1a2",
                "timeSlotId": 1415,
                "date": 1441324800000,
                "numberOfSeats": 4,
                "status": 1,
                "shiftIdCreated": null,
                "shiftId": 3,
                "note": "asdasd",
                "campaignId": "organic",
                "verificationKey": "73f19ecf-7931-402a-81fe-c70764c37d69",
                "restaurant": null,
                "widgetId": "54728d3355bda4ff32479560",
                "promotionId": null,
                "promotion": null,
                "adultSeats": 2,
                "childSeats": 2,
                "turnoverTime": null,
                "flexTableIds": null,
                "sendDealEmails": false,
                "paymentTransactionId": null,
                "restaurantReservationNote": null,
                "expired": false
            }
        ]
    }
}
                    </pre>
                </small>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message": "Reservation failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">CANCEL USER RESERVATION</label>
        <div class="well">
            <p>cancel a single user reservation</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/reservations/{reservationId}/cancel
                <hr/>
                {URL}/users/+639279655572/reservations/55c9c850e4b0dab9ae4da4c8/cancel
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>reservation canceled</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>action failed</td>

                    </tr>
                    <tr>
                        <td></td>
                        <td>user does not exist</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "message": "reservation canceled"
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "action failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>




<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">EDIT USER RESERVATION</label>
        <div class="well">
            <p>edit reservation</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/reservations/{reservationId}/edit
                <hr/>
                {URL}/users/+639279655572/reservations/55c9c850e4b0dab9ae4da4c8/
                edit?<?= htmlspecialchars('adult_seats=2&child_seats=0
                &timeslot_id=1500&shift_id=3&notes=asdasd') ?>
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>date</td>
                        <td><small>date</small></td>
                        <td><code>2015-12-16</code></td>
                    </tr>
                    <tr>
                        <td>promotion_id</td>
                        <td><small>int</small></td>
                        <td><code>55dedf1667ebfa5815000041</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">default is <code>null</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">use promotions API to get promotion IDs</td>
                    </tr>
                    <tr>
                        <td>timeslot_id</td>
                        <td><small>int</small></td>
                        <td><code>1830</code></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            this is from the <strong >restaurant availability</strong> API
                        </td>
                    </tr>
                    <tr>
                        <td>shift_id</td>
                        <td><small>int</small></td>
                        <td><code>5</code></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            this is usually <code>5</code> also from <strong >restaurant availability</strong> API
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            this can be retrieved from promotions API
                        </td>
                    </tr>
                    <tr>
                        <td>notes</td>
                        <td><small>string</small></td>
                        <td><code>""</code></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            this is the restaurant message
                        </td>
                    </tr>
                    <tr>
                        <td>adult_seats</td>
                        <td><small>int</small></td>
                        <td><code>1</code></td>
                    </tr>
                    <tr>
                        <td>child_seats</td>
                        <td><small>int</small></td>
                        <td><code>1</code></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>Reservation was successfully edited</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>Reservation edit failed</td>

                    </tr>
                    <tr>
                        <td></td>
                        <td>Reservation does not exist</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Query failed</td>

                    </tr>
                    <tr>
                        <td></td>
                        <td>No user with that mobile bumber was found</td>

                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <small>
                    <pre class="well">
{
    "response": {
        "status": "200",
        "message": "Reservation was successfully edited",
        "data": [
            {
                "id": "566fa922e4b0541e61becd71",
                "createdBy": null,
                "createdDate": 1450158370277,
                "lastModifiedBy": null,
                "lastModifiedDate": 1450158235377,
                "version": 2,
                "dinerId": "55c46ea3e4b07e4ac935570e",
                "diner": {
                    "id": "55c46ea3e4b07e4ac935570e",
                    "createdBy": null,
                    "createdDate": 1438936739549,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1450158370260,
                    "version": 8,
                    "firstName": "Wilhelm Paul",
                    "lastName": "Martinez",
                    "companyName": null,
                    "position": null,
                    "restaurantId": "556bb3b3e4b0032e07899488",
                    "webUserId": "559e3e24e4b04bf2709b49d0",
                    "birthday": null,
                    "anniversary": null,
                    "note": null,
                    "phones": [
                        {
                            "label": "Mobile",
                            "value": "9279655572",
                            "countryCode": "+63",
                            "isPrimary": true
                        }
                    ],
                    "emails": [
                        {
                            "label": "Email",
                            "value": "wilhelmpaulm@gmail.com",
                            "isPrimary": true
                        }
                    ],
                    "addresses": null,
                    "tagIds": null,
                    "relationships": null,
                    "allergies": null,
                    "salutation": "Mr",
                    "lastVisitDateTime": null,
                    "primaryMobileNumber": "+639279655572",
                    "groupId": "556bb3b3e4b0032e07899487",
                    "otherRestaurantIds": [
                        "55ded7a8e4b0032e0789b486"
                    ],
                    "zipCode": null,
                    "status": "active",
                    "walkin": false,
                    "newsletter": false,
                    "unwanted": false,
                    "countReservation": null
                },
                "timeSlotId": 1600,
                "note": "",
                "tagIds": null,
                "tables": null,
                "date": 1450224000000,
                "mode": 2,
                "numberOfSeats": 2,
                "status": 1,
                "templateSlotId": null,
                "overrideTemplateSlotId": "566fa922e4b0541e61becd70",
                "seatedTime": null,
                "completedTime": null,
                "waitingCreatedTime": null,
                "restaurantId": "55ded7a8e4b0032e0789b486",
                "restaurant": {
                    "id": "55ded7a8e4b0032e0789b486",
                    "createdBy": null,
                    "createdDate": 1440667560851,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1446025356157,
                    "version": 25,
                    "name": "SEATS BISTRO",
                    "localName": null,
                    "dressCode": null,
                    "diningStyle": null,
                    "cuisines": null,
                    "neighborhood": "",
                    "crossStreet": null,
                    "menu": null,
                    "price": "600",
                    "website": "",
                    "hoursOfOperation": "Daily 11am to 10pm",
                    "paymentOptions": [],
                    "acceptWalkins": null,
                    "additionalInfo": null,
                    "publicTransit": null,
                    "parking": null,
                    "parkingDetails": "",
                    "catering": null,
                    "specialEventAndPromotions": null,
                    "address": "Burgos Circle, BGC",
                    "localAddress": null,
                    "city": "Taguig City",
                    "localCity": null,
                    "country": "PH",
                    "postalCode": "1634",
                    "specialties": "Freebies & GC's",
                    "chef": "",
                    "description": "* Demo restaurant<br /><br />Get freebies and a chance to win Gift Certificates!<br /><br />Visit our booth to get freebies and a chance to win GCs!<br /><br />Here&rsquo;s how: <br />1. Make a reservation under the Seats Bistro restaurant<br />2. Visit the Seats booth&nbsp;<br />3. Provide the host with your name and claim your goodies<br /><br />* Each reservation from a device entitles user to one freebie and one raffle entry to win Gift Certificates.",
                    "phone": "9175144723",
                    "email": "info@seats.com.ph",
                    "latitude": 14.5523837,
                    "longitude": 121.0442479,
                    "images": [
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/seatsbs1_20150901_1600.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/seatsbs2_20150901_7875.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/seatsbs3_20150901_8304.jpg",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/11_20151028_8480.png",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/111_20151028_2717.png",
                        "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/1-1_20151028_9586.jpg"
                    ],
                    "publicTransport": "",
                    "corkageDetails": "",
                    "eventFacilities": "",
                    "additionalDetails": [],
                    "fullPhoneNumber": "+63 9175144723",
                    "featured": true,
                    "featuredDate": null,
                    "gmtMinutesOffset": -480,
                    "groupId": "556bb3b3e4b0032e07899487",
                    "planType": "covers",
                    "reservationEmail": null,
                    "facebookImage": null,
                    "tzid": "Asia/Manila",
                    "hoiioNumber": null,
                    "ipadAppVersion": null,
                    "logoUrl": null,
                    "chefRestaurant": null,
                    "config": null,
                    "listingUrl": null,
                    "replyTo": null,
                    "checkSize": null,
                    "restaurantSlug": "seats-bistro",
                    "ccCheckStartTimestamp": null,
                    "ccCheckEndTimestamp": null,
                    "ccCheckCovers": null,
                    "ccCheckCoversMessage": null,
                    "premiumbooking": null,
                    "accessPoints": null,
                    "street": "",
                    "thumbnail": "https://tabledb-seats.s3.amazonaws.com/seats/rest/thumb_55ded7a8e4b0032e0789b486.jpg",
                    "enableNewletter": false,
                    "disabledNoPromotion": false,
                    "extRestaurantId": null,
                    "showPromoField": null,
                    "gmtminutesOffset": -480
                },
                "restaurantReservationNote": null,
                "floorplanId": null,
                "configId": "5629e3b8e4b0a31f0086f3ae",
                "shiftId": 3,
                "quotedTime": null,
                "campaignId": "organic",
                "pending": null,
                "verificationKey": "cbcc9162-b675-4fa7-8eb2-b122e8c71153",
                "widgetId": "54728d3355bda4ff32479560",
                "widget": {
                    "id": "54728d3355bda4ff32479560",
                    "createdBy": null,
                    "createdDate": null,
                    "lastModifiedBy": null,
                    "lastModifiedDate": 1447659858909,
                    "version": 6,
                    "domainName": "",
                    "restaurantId": "",
                    "vendorType": "SINGLE",
                    "chargeable": true,
                    "description": "",
                    "partnerCode": ""
                },
                "resetWaitingCreatedTime": null,
                "resetCompletedTime": null,
                "resetSeatedTime": null,
                "hasSlotChanged": false,
                "modifyingDiner": false,
                "statusChanged": false,
                "spendingAmount": null,
                "turnoverTime": null,
                "promotion": null,
                "promotionId": null,
                "adultSeats": 2,
                "childSeats": 0,
                "flexDayConfigId": null,
                "flexTableIds": null,
                "flexIsJoinedTable": null,
                "messageSent": null,
                "emailSent": null,
                "paymentTransactionId": null,
                "staffName": null,
                "staffId": null,
                "originalBookedDate": null,
                "originalTimeslot": null,
                "historyTimeNotes": null,
                "extBookingId": null,
                "extCancelUrl": null,
                "originalAdult": null,
                "originalChild": null,
                "promocodeId": null,
                "waitingList": false
            }
        ]
    }
}}
                    </pre>
                </small>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <small>
                    <pre class="well">
{
    "response": {
        "status": "404",
        "message": "Reservation edit failed"
    }
}
                    </pre>
                </small>
            </div>
        </div>
    </div>
</div>