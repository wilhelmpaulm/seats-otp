<div id="_users" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET ALL USERS</label>
        <div class="well">
            <p>You get all the users</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>users are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>user does not exist</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
  "response": {
    "status": "200",
    "data": [
      {...},
      {...},
      {...},
      {
        "id": "2",
        "created_at": "2015-08-10 15:28:13",
        "updated_at": "2015-08-10 15:33:19",
        "status": "active",
        "type": "user",
        "id_raw_user": "55c85402e4b0dab9ae4da428",
        "id_raw_user_temp": "55c85478e4b0dab9ae4da429",
        "code": "4797",
        "first_name": null,
        "middle_name": null,
        "last_name": null,
        "designation": null,
        "email": "wilhelmpaulm@gmail.com",
        "mobile": "+639279655572",
        "birthdate": null,
        "gender": null,
        "password": null,
        "verified": "false",
        "points": "0",
        "picture": null,
        "fb_id": null,
        "fb_link": null
      }
    ]
  }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "query failed" 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET USER</label>
        <div class="well">
            <p>Get a user</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}
                <hr/>
                {URL}/users/+639279655572
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>user is returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>user does not exist</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
  "response": {
    "status": "200",
    "data": [
      {
        "id": "2",
        "created_at": "2015-08-10 15:28:13",
        "updated_at": "2015-08-10 15:33:19",
        "status": "active",
        "type": "user",
        "id_raw_user": "55c85402e4b0dab9ae4da428",
        "id_raw_user_temp": "55c85478e4b0dab9ae4da429",
        "code": "4797",
        "first_name": null,
        "middle_name": null,
        "last_name": null,
        "designation": null,
        "email": "wilhelmpaulm@gmail.com",
        "mobile": "+639279655572",
        "birthdate": null,
        "gender": null,
        "password": null,
        "verified": "false",
        "points": "0",
        "image": null,
        "gcm": null,
        "fb_id": null,
        "fb_link": null
      }
    ]
  }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message: : "user does not exist"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">ADD USER</label>
        <div class="well">
            <p>Add single user</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/add
                <hr/>
                {URL}/users/add?mobile=+639100000000
                &last_name=cruz&first_name=juan&middle_name=de
                &designation=Mr&gender=male&birthdate=1993-02-26
                &email=wilhelmpaulm@gmail.com
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>first_name</td>
                        <td><small>string</small></td>
                        <td><code>wilhelm</code></td>
                    </tr>
                    <tr>
                        <td>middle_name</td>
                        <td><small>string</small></td>
                        <td><code>bautista</code></td>
                    </tr>
                    <tr>
                        <td>last_name</td>
                        <td><small>string</small></td>
                        <td><code>martinez</code></td>
                    </tr>
                    <tr>
                        <td>designation</td>
                        <td><small>string</small></td>
                        <td><code>Mr</code>, <code>Ms</code> or <code>Mrs</code></td>
                    </tr>
                    <tr>
                        <td>mobile</td>
                        <td><small>string</small></td>
                        <td><code>+639279655572</code></td>
                    </tr>
                    <tr>
                        <td>email</td>
                        <td><small>string</small></td>
                        <td><code>wilhelmpaulm@gmail.com</code></td>
                    </tr>
                    <tr>
                        <td>gender</td>
                        <td><small>string</small></td>
                        <td><code>male</code> or <code>female</code></td>
                    </tr>
                    <tr>
                        <td>image</td>
                        <td><small>url</small></td>
                        <td>link to your user image</td>
                    </tr>
                    <tr>
                        <td>fb_id</td>
                        <td><small>string</small></td>
                        <td>FB ID collected from FB api, when this is present, image will be replaced with the link to user's FB profile picture</td>
                    </tr>
                    <tr>
                        <td>gcm</td>
                        <td><small>gcm hash</small></td>
                        <td>
                            <code>
                                APA91bGkLmf5ESox2fzgwBpunVf6Ywwh_HOoCRu
                                QB1IgFxlXJaUH0WQ-UkuRADxUJRPPibC3ttglrc2
                                8u_odSGSgLvRZG3_ULeyxFr4rL2Lx0SK_Yf4EBHG
                                LWX0ci9f0x_Nf1JJEVylbbC49SxviiGYZ3s5_zPICfQ
                            </code>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>success</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>add user failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
"response": {
    "status": "200",
        "data": [
            {
            "id": "5",
            "created_at": "2015-08-12 02:11:14",
            "updated_at": "2015-08-12 02:11:16",
            "status": "active",
            "type": "user",
            "id_raw_user": "55ca3ac5e4b0dab9ae4da4e9",
            "id_raw_user_temp": "55ca3ac5e4b0dab9ae4da4e9",
            "code": null,
            "first_name": "juan",
            "middle_name": "de",
            "last_name": "cruz",
            "designation": "Mr",
            "email": "wilhelmpaulm@gmail.com",
            "mobile": "+639100000000",
            "birthdate": "1993-02-26 00:00:00",
            "gender": "male",
            "password": null,
            "verified": "false",
            "points": "0",
            "picture": null,
            "fb_id": null,
            "fb_link": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "add user failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">EDIT USER</label>
        <div class="well">
            <p>Edit single user</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/edit/{mobile}/edit
                <hr/>
                {URL}/users/+639100000000/edit?
                last_name=senyor&first_name=luchador&middle_name=iniego
                &designation=Ms&gender=female&birthdate=1995-02-26
                &email=madame_lucha@dora.com
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>first_name</td>
                        <td><small>string</small></td>
                        <td><code>luchador</code></td>
                    </tr>
                    <tr>
                        <td>middle_name</td>
                        <td><small>string</small></td>
                        <td><code>iniego</code></td>
                    </tr>
                    <tr>
                        <td>last_name</td>
                        <td><small>string</small></td>
                        <td><code>senyor</code></td>
                    </tr>
                    <tr>
                        <td>designation</td>
                        <td><small>string</small></td>
                        <td><code>Mr</code>, <code>Ms</code> or <code>Mrs</code></td>
                    </tr>
                    <tr>
                        <td>mobile</td>
                        <td><small>string</small></td>
                        <td><code>+639100000000</code></td>
                    </tr>
                    <tr>
                        <td>email</td>
                        <td><small>string</small></td>
                        <td><code>madame_lucha@dora.com</code></td>
                    </tr>
                    <tr>
                        <td>fb_id</td>
                        <td><small>string</small></td>
                        <td><code>FB ID collected from the FB API</code></td>
                    </tr>
                    <tr>
                        <td>gender</td>
                        <td><small>string</small></td>
                        <td><code>male</code> or <code>female</code></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>success</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>add user failed</td>
                    </tr>
                    <tr>
                        <td>403</td>
                        <td>request for user already exists</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "5",
                "created_at": "2015-08-12 02:11:14",
                "updated_at": "2015-08-12 02:17:59",
                "status": "active",
                "type": "user",
                "id_raw_user": "55ca3bfbe4b0dab9ae4da4ea",
                "id_raw_user_temp": "55ca3bfbe4b0dab9ae4da4ea",
                "code": null,
                "first_name": "luchador",
                "middle_name": "iniego",
                "last_name": "senyor",
                "designation": "Ms",
                "email": "madame_lucha@dora.com",
                "mobile": "+639100000000",
                "birthdate": "1995-02-26 00:00:00",
                "gender": "female",
                "password": null,
                "verified": "false",
                "points": "0",
                "picture": null,
                "fb_id": null,
                "fb_link": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "add user failed"
    }
}

{
    "response": {
        "status": "403",
        "message": "request for user already exists"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>