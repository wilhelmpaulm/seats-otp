<div id="_restaurants" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET ALL RESTAURANTS</label>
        <div class="well">
            <p>tada restaurants</p>
            <p>restaurants now also have ratings</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/restaurants
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>restaurants are returned with ratings</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {...),
            {...),
            {...),
            {
                "id": "556bb4cfe4b0032e0789948d",
                "createdBy": null,
                "createdDate": 1433121999550,
                "lastModifiedBy": null,
                "lastModifiedDate": 1434858874630,
                "version": 6,
                "name": "Oh! Is That Bacon? (TGP)",
                "localName": null,
                "dressCode": 1,
                "diningStyle": 1,
                "cuisines": null,
                "neighborhood": "Bonifacio Global City",
                "crossStreet": null,
                "menu": null,
                "price": "300",
                "website": "",
                "hoursOfOperation": "Monday to Friday 8:00 am to 5:00 pm",
                "paymentOptions": [
                    1,
                    2,
                    3
                ],
                "acceptWalkins": null,
                "additionalInfo": null,
                "publicTransit": null,
                "parking": null,
                "parkingDetails": "Building Parking",
                "catering": null,
                "specialEventAndPromotions": null,
                "address": "24F The Globe Tower, Bonifacio Global City",
                "localAddress": null,
                "city": "Taguig",
                "localCity": null,
                "country": "PH",
                "postalCode": "1634",
                "specialties": "Bacon",
                "chef": "",
                "description": "DEMO TGP",
                "phone": "9176881513",
                "email": "cjtan@globe.com.ph",
                "latitude": 14.553455,
                "longitude": 121.049882,
                "images": [
                    "https://tabledb-seats.s3.amazonaws.com/seats/rest/556bb4cfe4b0032e0789948d/ohbbanner-2_20150603_8938.jpg",
                    "https://tabledb-seats.s3.amazonaws.com/seats/rest/556bb4cfe4b0032e0789948d/ohbbanner_20150603_2815.jpg"
                ],
                "publicTransport": "",
                "corkageDetails": "",
                "eventFacilities": "",
                "additionalDetails": [
                    1,
                    7,
                    13,
                    14
                ],
                "fullPhoneNumber": "+63 9176881513",
                "featured": true,
                "featuredDate": null,
                "gmtMinutesOffset": -480,
                "groupId": "556bb3b3e4b0032e07899487",
                "planType": "covers",
                "reservationEmail": null,
                "facebookImage": null,
                "tzid": "Asia/Manila",
                "hoiioNumber": null,
                "ipadAppVersion": null,
                "logoUrl": null,
                "chefRestaurant": null,
                "config": null,
                "listingUrl": "http://restaurants.seats.com.ph/oh-is-that-bacon-tgp/556bb4cfe4b0032e0789948d",
                "replyTo": null,
                "checkSize": null,
                "restaurantSlug": "oh-is-that-bacon-tgp",
                "ccCheckStartTimestamp": null,
                "ccCheckEndTimestamp": null,
                "ccCheckCovers": null,
                "ccCheckCoversMessage": null,
                "premiumbooking": null,
                "accessPoints": null,
                "street": "",
                "thumbnail": "https://tabledb-seats.s3.amazonaws.com/seats/rest/thumb_556bb4cfe4b0032e0789948d.jpg",
                "enableNewletter": false,
                "disabledNoPromotion": false,
                "gmtminutesOffset": -480,
                "rating": {
                    "taste": 0,
                    "ambiance": 0,
                    "value": 0,
                    "cleanliness": 0,
                    "service": 0,
                    "general": 0
                }
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404" 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET RESTAURANT</label>
        <div class="well">
            <p>get a restaurant</p>
            <p>restaurants now also has ratings</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/restaurants/{restaurantId}
                <hr/>
                {URL}/restaurants/556bb4cfe4b0032e0789948d
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>user is returned with ratings</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>user does not exist</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "556bb4cfe4b0032e0789948d",
                "createdBy": null,
                "createdDate": 1433121999550,
                "lastModifiedBy": null,
                "lastModifiedDate": 1434858874630,
                "version": 6,
                "name": "Oh! Is That Bacon? (TGP)",
                "localName": null,
                "dressCode": 1,
                "diningStyle": 1,
                "cuisines": null,
                "neighborhood": "Bonifacio Global City",
                "crossStreet": null,
                "menu": null,
                "price": "300",
                "website": "",
                "hoursOfOperation": "Monday to Friday 8:00 am to 5:00 pm",
                "paymentOptions": [
                    1,
                    2,
                    3
                ],
                "acceptWalkins": null,
                "additionalInfo": null,
                "publicTransit": null,
                "parking": null,
                "parkingDetails": "Building Parking",
                "catering": null,
                "specialEventAndPromotions": null,
                "address": "24F The Globe Tower, Bonifacio Global City",
                "localAddress": null,
                "city": "Taguig",
                "localCity": null,
                "country": "PH",
                "postalCode": "1634",
                "specialties": "Bacon",
                "chef": "",
                "description": "DEMO TGP",
                "phone": "9176881513",
                "email": "cjtan@globe.com.ph",
                "latitude": 14.553455,
                "longitude": 121.049882,
                "images": [
                    "https://tabledb-seats.s3.amazonaws.com/seats/rest/556bb4cfe4b0032e0789948d/ohbbanner-2_20150603_8938.jpg",
                    "https://tabledb-seats.s3.amazonaws.com/seats/rest/556bb4cfe4b0032e0789948d/ohbbanner_20150603_2815.jpg"
                ],
                "publicTransport": "",
                "corkageDetails": "",
                "eventFacilities": "",
                "additionalDetails": [
                    1,
                    7,
                    13,
                    14
                ],
                "fullPhoneNumber": "+63 9176881513",
                "featured": true,
                "featuredDate": null,
                "gmtMinutesOffset": -480,
                "groupId": "556bb3b3e4b0032e07899487",
                "planType": "covers",
                "reservationEmail": null,
                "facebookImage": null,
                "tzid": "Asia/Manila",
                "hoiioNumber": null,
                "ipadAppVersion": null,
                "logoUrl": null,
                "chefRestaurant": null,
                "config": null,
                "listingUrl": "http://restaurants.seats.com.ph/oh-is-that-bacon-tgp/556bb4cfe4b0032e0789948d",
                "replyTo": null,
                "checkSize": null,
                "restaurantSlug": "oh-is-that-bacon-tgp",
                "ccCheckStartTimestamp": null,
                "ccCheckEndTimestamp": null,
                "ccCheckCovers": null,
                "ccCheckCoversMessage": null,
                "premiumbooking": null,
                "accessPoints": null,
                "street": "",
                "thumbnail": "https://tabledb-seats.s3.amazonaws.com/seats/rest/thumb_556bb4cfe4b0032e0789948d.jpg",
                "enableNewletter": false,
                "disabledNoPromotion": false,
                "gmtminutesOffset": -480,
                "rating": {
                    "taste": 0,
                    "ambiance": 0,
                    "value": 0,
                    "cleanliness": 0,
                    "service": 0,
                    "general": 0
                }
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message: : "query failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET RESTAURANT AVAILABILITY</label>
        <div class="well">
            <p>get restaurant availability</p>
            <p>required step to reserve</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/restaurants/{restaurantId}/availability/
                <hr/>
                {URL}/restaurants/{restaurantId}/availability/{date}
                <hr/>
                {URL}/restaurants/556bb4cfe4b0032e0789948d/availability/2015-10-05
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>available schedules are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": 0,
        "startRow": 0,
        "endRow": 15,
        "totalRows": 16,
        "data": [
            {
                "timeSlotId": 2100,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 1800,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 2030,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 2145,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 2015,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 1815,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 1945,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 2130,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 1930,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 1830,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 2000,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 1900,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 2045,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 2115,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 1845,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            },
            {
                "timeSlotId": 1915,
                "remainingTimeSlotCapacity": 20,
                "remainingShiftCapacity": 20,
                "maxNumberOfSeatsForSingleReservation": 12,
                "minNumberOfSeatsForSingleReservation": 1,
                "shiftId": 5,
                "cutOffTime": null,
                "minPartySizeBooking": null,
                "maxPartySizeBooking": null
            }
        ],
        "totalPages": null,
        "exception": null
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message: : "query failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>