<div id="_tags" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET DEFAULT TAGS</label>
        <div class="well">
            <p>Default lists for tags</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/tags
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">
                <tbody>
                    <tr>
                        <td>200</td>
                        <td>an array of default lists will be returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "index": "1",
                "name": "lunch",
                "image": "http://52.74.54.234:7777/public/bg/lunch.png"
            },
            {
                "index": "2",
                "name": "buffet",
                "image": "http://52.74.54.234:7777/public/bg/buffet.png"
            },
            {
                "index": "3",
                "name": "cafe",
                "image": "http://52.74.54.234:7777/public/bg/cafe.png"
            },
            {
                "index": "4",
                "name": "big groups",
                "image": "http://52.74.54.234:7777/public/bg/big_group.png"
            },
            {
                "index": "5",
                "name": "vegetarian",
                "image": "http://52.74.54.234:7777/public/bg/vegetarian.png"
            },
            {
                "index": "6",
                "name": "halal",
                "image": "http://52.74.54.234:7777/public/bg/halal.png"
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "query failed" 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>