<div id="_login" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">LOGIN</label>
        <div class="well">
            <p>login user</p>
            <p>sends an OTP to the user's phone</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/login
                <hr/>
                {URL}/users/+639279655572/login
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>user key is returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>user does not exist</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": 0,
        "startRow": 0,
        "endRow": 1,
        "totalRows": 1,
        "data": [
            "55caea20e4b0dab9ae4da4f5"
        ],
        "totalPages": null,
        "exception": null
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "user does not exist" 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">CONFIRM LOGIN</label>
        <div class="well">
            <p>confirm user login to retrieve user information</p>
            <p>by confirming the user, the user details are returned</p>
            <p>PS</p>
            <p>while stagging, the OTP isn't sent to the user's mobile</p>
            <p>please use the URL FOR STAGGING link to check the OTP code</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/login/{code}
                <hr/>
                {URL}/users/+639279655572/login/8634 
            </code>
        </div>
        <label>URL FOR STAGGING</label>
        <div class="well">
            <code>
                <a href="http://stagingreservations.seats.com.ph/seeotp.php">stagingreservations.seats.com.ph/seeotp.php</a>
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>user is returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>user does not exist</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>action failed</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>invalid key</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "2",
                "created_at": "2015-08-10 15:28:13",
                "updated_at": "2015-08-12 14:35:57",
                "status": "active",
                "type": "user",
                "id_raw_user": "55c85402e4b0dab9ae4da428",
                "id_raw_user_temp": "55caea20e4b0dab9ae4da4f5",
                "code": "4797",
                "first_name": "wilhelm",
                "middle_name": null,
                "last_name": "martinez",
                "designation": null,
                "email": "wilhelmpaulm@gmail.com",
                "mobile": "+639279655572",
                "birthdate": null,
                "gender": null,
                "password": null,
                "verified": "false",
                "points": "0",
                "picture": null,
                "fb_id": null,
                "fb_link": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message: : "user does not exist"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">ADD USER</label>
        <div class="well">
            <p>Add single user</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/add
                <hr/>
                {URL}/users/add?mobile=+639100000000
                &last_name=cruz&first_name=juan&middle_name=de
                &designation=Mr&gender=male&birthdate=1993-02-26
                &email=wilhelmpaulm@gmail.com
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>first_name</td>
                        <td><small>string</small></td>
                        <td><code>wilhelm</code></td>
                    </tr>
                    <tr>
                        <td>middle_name</td>
                        <td><small>string</small></td>
                        <td><code>bautista</code></td>
                    </tr>
                    <tr>
                        <td>last_name</td>
                        <td><small>string</small></td>
                        <td><code>martinez</code></td>
                    </tr>
                    <tr>
                        <td>designation</td>
                        <td><small>string</small></td>
                        <td><code>Mr</code>, <code>Ms</code> or <code>Mrs</code></td>
                    </tr>
                    <tr>
                        <td>mobile</td>
                        <td><small>string</small></td>
                        <td><code>+639279655572</code></td>
                    </tr>
                    <tr>
                        <td>email</td>
                        <td><small>string</small></td>
                        <td><code>wilhelmpaulm@gmail.com</code></td>
                    </tr>
                    <tr>
                        <td>gender</td>
                        <td><small>string</small></td>
                        <td><code>male</code> or <code>female</code></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>success</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>add user failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
"response": {
    "status": "200",
        "data": [
            {
            "id": "5",
            "created_at": "2015-08-12 02:11:14",
            "updated_at": "2015-08-12 02:11:16",
            "status": "active",
            "type": "user",
            "id_raw_user": "55ca3ac5e4b0dab9ae4da4e9",
            "id_raw_user_temp": "55ca3ac5e4b0dab9ae4da4e9",
            "code": null,
            "first_name": "juan",
            "middle_name": "de",
            "last_name": "cruz",
            "designation": "Mr",
            "email": "wilhelmpaulm@gmail.com",
            "mobile": "+639100000000",
            "birthdate": "1993-02-26 00:00:00",
            "gender": "male",
            "password": null,
            "verified": "false",
            "points": "0",
            "picture": null,
            "fb_id": null,
            "fb_link": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "add user failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">EDIT USER</label>
        <div class="well">
            <p>Edit single user</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/edit/{mobile}/edit
                <hr/>
                {URL}/users/+639100000000/edit?
                last_name=senyor&first_name=luchador&middle_name=iniego
                &designation=Ms&gender=female&birthdate=1995-02-26
                &email=madame_lucha@dora.com
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>first_name</td>
                        <td><small>string</small></td>
                        <td><code>luchador</code></td>
                    </tr>
                    <tr>
                        <td>middle_name</td>
                        <td><small>string</small></td>
                        <td><code>iniego</code></td>
                    </tr>
                    <tr>
                        <td>last_name</td>
                        <td><small>string</small></td>
                        <td><code>senyor</code></td>
                    </tr>
                    <tr>
                        <td>designation</td>
                        <td><small>string</small></td>
                        <td><code>Mr</code>, <code>Ms</code> or <code>Mrs</code></td>
                    </tr>
                    <tr>
                        <td>mobile</td>
                        <td><small>string</small></td>
                        <td><code>+639100000000</code></td>
                    </tr>
                    <tr>
                        <td>email</td>
                        <td><small>string</small></td>
                        <td><code>madame_lucha@dora.com</code></td>
                    </tr>
                    <tr>
                        <td>gender</td>
                        <td><small>string</small></td>
                        <td><code>male</code> or <code>female</code></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>success</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>add user failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "5",
                "created_at": "2015-08-12 02:11:14",
                "updated_at": "2015-08-12 02:17:59",
                "status": "active",
                "type": "user",
                "id_raw_user": "55ca3bfbe4b0dab9ae4da4ea",
                "id_raw_user_temp": "55ca3bfbe4b0dab9ae4da4ea",
                "code": null,
                "first_name": "luchador",
                "middle_name": "iniego",
                "last_name": "senyor",
                "designation": "Ms",
                "email": "madame_lucha@dora.com",
                "mobile": "+639100000000",
                "birthdate": "1995-02-26 00:00:00",
                "gender": "female",
                "password": null,
                "verified": "false",
                "points": "0",
                "picture": null,
                "fb_id": null,
                "fb_link": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "add user failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>