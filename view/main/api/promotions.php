<div id="_promotions" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET ALL PROMOTIONS</label>
        <div class="well">
            <p>get promotions to be used for association with reservation</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/promotions
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">
                <tbody>
                    <tr>
                        <td>200</td>
                        <td>all promotions are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "54a770d9e4b0f38e0d2e1cbc",
                "createdBy": null,
                "createdDate": 1420259545124,
                "lastModifiedBy": "4412e6e6bd82c225da343a77366e7746fb9276b0",
                "lastModifiedDate": 1420687190414,
                "version": 4,
                "promotionType": null,
                "firstTimeSlotId": null,
                "lastTimeSlotId": null,
                "maximumCover": 999999,
                "maximumPartySize": 12,
                "minimumPartySize": 1,
                "title": "Buy one get one this holiday\nAnh NG Test",
                "description": "Start 10am everyday\nTest test test\nTest test\nTest",
                "dinerTypes": [
                    {
                        "id": "4fcf10c23467c7de36000001",
                        "createdBy": null,
                        "createdDate": null,
                        "lastModifiedBy": null,
                        "lastModifiedDate": null,
                        "version": null,
                        "name": "All",
                        "description": "This promotion will be available for booking.",
                        "postURL": "http: //localhost: 2345",
                        "always": true,
                        "pushLastMinuteOnly": false
                    }
                ],
                "dinerTypeIds": [],
                "restaurantStatus": null,
                "endTimestamp": 1456646400000,
                "startTimestamp": 1420675200000,
                "durationDays": null,
                "daysOfWeek": [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7
                ],
                "blockedDates": [
                    1420934400000,
                    1421539200000,
                    1422144000000,
                    1422748800000
                ],
                "live": true,
                "lastMinute": false,
                "shiftIds": [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6
                ],
                "currentCover": 2,
                "restaurantId": "54a6652ee4b0f38e0d2e1c46",
                "withdrawn": false,
                "filter": null,
                "editable": true,
                "partnerCode": null,
                "price": null,
                "specialPrice": null,
                "perDayCover": 999999,
                "dailyCoversLimits": null,
                "displayInHGW": false,
                "featuredDeal": 0,
                "dealTypeId": null,
                "dealType": null,
                "promotionImage": null,
                "promotionSummary": null,
                "promotionTermsAndConditions": null,
                "socialSharingTexts": null,
                "countryCode": null,
                "internalRemarks": null
            },
            { ... },
            { ... },
            { ... },
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET SINGLE PROMOTION</label>
        <div class="well">
            <p>get a promotions</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/promotions/{promotion_id}
                <hr/>
                {URL}/promotions/54a770d9e4b0f38e0d2e1cbc
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">
                <tbody>
                    <tr>
                        <td>200</td>
                        <td>Single promotion is returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>Query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "54a770d9e4b0f38e0d2e1cbc",
                "createdBy": null,
                "createdDate": 1420259545124,
                "lastModifiedBy": "4412e6e6bd82c225da343a77366e7746fb9276b0",
                "lastModifiedDate": 1420687190414,
                "version": 4,
                "promotionType": null,
                "firstTimeSlotId": null,
                "lastTimeSlotId": null,
                "maximumCover": 999999,
                "maximumPartySize": 12,
                "minimumPartySize": 1,
                "title": "Buy one get one this holiday\nAnh NG Test",
                "description": "Start 10am everyday\nTest test test\nTest test\nTest",
                "dinerTypes": [
                    {
                        "id": "4fcf10c23467c7de36000001",
                        "createdBy": null,
                        "createdDate": null,
                        "lastModifiedBy": null,
                        "lastModifiedDate": null,
                        "version": null,
                        "name": "All",
                        "description": "This promotion will be available for booking.",
                        "postURL": "http: //localhost: 2345",
                        "always": true,
                        "pushLastMinuteOnly": false
                    }
                ],
                "dinerTypeIds": [],
                "restaurantStatus": null,
                "endTimestamp": 1456646400000,
                "startTimestamp": 1420675200000,
                "durationDays": null,
                "daysOfWeek": [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7
                ],
                "blockedDates": [
                    1420934400000,
                    1421539200000,
                    1422144000000,
                    1422748800000
                ],
                "live": true,
                "lastMinute": false,
                "shiftIds": [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6
                ],
                "currentCover": 2,
                "restaurantId": "54a6652ee4b0f38e0d2e1c46",
                "withdrawn": false,
                "filter": null,
                "editable": true,
                "partnerCode": null,
                "price": null,
                "specialPrice": null,
                "perDayCover": 999999,
                "dailyCoversLimits": null,
                "displayInHGW": false,
                "featuredDeal": 0,
                "dealTypeId": null,
                "dealType": null,
                "promotionImage": null,
                "promotionSummary": null,
                "promotionTermsAndConditions": null,
                "socialSharingTexts": null,
                "countryCode": null,
                "internalRemarks": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET ALL PROMOTIONS OF A RESTAURANT</label>
        <div class="well">
            <p>get promotions specific to a restaurant</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/restaurant/{restaurant_id}/promotions
                <hr/>
                {URL}/restaurants/54a6652ee4b0f38e0d2e1c46/promotions
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">
                <tbody>
                    <tr>
                        <td>200</td>
                        <td>all promotions are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "54a770d9e4b0f38e0d2e1cbc",
                "createdBy": null,
                "createdDate": 1420259545124,
                "lastModifiedBy": "4412e6e6bd82c225da343a77366e7746fb9276b0",
                "lastModifiedDate": 1420687190414,
                "version": 4,
                "promotionType": null,
                "firstTimeSlotId": null,
                "lastTimeSlotId": null,
                "maximumCover": 999999,
                "maximumPartySize": 12,
                "minimumPartySize": 1,
                "title": "Buy one get one this holiday\nAnh NG Test",
                "description": "Start 10am everyday\nTest test test\nTest test\nTest",
                "dinerTypes": [
                    {
                        "id": "4fcf10c23467c7de36000001",
                        "createdBy": null,
                        "createdDate": null,
                        "lastModifiedBy": null,
                        "lastModifiedDate": null,
                        "version": null,
                        "name": "All",
                        "description": "This promotion will be available for booking.",
                        "postURL": "http: //localhost: 2345",
                        "always": true,
                        "pushLastMinuteOnly": false
                    }
                ],
                "dinerTypeIds": [],
                "restaurantStatus": null,
                "endTimestamp": 1456646400000,
                "startTimestamp": 1420675200000,
                "durationDays": null,
                "daysOfWeek": [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7
                ],
                "blockedDates": [
                    1420934400000,
                    1421539200000,
                    1422144000000,
                    1422748800000
                ],
                "live": true,
                "lastMinute": false,
                "shiftIds": [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6
                ],
                "currentCover": 2,
                "restaurantId": "54a6652ee4b0f38e0d2e1c46",
                "withdrawn": false,
                "filter": null,
                "editable": true,
                "partnerCode": null,
                "price": null,
                "specialPrice": null,
                "perDayCover": 999999,
                "dailyCoversLimits": null,
                "displayInHGW": false,
                "featuredDeal": 0,
                "dealTypeId": null,
                "dealType": null,
                "promotionImage": null,
                "promotionSummary": null,
                "promotionTermsAndConditions": null,
                "socialSharingTexts": null,
                "countryCode": null,
                "internalRemarks": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<!--<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">ADD PROMOTION</label>
        <div class="well">
            <p>add ratings</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
            or
            <code>
                POST
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/restaurants/{restaurant_id}/promotions/add
                <hr/>
                {URL}/restaurants/54c9a287e4b006e8e6ce9a7a/promotions/add
                ?title=sample$description=sample
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>status</td>
                        <td><small>string</small></td>
                        <td><code>live</code>, <code>draft</code> or <code>withdrawn</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>draft</code></td>
                    </tr>
                    <tr>
                        <td>id_raw_restaurant</td>
                        <td><small>string</small></td>
                        <td><code>54c9a287e4b006e8e6ce9a7a</code></td>
                    </tr>
                    <tr>
                        <td>title</td>
                        <td><small>string</small></td>
                        <td><code>title</code></td>
                    </tr>
                    <tr>
                        <td>description</td>
                        <td><small>string</small></td>
                        <td><code>description</code></td>
                    </tr>
                    <tr>
                        <td>start_date</td>
                        <td><small>SQL date format</small></td>
                        <td><code>2015-08-27</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is the date today</td>
                    </tr>
                    <tr>
                        <td>end_date</td>
                        <td><small>SQL date format</small></td>
                        <td><code>2015-08-28</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is the date tomorrow</td>
                    </tr>
                    <tr>
                        <td>start_time</td>
                        <td><small>four digit time</small></td>
                        <td><code>0800</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>0000</code></td>
                    </tr>
                    <tr>
                        <td>end_time</td>
                        <td><small>four digit time</small></td>
                        <td><code>0800</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>2400</code></td>
                    </tr>
                    <tr>
                        <td>party_size_min</td>
                        <td><small>int</small></td>
                        <td><code>2</code> to <code>50</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>2</code></td>
                    </tr>
                    <tr>
                        <td>party_size_max</td>
                        <td><small>int</small></td>
                        <td><code>2</code> to <code>50</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>50</code></td>
                    </tr>
                    <tr>
                        <td>cover_max</td>
                        <td><small>int</small></td>
                        <td><code>2</code> to <code>100</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>50</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> this is the total number of promotion tickets per person</td>
                    </tr>
                    <tr>
                        <td>days_of_week</td>
                        <td><small>int</small></td>
                        <td><code>1,2,3,4,5,6,7</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>1,2,3,4,5,6,7</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the days where the promotion is available</td>
                    </tr>
                    <tr>
                        <td>image</td>
                        <td><small>url</small></td>
                        <td><code>52.74.54.234:7777/public/bg/buffet.png</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>null</code></td>
                    </tr>
                    <tr>
                        <td>duration</td>
                        <td><small>int</small></td>
                        <td><code>30</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the default is <code>30</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2"> the number of days the promotion will be active</td>
                    </tr>
                    

                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>rating is returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>action failed</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "2",
                "created_at": "2015-08-12 03:50:14",
                "updated_at": null,
                "status": "pending",
                "type": "feedback",
                "id_raw_user": "55c85402e4b0dab9ae4da428",
                "id_raw_reservation": "00000022222",
                "id_raw_restaurant": "000001111",
                "id_user": "2",
                "taste": "2",
                "ambiance": null,
                "value": "4",
                "cleanliness": "5",
                "service": "2",
                "general": "3",
                "comments": "comment",
                "review": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message: : "action failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">DELETE PROMOTION</label>
        <div class="well">
            <p>delete a promotion</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/promotions/{promotion_id}/delete
                <hr/>
                {URL}/restaurant/{restaurant_id}/promotions/{promotion_id}/delete
                <hr/>
                {URL}/promotions/55dedf1667ebfa5815000041/delete
                <hr/>
                {URL}/restaurant/55dedf1667ebfa5815000041/promotions/55dedf1667ebfa5815000041/delete
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">
                <tbody>
                    <tr>
                        <td>200</td>
                        <td>all promotions are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "message": "promotion deleted",
        "data": [
            {
                "id": "2",
                "created_at": "2015-08-27 17:57:42",
                "updated_at": null,
                "status": "draft",
                "type": "normal",
                "id_raw_promotion": "55dedf1667ebfa5815000041",
                "id_raw_restaurant": "54c9a287e4b006e8e6ce9a7a",
                "title": "bang",
                "description": "boooo",
                "start_date": "2015-08-27",
                "end_date": "2015-08-28",
                "start_time": "0000",
                "end_time": "2400",
                "party_size_min": "2",
                "party_size_max": "50",
                "cover_max": "50",
                "days_of_week": "1,2,3,4,5,6,7",
                "deal_availability": "all",
                "image": null,
                "duration": "30"
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>-->