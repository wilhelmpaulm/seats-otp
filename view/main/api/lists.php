<div id="_lists" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET DEFAULT LISTS</label>
        <div class="well">
            <p>Default lists for filters and dropdowns</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/lists
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">
                <tbody>
                    <tr>
                        <td>200</td>
                        <td>an array of default lists will be returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "type": "tags",
                "list": [
                    "none",
                    "lunch",
                    "buffet",
                    "cafe",
                    "big groups",
                    "vegetarian",
                    "halal"
                ]
            },
            {
                "type": "sort",
                "list": [
                    "Most Relevant",
                    "Price : Ascending",
                    "Price : Descending",
                    "Alphabetical : Ascending",
                    "Alphabetical : Descending",
                    "Ratings : Ascending",
                    "Ratings : Descending"
                ]
            },
            {
                "type": "dining_style",
                "list": [
                    "Casual",
                    "Casual Elegant",
                    "Fine Dining"
                ]
            },
            {
                "type": "dress_code",
                "list": [
                    "Casual",
                    "Smart Casual",
                    "Jacket Required",
                    "Jacket and Tie Required",
                    "Formal"
                ]
            },
            {
                "type": "ocassions",
                "list": [
                    "Cocktails",
                    "Group Dining",
                    "Nightlife",
                    "Romantic",
                    "Buffet",
                    "After Work",
                    "BBQ",
                    "Brunch",
                    "Wine",
                    "Lunch ",
                    "Breakfast",
                    "Ala Carte",
                    "Birthday"
                ]
            },
            {
                "type": "cuisine",
                "list": [
                    "Swiss",
                    "Thai",
                    "Celebrations",
                    "Vietnamese",
                    "Western",
                    "Cantonese",
                    "Filipino",
                    "Australian",
                    "Desserts",
                    "Singaporean",
                    "Globe",
                    "Turkish",
                    "Spanish",
                    "Beer",
                    "Continental",
                    "Vietnamese",
                    "Mongolian",
                    "Burgers",
                    "Indian",
                    "Breakfast",
                    "Wine",
                    "Lunch",
                    "Pizza",
                    "Lunch",
                    "Scandinavian",
                    "Comfort Food",
                    "International",
                    "American",
                    "Japanese",
                    "Brunch",
                    "Yugoslavian",
                    "English",
                    "Greek",
                    "Dimsum",
                    "Korean",
                    "Barbecue",
                    "Dinner",
                    "Fusion",
                    "Buffet",
                    "Traditional",
                    "French",
                    "Seafood",
                    "Steak",
                    "Malaysian",
                    "Shanghai",
                    "Cocktails",
                    "Noodles",
                    "Persian",
                    "Italian",
                    "Portuguese",
                    "Modern",
                    "Pastries",
                    "Chinese",
                    "Pasta",
                    "Brazilian",
                    "Sushi",
                    "Salcedo Village",
                    "Mexican",
                    "Coffee",
                    "Family Friendly",
                    "African",
                    "Caribbean",
                    "Asian",
                    "European"
                ]
            },
            {
                "type": "payment_options",
                "list": [
                    "Cash",
                    "Visa",
                    "MasterCard",
                    "JCB",
                    "American Express",
                    "Cash on Delivery",
                    "Debit",
                    "NETS",
                    "UnionPay",
                    "Other",
                    "Diners Club"
                ]
            },
            {
                "type": "neighborhood",
                "list": [
                    "Ortigas Center",
                    "Greenhills",
                    "Makati",
                    "Bonifacio Global City",
                    "Salcedo Village",
                    "Legaspi Village",
                    "Paranaque",
                    "San Juan",
                    "Quezon City",
                    "Rockwell",
                    "Mall of Asia Complex",
                    "Alabang"
                ]
            },
            {
                "type": "additional_details",
                "list": [
                    "Alfresco",
                    "Bar",
                    "Child Friendly",
                    "Catering",
                    "Corporate Events",
                    "Delivery",
                    "Free Wi-Fi",
                    "Full Bar",
                    "Gluten-Free Menu",
                    "Large Groups",
                    "Live Music",
                    "Friendly",
                    "Private Rooms",
                    "Scenic View",
                    "Smoking Area",
                    "Non-smoking Restaurant",
                    "Takeaway",
                    "Waterfront",
                    "Weddings",
                    "Weekend Brunch",
                    "Wheelchair Friendly",
                    "Wine List"
                ]
            },
            {
                "type": "price",
                "default": "150",
                "min": "0",
                "max": "5000"
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "query failed" 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET SPECIFIC LIST</label>
        <div class="well">
            <p>Default lists for filters and dropdowns</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/lists/{name}
                <hr/>
                {URL}/lists/sort
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">
                <tbody>
                    <tr>
                        <td>200</td>
                        <td>an array of default lists will be returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "type": "sort",
                "list": [
                    "Most Relevant",
                    "Price : Ascending",
                    "Price : Descending",
                    "Alphabetical : Ascending",
                    "Alphabetical : Descending",
                    "Ratings : Ascending",
                    "Ratings : Descending"
                ]
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "query failed" 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>