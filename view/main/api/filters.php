<div id="_filters" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">FILTER</label>
        <div class="well">
            <p>Filter search results</p>
            <p>Please use the <string>lists</string> API for the comma separated list of indeces</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/filters
            </code>
            <hr/>
            <code>
                {URL}/filters?text=jade
            </code>
            <hr/>
            <code>
                {URL}/filters?sort=0&price_min=0&price_max=250&neighborhood=any&rating=2
            </code>
            <hr/>
            <code>
                {URL}/filters?sort=3&price_min=0&price_max=550&cuisine=any&neighborhood=1,2
            </code>
            <hr/>
            <code>
                {URL}/filters?sort=3&price_max=300&date=2015-08-28
            </code>
            <hr/>
            <code>
                {URL}/filters?sort=3&price_max=300&date_strict=false
            </code>
            <hr/>
            <code>
                {URL}/filters?sort=3&price_min=0&price_max=550
                &cuisine=any&neighborhood=1,2&additional_details=6
            </code>
            <hr/>
            <code>
                {URL}/filters?lat=14.5564690&long=121.015447
            </code>
            
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>sort</td>
                        <td><small>string</small></td>
                        <td><code>1</code> to <code>5</code></td>
                    </tr>
                    <tr>
                        <td>text</td>
                        <td><small>string</small></td>
                        <td><code>jade</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            this searches in the name, specialties and neighborhood 
                        </td>
                    </tr>
                  
                    <tr>
                        <td>time</td>
                        <td><small>24 hour format</small></td>
                        <td><code>0832</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            the default is the current time
                        </td>
                    </tr>
                    <tr>
                        <td>date</td>
                        <td><small>SQL date format</small></td>
                        <td><code>2015-08-28</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            the default is the date today
                        </td>
                    </tr>
                    <tr>
                        <td>date_strict</td>
                        <td><small>string</small></td>
                        <td><code>false</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            the default is <code>false</code>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            when set to <code>true</code> only restaurants that are available will be returned
                            <hr/>
                            when set to <code>false</code> all restaurants will be returned
                        </td>
                    </tr>
                    <tr>
                        <td>cuisine</td>
                        <td><small>string</small></td>
                        <td><code>0</code> to <code>62</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            sent as a comma separated list
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            you can also use <code>"any"</code> and this is also set as the default 
                        </td>
                    </tr>

                    <tr>
                        <td>distance</td>
                        <td><small>int</small></td>
                        <td><code>0</code> to <code>x</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            the default is <code>1000</code>
                        </td>
                    </tr>
                    <tr>
                        <td>long</td>
                        <td><small>float</small></td>
                        <td><code>0.0</code> to <code>x</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            the default is <code>null</code>
                        </td>
                    </tr>
                    <tr>
                        <td>lat</td>
                        <td><small>float</small></td>
                        <td><code>0.0</code> to <code>x</code></td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td colspan="2">
                            the default is <code>null</code>
                        </td>
                    </tr>
                    <tr>
                        <td>tags</td>
                        <td><small>string</small></td>
                        <td><code>0</code> to <code>x</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            this is also the categories <br/>
                            sent as a comma separated list
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            you can also use <code>"any"</code> and this is also set as the default 
                        </td>
                    </tr>
                    <tr>
                        <td>neighborhood</td>
                        <td><small>int</small></td>
                        <td><code>1</code> to <code>10</code></td>
                    </tr>
                     <tr>
                        <td></td>
                        <td colspan="2">
                            sent as a comma separated list
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            you can also use <code>"any"</code> and this is also set as the default 
                        </td>
                    </tr>
                    <tr>
                        <td>additional_details</td>
                        <td><small>int</small></td>
                        <td><code>0</code> to <code>21</code></td>
                    </tr>
                     <tr>
                        <td></td>
                        <td colspan="2">
                            sent as a comma separated list
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            you can also use <code>"any"</code> and this is also set as the default 
                        </td>
                    </tr>
                    <tr>
                        <td>price_min</td>
                        <td><small>int</small></td>
                        <td><code>0</code> to <code>50000</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            default is <code>0</code>
                        </td>
                    </tr>
                    <tr>
                        <td>price_max</td>
                        <td><small>int</small></td>
                        <td><code>1</code> to <code>50000</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            default is <code>5000</code>
                        </td>
                    </tr>
                    <tr>
                        <td>rating</td>
                        <td><small>int</small></td>
                        <td><code>0</code> to <code>5</code></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            default is <code>0</code>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">
                <tbody>
                    <tr>
                        <td>200</td>
                        <td>a sorted array of filtered restaurants</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "55233249e4b0f1bb4378cbbd",
                "createdBy": null,
                "createdDate": 1428369993398,
                "lastModifiedBy": null,
                "lastModifiedDate": 1428373550668,
                "version": 8,
                "name": "Felix",
                "localName": null,
                "dressCode": "Casual",
                "diningStyle": "Casual Elegant",
                "cuisines": null,
                "neighborhood": "Makati",
                "crossStreet": null,
                "menu": null,
                "price": "500",
                "website": "www.felix.ph",
                "hoursOfOperation": "Monday - Sunday 11AM to 11PM",
                "paymentOptions": [
                    "Cash",
                    "Visa",
                    "MasterCard"
                ],
                "acceptWalkins": null,
                "additionalInfo": null,
                "publicTransit": null,
                "parking": null,
                "parkingDetails": "Parking lot",
                "catering": null,
                "specialEventAndPromotions": null,
                "address": "Felix Restaurant Greenbelt 5  Legaspi St. Legaspi Village, Ayala Center, Makati City",
                "localAddress": null,
                "city": "Makati",
                "localCity": null,
                "country": "PH",
                "postalCode": "12",
                "specialties": "Fusion Cuisine",
                "chef": "Chef Florabel Co-Yatco",
                "description": "Dine at Felix Restaurant for an overall dining experience that is refreshing &ndash; an escape from the ordinary. The restaurant offers fusion cuisine, with every dish and drink ascertained superlative. Having the prominent Chef Florabel Co-Yatco as the master behind Felix, and the restaurant being named after her father, you are sure that only the finest and superior-quality ingredients make up each dish.<br /><br />You can&rsquo;t go wrong in your food choices because the carefully plated, mouth-watering looks of the dishes in the menu are at par with the flavor and taste that can only be described by guests as simply, divine.",
                "phone": "27299062",
                "email": "info@felix.ph",
                "latitude": 14.55271,
                "longitude": 121.017094,
                "images": [
                    "https://tabledb-seats.s3.amazonaws.com/seats/rest/55233249e4b0f1bb4378cbbd/felix1_20150407_4111.jpg",
                    "https://tabledb-seats.s3.amazonaws.com/seats/rest/55233249e4b0f1bb4378cbbd/felix4_20150407_9881.jpg"
                ],
                "publicTransport": "",
                "corkageDetails": "",
                "eventFacilities": "",
                "additionalDetails": [
                    "Alfresco",
                    "Child Friendly",
                    "Catering",
                    "Corporate Events",
                    "Free Wi-Fi",
                    "Smoking Area"
                ],
                "fullPhoneNumber": "+63 27299062",
                "featured": true,
                "featuredDate": null,
                "gmtMinutesOffset": -480,
                "groupId": "55233249e4b0f1bb4378cbbc",
                "planType": "covers",
                "reservationEmail": null,
                "facebookImage": null,
                "tzid": "Asia/Manila",
                "hoiioNumber": null,
                "ipadAppVersion": null,
                "logoUrl": null,
                "chefRestaurant": null,
                "config": null,
                "listingUrl": null,
                "replyTo": null,
                "checkSize": null,
                "restaurantSlug": "felix",
                "ccCheckStartTimestamp": null,
                "ccCheckEndTimestamp": null,
                "ccCheckCovers": null,
                "ccCheckCoversMessage": null,
                "premiumbooking": null,
                "accessPoints": null,
                "street": "",
                "thumbnail": "https://tabledb-seats.s3.amazonaws.com/seats/rest/thumb_55233249e4b0f1bb4378cbbd.jpg",
                "enableNewletter": false,
                "disabledNoPromotion": false,
                "gmtminutesOffset": -480,
                "rating": {
                    "taste": 0,
                    "ambiance": 0,
                    "value": 0,
                    "cleanliness": 0,
                    "service": 0,
                    "general": 0
                }
            },
            {
                "id": "54e4491de4b0dcdb5bfd065f",
                "createdBy": null,
                "createdDate": 1424247069955,
                "lastModifiedBy": null,
                "lastModifiedDate": 1424658681578,
                "version": 7,
                "name": "Omakase",
                "localName": null,
                "dressCode": "Casual",
                "diningStyle": "Casual",
                "cuisines": null,
                "neighborhood": "Makati",
                "crossStreet": null,
                "menu": null,
                "price": "300",
                "website": "www.facebook.com/weloveomakase",
                "hoursOfOperation": "Mon - Sun: <br /> 11:30 am - 2:30 pm<br /> 5:30 pm - 10:00 pm",
                "paymentOptions": [
                    "Cash",
                    "Visa",
                    "MasterCard",
                    "American Express",
                    "Debit"
                ],
                "acceptWalkins": null,
                "additionalInfo": null,
                "publicTransit": null,
                "parking": null,
                "parkingDetails": "",
                "catering": null,
                "specialEventAndPromotions": null,
                "address": "Ayala Triangle Gardens, Ayala Avenue",
                "localAddress": null,
                "city": "Makati",
                "localCity": null,
                "country": "PH",
                "postalCode": "12",
                "specialties": "Sushi rolls American Dream, Deep Blue Sea, Jurassic Maki",
                "chef": "",
                "description": "The best Japanese place for unique and mouth-watering sushi in town!",
                "phone": "5516030",
                "email": "omakaseinc@yahoo.com",
                "latitude": 14.556743,
                "longitude": 121.024323,
                "images": [
                    "https://tabledb-seats.s3.amazonaws.com/seats/rest/54e4491de4b0dcdb5bfd065f/omakase1_20150223_9518.jpg",
                    "https://tabledb-seats.s3.amazonaws.com/seats/rest/54e4491de4b0dcdb5bfd065f/omakase2_20150223_1435.jpg"
                ],
                "publicTransport": "",
                "corkageDetails": "",
                "eventFacilities": "",
                "additionalDetails": [
                    "Takeaway",
                    "Non-smoking Restaurant",
                    "Alfresco",
                    "Child Friendly",
                    "Catering",
                    "Delivery",
                    "Large Groups"
                ],
                "fullPhoneNumber": "+63 5516030",
                "featured": true,
                "featuredDate": null,
                "gmtMinutesOffset": -480,
                "groupId": "54e4491de4b0dcdb5bfd065e",
                "planType": "covers",
                "reservationEmail": null,
                "facebookImage": null,
                "tzid": "Asia/Manila",
                "hoiioNumber": null,
                "ipadAppVersion": null,
                "logoUrl": null,
                "chefRestaurant": null,
                "config": null,
                "listingUrl": null,
                "replyTo": null,
                "checkSize": null,
                "restaurantSlug": "omakase",
                "ccCheckStartTimestamp": null,
                "ccCheckEndTimestamp": null,
                "ccCheckCovers": null,
                "ccCheckCoversMessage": null,
                "premiumbooking": null,
                "accessPoints": null,
                "street": "",
                "thumbnail": "https://tabledb-seats.s3.amazonaws.com/seats/rest/thumb_54e4491de4b0dcdb5bfd065f.jpg",
                "enableNewletter": false,
                "disabledNoPromotion": false,
                "gmtminutesOffset": -480,
                "rating": {
                    "taste": 0,
                    "ambiance": 0,
                    "value": 0,
                    "cleanliness": 0,
                    "service": 0,
                    "general": 0
                }
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message" : "query failed" 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>