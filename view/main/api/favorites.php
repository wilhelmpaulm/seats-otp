<div id="_favorites" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET ALL FAVORITES</label>
        <div class="well">
            <p>get them favorites</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/favorites
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>favorites are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id": "1",
                "created_at": "2015-08-12 03:47:41",
                "updated_at": null,
                "status": "pending",
                "type": "feedback",
                "id_raw_user": "55c85402e4b0dab9ae4da428",
                "id_raw_reservation": "00000022222",
                "id_raw_restaurant": "000001111",
                "id_user": "2",
                "taste": "2",
                "ambiance": null,
                "value": "4",
                "cleanliness": "5",
                "service": "2",
                "general": "3",
                "comments": "comment",
                "review": null
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message": "query failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">GET USER FAVORITES</label>
        <div class="well">
            <p>get a single user's favorites favorites</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/favorites
                <hr/>
                {URL}/users/+639279655572/favorites
            </code>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>user's favorites are returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>query failed</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>user does not exist</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            [
                {
                    "id": "1",
                    "created_at": "2015-08-12 10:11:29",
                    "updated_at": null,
                    "status": "active",
                    "type": "favorite",
                    "id_raw_user": "55c85402e4b0dab9ae4da428",
                    "id_raw_restaurant": "0001111",
                    "id_user": "2",
                    "comments": "'this is my comment'"
                },
                {
                    "id": "2",
                    "created_at": "2015-08-12 10:11:42",
                    "updated_at": null,
                    "status": "active",
                    "type": "favorite",
                    "id_raw_user": "55c85402e4b0dab9ae4da428",
                    "id_raw_restaurant": "0001111",
                    "id_user": "2",
                    "comments": "this is my comment"
                },
                {
                    "id": "3",
                    "created_at": "2015-08-12 10:15:17",
                    "updated_at": null,
                    "status": "active",
                    "type": "favorite",
                    "id_raw_user": "55c85402e4b0dab9ae4da428",
                    "id_raw_restaurant": "0001111",
                    "id_user": "2",
                    "comments": "this is my comment"
                }
            ]
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404", 
        "message": "query failed", 
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">ADD FAVORITE</label>
        <div class="well">
            <p>add favorites</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/favorites/add
                <hr/>
                {URL}/users/+639279655572/favorites/add
                ?id_raw_restaurant=0001111&comments=comment
            </code>
        </div>
        <label>ATTRIBUTES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>id_raw_restaurant</td>
                        <td><small>string</small></td>
                        <td><code>0001111</code></td>
                    </tr>
                    <tr>
                        <td>comments</td>
                        <td><small>string</small></td>
                        <td><code>this is my comment</code></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>favorite is returned</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>action failed</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>user does not exist</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>favorite already exist</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "data": [
            {
                "id_raw_user": "55c85402e4b0dab9ae4da428",
                "id_raw_restaurant": "0001111",
                "id_user": "2",
                "comments": "this is my comment"
            }
        ]
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message: : "query failed"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>
<div id="" class="row pb">
    <div class="col-md-6">
        <label class="c-sunflower">DELETE FAVORITE</label>
        <div class="well">
            <p>delete favorites</p>
        </div>
        <label>METHOD</label>
        <div class="well">
            <code>
                GET
            </code>
        </div>
        <label>URL</label>
        <div class="well">
            <code>
                {URL}/users/{mobile}/favorites/{restaurant_id}/delete
                <hr/>
                {URL}/users/+639279655572/favorites/0001111/delete
            </code>
        </div>

        <label>STATUS CODES</label>
        <div class="">
            <table class="table table-bordered table-striped well">

                <tbody>
                    <tr>
                        <td>200</td>
                        <td>Favorite removed successfully</td>
                    </tr>
                    <tr>
                        <td>404</td>
                        <td>Restaurant not yet in favorites</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>No user with that mobile number was found</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <label>SUCCESS</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "200",
        "message": "Favorite removed successfully"
    }
}
                </pre>
            </div>
        </div>
        <div class="">
            <label>FAIL</label>
            <div>
                <pre class="well">
{
    "response": {
        "status": "404",
        "message": "Restaurant not a favorite yet"
    }
}
                </pre>
            </div>
        </div>
    </div>
</div>