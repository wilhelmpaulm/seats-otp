<?php include linkPage("template/header"); ?>
<div id="" class="">
    <br/>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1">
                <label class="c-sunflower">CHECK OTP</label>
                <div class="well">
                    <div class="form-group">
                        <form action="<?= linkTo("")?>" method="POST">
                            <label class="control-label">Mobile Number</label>
                            <div class="input-group">
                                <span class="input-group-addon">+63</span>
                                <input type="number" min="9000000000" max="9999999999" name="mobile" value="<?= $mobile ?>" required=""  class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-primary" type="submit">Check OTP</button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <hr>
                    <?php if (count($otps) > 0): ?>
                        <div class="text-center">
                            <label class="control-label">Available OTP/s</label>
                            <?php foreach ($otps as $otp): ?>
                                <p class=""><?= $otp["code"] ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php else: ?>
                        <div class="text-center">
                            <label class="control-label">No Available OTPs</label>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage("template/footer"); ?>