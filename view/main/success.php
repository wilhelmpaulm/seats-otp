<?php include linkPage("template/header"); ?>
<div id="">

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="">
                        <div class="well well-sm text-center">
                            REGISTRATION SUCCESS
                            <script type="text/javascript">
                                if (window != top) {
                                    top.location.href = <?= sendTo("back") ?>;
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<?php include linkPage("template/footer"); ?>