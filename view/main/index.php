<?php include_once linkPage("template/index_header"); ?>

<body class="skin-black layout-boxed">
    <div class="wrapper">


        <!-- Left side column. contains the logo and sidebar -->
        <?php include_once linkPage("header/index_header"); ?>
        <?php include_once linkPage("sidebar/index_sidebar"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Home
                    <small> </small>
                </h1>
                <ol class="breadcrumb hidden">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Tables</a></li>
                    <li class="active">Data tables</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">

                                </div>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->


                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
        <?php include_once linkPage("footer/index"); ?>
    </div><!-- ./wrapper -->

    <!-- page script -->

</body>
<script>
    activeSide("Home");
</script>
<?php include_once linkPage("template/footer"); ?>
