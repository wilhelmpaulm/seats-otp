<?php include_once linkPage("template/index_header"); ?>

<body class="skin-black layout-boxed ">
    <div class="wrapper">


        <!-- Left side column. contains the logo and sidebar -->
        <?php include_once linkPage("header/index_header"); ?>
        <?php include_once linkPage("sidebar/index_sidebar"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Register
                    <small></small>
                </h1>
                <ol class="breadcrum hidden">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Tables</a></li>
                    <li class="active">Data tables</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <!--<h3 class="box-title">Hover Data Table</h3>-->
                            </div><!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3">
                                            <form action="<?= linkTo("register") ?>" method="post">
                                                <div class="row">
                                                    <div class="form-group has-feedback col-xs-6">
                                                        <input type="text"  name="first_name" class="form-control " placeholder="First name"/>
                                                    </div>
                                                    <div class="form-group has-feedback col-xs-6">
                                                        <input type="text"  name="last_name" pattern=".{2,}" class="form-control " placeholder="Last name"/>
                                                    </div>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <input type="text" name="contact_number" class="form-control" placeholder="Contact Number"/>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <input type="email" name="email" class="form-control" placeholder="Email"/>
                                                </div>

                                                <div class="form-group has-feedback">
                                                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-7 col-xs-offset-1">    
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"> I agree to the <a href="#">terms.</a>
                                                            </label>
                                                        </div>                        
                                                    </div><!-- /.col -->
                                                    <div class="col-xs-4">
                                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                                                    </div><!-- /.col -->
                                                </div>
                                            </form>        

                                            <div class="social-auth-links text-center">
                                                <!--<p> OR </p>-->
                                                <hr>
                                                <span class="btn-group btn-group-justified">
                                                    <a href="<?= linkTo("register/fb") ?>" class="btn btn-block btn-facebook btn-flat"><i class="fa fa-facebook"></i></a>
                                                    <a href="#" class="btn btn-block btn-twitter btn-flat"><i class="fa fa-twitter"></i></a>
                                                    <a href="#" class="btn btn-block btn-google-plus btn-flat"><i class="fa fa-google-plus"></i></a>
<!--                                                    <a href="#" class="btn btn-block btn-tumblr btn-flat"><i class="fa fa-tumblr"></i></a>
                                                    <a href="#" class="btn btn-block btn-instagram btn-flat"><i class="fa fa-instagram"></i></a>-->
                                                    <!--<br>-->
                                                </span>
                                                <hr>
                                                <span class="btn-group btn-group-justified">
                                                    <a href="login.html" class="btn btn-block btn-warning btn-flat text-center">I'm already a user</a>
                                                </span>
                                                <!--<a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->


                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
        <?php include_once linkPage("footer/index"); ?>
    </div><!-- ./wrapper -->

    <!-- page script -->

</body>
<script>
    activeSide("Register");
</script>
<?php include_once linkPage("template/footer"); ?>
