<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <span class="admin-logo">
            <img src="<?= linkPublic("admin/images/seat-admin-logo.png")?>" />
        </span>
    </div>
    <!-- Top Menu Items -->
    <div class="search-bar navbar-left">
        <input type="text" class="top-search" name="search" placeholder="Search" value="">                  
    </div>
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>   <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?= linkTo("admin/logout") ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class=" ">
                <a href="<?= linkTo("admin/restaurants") ?>"><i class="fa fa-cutlery"></i> Restaurants</a>
            </li>
            <li class="">
                <a href="<?= linkTo("admin/tags") ?>"><i class="fa fa-cutlery"></i> Tags</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>  