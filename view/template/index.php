<?php include linkPage("template/header"); ?>
<div id="wrapper" class="hidden-print">

    <!-- Sidebar -->
    <?php include linkPage("template/sidebar"); ?>
    <!-- /#sidebar-wrapper -->
    <!--<a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>-->


    <!-- Page Content -->
    <div id="page-content-wrapper">
        <br/>
        <div class="row">
            <div class="col-md-12">
                <div class="" style="min-height: 24em">
                    <table class="table dtable table-bordered  table-condensed table-striped">
                        <thead>
                            <tr>
                                <th width='1%'>ID</th>
                                <th width='15%'>Date Publish</th>
                                <th width='15%'>Date End</th>
                                <th width='15%'>Recipient</th>
                                <th>Message</th>
                                <th width='1%'></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ([] as $b): ?>
                                <tr>
                                    <td class="text-center"> <?= $b["id"] ?></td>
                                    <td> <?= date_format(date_create($b["date_publish"]), "F d, Y") ?></td>
                                    <td> <?= date_format(date_create($b["date_publish_end"]), "F d, Y") ?></td>
                                    <td> <?= $b["barangays"] ?></td>
                                    <td>
                                        <p>
                                            <?= substr($b["message"], 0, 130) ?> 
                                        </p>
                                    </td>
                                    <td>
                                        <div class="pside20">
                                            <div><a href="<?= linkTo("broadcasts/{$b['id']}/delete") ?>" class="c-red remove"><i class="fa fa-minus-circle"></i></a></div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->

</div>
<?php include linkPage("template/footer"); ?>