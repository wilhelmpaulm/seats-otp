<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<!DOCTYPE html>
<html lang="en">

    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="http://<?= getInit('localhost') ?>/public/favicon.png">

        <title>SEATS DEV OTP CHECKER</title>

        <?php if (getInit("status") == "online"): ?>
            <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
            <link href="http://<?= getInit('localhost') ?>/public/assets/css/wilhelmpaulm.css" rel="stylesheet"/>

            <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
            <script src="http://<?= getInit('localhost') ?>/public/assets/js/moment.js"></script>
            <script src="http://<?= getInit('localhost') ?>/public/assets/js/bootstrap.min.js"></script>
        <?php elseif (getInit("status") == "offline"): ?>
            <link href="http://<?= getInit('localhost') ?>/public/assets/css/bootstrap.min.css" rel="stylesheet"/>
            <link href="http://<?= getInit('localhost') ?>/public/assets/css/font-awesome.min.css" rel="stylesheet"/>
            <link href="http://<?= getInit('localhost') ?>/public/assets/css/wilhelmpaulm.css" rel="stylesheet"/>

            <script src="http://<?= getInit('localhost') ?>/public/assets/js/jquery.js"></script>
            <script src="http://<?= getInit('localhost') ?>/public/assets/js/moment.js"></script>
            <script src="http://<?= getInit('localhost') ?>/public/assets/js/bootstrap.min.js"></script>
        <?php endif; ?>
    </head>
