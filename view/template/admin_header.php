<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<!DOCTYPE html>
<html lang="en">
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="http://<?= getInit('localhost') ?>/public/favicon.png">

        <title>Seats API</title>
        <!--<meta content="width=device-width, initial-scale=1, maximum-scale=1,    user-scalable=no" name="viewport">-->

        <link href="http://<?= getInit('localhost') ?>/public/assets/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="http://<?= getInit('localhost') ?>/public/assets/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="http://<?= getInit('localhost') ?>/public/assets/packages/DataTables/css/dataTables.bootstrap.css" rel="stylesheet"/>
        <link href="http://<?= getInit('localhost') ?>/public/assets/css/parsley.css" rel="stylesheet"/>
        <link href="http://<?= getInit('localhost') ?>/public/assets/css/wilhelmpaulm.css" rel="stylesheet"/>
        <link href="http://<?= getInit('localhost') ?>/public/assets/css/sb-admin.css" rel="stylesheet"/>
        <link href="http://<?= getInit('localhost') ?>/public/assets/css/admin.css" rel="stylesheet"/>

        <script src="http://<?= getInit('localhost') ?>/public/assets/js/jquery.js"></script>
        <script src="http://<?= getInit('localhost') ?>/public/assets/js/bootstrap.min.js"></script>
        <script src="http://<?= getInit('localhost') ?>/public/assets/js/parsley.min.js"></script>
        <script src="http://<?= getInit('localhost') ?>/public/assets/packages/DataTables/js/dataTables.js"></script>
        <script src="http://<?= getInit('localhost') ?>/public/assets/packages/DataTables/js/dataTables.bootstrap.js"></script>
        <script src="http://<?= getInit('localhost') ?>/public/assets/js/holder.js"></script>


        <style>

/*            input, textarea, button, span, div, nav, a {
                border-radius: 0!important;
            }*/

            .accordion, .accordion * {
                border:none;
            }
            a {
                text-decoration: none;   
            }
            a:hover {
                text-decoration: none;   
            }

            .bootstrap-datetimepicker-widget table td.active,
            .bootstrap-datetimepicker-widget table td.active:hover {
                background-color: #00ABA9;
                background-repeat: no-repeat;
                color: #ffffff;
                text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            }
            .bootstrap-datetimepicker-widget table td.day {
                height: 20px;
                line-height: 20px;
                width: 20px;
            }
            .bootstrap-datetimepicker-widget table td span.active {
                background-color: #00ABA9;
                color: #ffffff;
                text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            }
            .panel-heading , .modal-header {
                background-color: #333333!important;
                color: white;
            } 

/*            .modal-content {
                height: 100vh;
            }*/
            .panel-heading span {
                color: white;
            } 
            .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
                border-color: #ffba0f;
                background: #ffba0f;
            }
            .bootstrap-datetimepicker-widget table td span.active,
            .bootstrap-datetimepicker-widget table td.active, .bootstrap-datetimepicker-widget table td.active:hover,
            .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
                background: #ffba0f;
            }
        </style>
        <script>
            $("document").ready(function () {
                $(".dtable").dataTable();
                
                
            });
        </script>



    </head>
