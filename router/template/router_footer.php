<?php

$match = $router->match();

if ($match && is_callable($match['target'])) {
    call_user_func_array($match['target'], $match['params']);
} else if ($match) {
    require $match['target'];
} else {
    header("HTTP/1.0 404 Not Found");
    sendTo("");
//    echo '404.php';
}
  
