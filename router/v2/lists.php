<?php

//FORUMS

$router->map('GET', '/lists', function() {
    header('Content-Type: application/json');
    $data = getListDefaults2();
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});

$router->map('GET', '/lists/[:name]', function($name) {
    header('Content-Type: application/json');
    $data = getListDefaults2($name);
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});
