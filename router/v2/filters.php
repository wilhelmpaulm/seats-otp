<?php

//FORUMS

$router->map('GET', '/filters', function() {
    header('Content-Type: application/json');
    date_default_timezone_set('Asia/Manila');
    $get = [];
    $list_occasions = getListDefaults2("occasions");
    $list_cuisine = getListDefaults2("cuisine");
    $list_neighborhood = getListDefaults2("neighborhood");
    $list_sort = getListDefaults2("sort");
    $list_additional_details = getListDefaults2("additional_details");
    $list_tags = getListDefaults2("tags");

    $get["sort"] = str_replace(" ", "", getGet("sort"));
    if (getGet("neighborhood") != null && getGet("neighborhood") != "any") {
        $neighborhood_temp = explode(",", str_replace(" ", "", getGet("neighborhood")));
        $get["neighborhood"] = [];
        foreach ($neighborhood_temp as $nt) {
            array_push($get["neighborhood"], $list_neighborhood[$nt]);
        }
    } else {
        $get["neighborhood"] = null;
    }
    if (getGet("cuisine") != null && getGet("cuisine") != "any") {
        $get["cuisine"] = explode(",", str_replace(" ", "", getGet("cuisine")));
    } else {
        $get["cuisine"] = null;
    }
    if (getGet("occasions") != null && getGet("occasions") != "any") {
        $get["occasions"] = explode(",", str_replace(" ", "", getGet("occasions")));
    } else {
        $get["occasions"] = null;
    }
    if (getGet("additional_details") != null && getGet("additional_details") != "any") {
        $get["additional_details"] = explode(",", str_replace(" ", "", getGet("additional_details")));
    } else {
        $get["additional_details"] = null;
    }
    if (getGet("tags") != null && getGet("tags") != "any") {
        $get["tags"] = explode(",", str_replace(" ", "", getGet("tags")));
    } else {
        $get["tags"] = null;
    }

    $filters = [
        "type" => getGet("type") ? getGet("type") : "all",
        "number_people" => getGet("number_people") ? getGet("number_people") : 2,
        "mobile" => getGet("mobile") ? getGet("mobile") : null,
        "sort" => $get["sort"] ? $get["sort"] : 0,
        "neighborhood" => $get["neighborhood"] ? $get["neighborhood"] : "any",
        "cuisine" => $get["cuisine"] ? $get["cuisine"] : "any",
        "occasions" => $get["occasions"] ? $get["occasions"] : "any",
        "tags" => $get["tags"] ? $get["tags"] : "any",
        "additional_details" => $get["additional_details"] ? $get["additional_details"] : "any",
        "price_min" => getGet("price_min") ? getGet("price_min") : 0,
        "price_max" => getGet("price_max") ? getGet("price_max") : 5000,
        "rating" => getGet("rating") ? getGet("rating") : 0,
        "distance" => getGet("distance") ? getGet("distance") : 1000,
        "text" => getGet("text") ? getGet("text") : null,
        "time" => getGet("time") ? getGet("time") : date("Hi"),
        "lat" => getGet("lat") ? getGet("lat") : 0.0,
        "long" => getGet("long") ? getGet("long") : 0.0,
        "date" => getGet("date") ? getGet("date") : substr(currentdatetime(), 0, 10),
        "date_strict" => getGet("date_strict") ? getGet("date_strict") : "false"
    ];
    $i = 0;
    if ($filters["additional_details"] != "any") {
        for ($i = 0; $i < count($filters["additional_details"]); $i++) {
            $filters["additional_details"][$i] = $list_additional_details[$filters["additional_details"][$i]];
        }
    }
    $i = 0;
    if ($filters["occasions"] != "any") {
        for ($i = 0; $i < count($filters["occasions"]); $i++) {
            $filters["occasions"][$i] = $list_occasions[$filters["occasions"][$i]];
        }
    }
    $i = 0;
    if ($filters["cuisine"] != "any") {
        for ($i = 0; $i < count($filters["cuisine"]); $i++) {
            $filters["cuisine"][$i] = $list_cuisine[$filters["cuisine"][$i]];
        }
    }
    $i = 0;
    if ($filters["tags"] != "any") {
        for ($i = 0; $i < count($filters["tags"]); $i++) {
            $filters["tags"][$i] = $list_tags[$filters["tags"][$i]];
        }
    }
    $restaurants_old = getRestaurantsNew();
//    $details = selectTable("restaurants");
    $restaurants_new = [];
    foreach ($restaurants_old as $res) {
//        $sta = false;
        $sta = true;
//        foreach ($details as $lres) {
//            if ($lres["id_raw_restaurant"] == $res->id) {
//                if ($lres["status"] == "live") {
//                    $sta = true;
//                    break;
//                }
//            }
//        }
        if ($sta) {
            if ($filters["type"] == "all"):
                array_push($restaurants_new, $res);
            elseif ($filters["type"] == "bookable" && $res->status == "active"):
                array_push($restaurants_new, $res);
            elseif ($filters["type"] == "listing" && $res->status != "active"):
                array_push($restaurants_new, $res);
            endif;
        }
    }
    $restaurants_old = [];
    $restaurants_old = $restaurants_new;


    $restaurants = [];
    if ($filters["text"] != null) {
        foreach ($restaurants_old as $ro) {
            $text = str_replace(" ", "|", getGet("text"));
            $text = str_replace(",", "|", $text);
            $text = str_replace("-", "|", $text);
            $reg = "/";
            foreach (explode("|", $text) as $t) {
                $reg .= '(?=.*?' . $t . ")";
            }
            $reg .= "^.*$/i";
            $pool = $ro->name . " " . $ro->specialties . " " . $ro->neighborhood;
            if (preg_match($reg, $pool)) {
                array_push($restaurants, $ro);
            }
        }
    } else {
        $restaurants = $restaurants_old;
    }

    $restaurants_old = $restaurants;
    $restaurants = [];
    foreach ($restaurants_old as $ro) {
        if ($ro->rating->general >= $filters["rating"] && $ro->price >= $filters["price_min"] && $ro->price <= $filters["price_max"]) {
            if ($filters["neighborhood"] == "any") {
                array_push($restaurants, $ro);
            } else {
                if (in_array($ro->neighborhood, $filters["neighborhood"])) {
                    array_push($restaurants, $ro);
                }
            }
        }
    }


    if ($filters["cuisine"] != "any") {
        $restaurants_old = $restaurants;
        $restaurants = [];

        foreach ($restaurants_old as $ro) {
            $cuisines_in_restaurant = $ro->cuisine;
            foreach ($cuisines_in_restaurant as $cir) {
                if (in_array($cir, $filters["cuisine"])) {
                    array_push($restaurants, $ro);
                    break;
                }
            }
        }
    }
    if ($filters["occasions"] != "any") {
        $restaurants_old = $restaurants;
        $restaurants = [];

        foreach ($restaurants_old as $ro) {
            $occasions_in_restaurant = $ro->occasions;
            foreach ($occasions_in_restaurant as $cir) {
                if (in_array($cir, $filters["occasions"])) {
                    array_push($restaurants, $ro);
                    break;
                }
            }
        }
    }



    $restaurants_old = $restaurants;
    $restaurants = [];

    foreach ($restaurants_old as $ro) {
        if ($filters["additional_details"] == "any") {
            array_push($restaurants, $ro);
        } else {
            $additional_details_in_restaurant = $ro->additionalDetails;
            foreach ($additional_details_in_restaurant as $adir) {
                if (in_array($adir, $filters["additional_details"])) {
                    if (!in_array($ro, $restaurants)) {
                        array_push($restaurants, $ro);
                    }
                }
            }
        }
    }
    $restaurants_old = $restaurants;
    $restaurants = [];

    foreach ($restaurants_old as $ro) {
        if ($filters["tags"] == "any") {
            array_push($restaurants, $ro);
        } else {
            $tags_in_restaurant = $ro->tags;
            foreach ($tags_in_restaurant as $tir) {
                if (in_array($tir, $filters["tags"])) {
                    if (!in_array($ro, $restaurants)) {
                        array_push($restaurants, $ro);
                    }
                }
            }
        }
    }

    $restaurants_old = $restaurants;
    $restaurants = [];

    foreach ($restaurants_old as $ro) {
        if ($filters["lat"] != null && $filters["lat"] != null) {
            $distance = getDistance($ro->latitude, $ro->longitude, $filters["lat"], $filters["long"]);
            if ($distance <= $filters["distance"]) {
                $ro->distance = $distance . " meters";
                array_push($restaurants, $ro);
            }
        } else {
            $ro->distance = null;
            array_push($restaurants, $ro);
        }
    }
    if ($filters["lat"] != null && $filters["lat"] != null) {
        usort($restaurants, function($a, $b) {
            return $a->distance > $b->distance;
        });
    }

    $restaurants_old = $restaurants;
    $restaurants = [];

    foreach ($restaurants_old as $ro) {
        if ($filters["date_strict"] == "true") {
            $avaliability = curlyGet(getInit("api_url") . "tabledb-web/middlelayer/availability/{$filters["date"]}/{$ro->id}");
            $avaliability = json_decode($avaliability)->response->data ? json_decode($avaliability)->response->data : [];
            if (count($avaliability) > 0) {
                usort($avaliability, function($a, $b) {
                    return $a->timeSlotId > $b->timeSlotId;
                });
            }
            $avaliability_new = [];
            if (count($avaliability) > 0) {
                foreach ($avaliability as $av) {
                    if ($av->timeSlotId . "" >= $filters["time"] && $av->remainingTimeSlotCapacity >= $filters["number_people"] && $av->maxNumberOfSeatsForSingleReservation >= $filters["number_people"]) {
                        array_push($avaliability_new, $av);
                    }
                }
            }
            $ro->availability = $avaliability_new;
            if (count($avaliability_new) > 0) {
                foreach ($avaliability_new as $av) {
                    if ($av->timeSlotId . "" >= $filters["time"] && $av->remainingTimeSlotCapacity >= $filters["number_people"] && $av->maxNumberOfSeatsForSingleReservation >= $filters["number_people"]) {
                        array_push($restaurants, $ro);
                        break;
                    }
                }
            }
        } else {
            $avaliability = json_decode(json_encode(getAvailabilities($ro->id, $filters["date"])));
//            $avaliability = json_decode($avaliability)->response->data ? json_decode($avaliability)->response->data : [];
            $avaliability_new = [];
            if (count($avaliability) > 0) {
                usort($avaliability, function($a, $b) {
                    return $a->timeSlotId > $b->timeSlotId;
                });
                foreach ($avaliability as $av) {
                    if ($av->timeSlotId . "" >= $filters["time"] && $av->remainingTimeSlotCapacity >= $filters["number_people"] && $av->maxNumberOfSeatsForSingleReservation >= $filters["number_people"]) {
                        array_push($avaliability_new, $av);
                    }
                }
            }
            $ro->availability = $avaliability_new;
            array_push($restaurants, $ro);
        }
    }

    if (substr(currentdatetime(), 0, 10) == $filters["date"]) {
        $restaurants_old = $restaurants;
        $restaurants = [];

        foreach ($restaurants_old as $ro) {
            $availability_new = [];
            foreach ($ro->availability as $av) {
                if ($av->timeSlotId . "" >= $filters["time"]) {
                    array_push($availability_new, $av);
                }
            }
            $ro->availability = [];
            usort($availability_new, function($a, $b) {
                return $a->timeSlotId > $b->timeSlotId;
            });
            $ro->availability = $availability_new;
            array_push($restaurants, $ro);
        }
    }


    $user = getUsers(urlencode($filters["mobile"] ? $filters["mobile"] : "false"));
    if ($user) {
        $favorites = selectTable2("favorites", ["id_user_raw" => $user["id_raw_user"]]);
        $restaurants_old = $restaurants;
        $restaurants = [];
        foreach ($restaurants_old as $ro) {
            $favorite = false;
            foreach ($favorites as $fa) {
                if ($fa["id_restaurant_raw"] == $ro->id) {
                    $favorite = true;
                    break;
                }
            }
            $ro->favorite = $favorite;
            array_push($restaurants, $ro);
        }
    } else {
        $restaurants_old = $restaurants;
        $restaurants = [];
        foreach ($restaurants_old as $ro) {
            $favorite = false;
            $ro->favorite = $favorite;
            array_push($restaurants, $ro);
        }
    }

    $restaurants_old = [];
    $restaurants_old = $restaurants;


    switch ($get["sort"]) {
        case 1:
            usort($restaurants_old, function($a, $b) {
                return $a->price > $b->price;
            });
            break;
        case 2:
            usort($restaurants_old, function($a, $b) {
                return $a->price < $b->price;
            });
            break;
        case 3:
            usort($restaurants_old, function($a, $b) {
                return $a->name > $b->name;
            });
            break;
        case 4:
            usort($restaurants_old, function($a, $b) {
                return $a->name < $b->name;
            });
            break;
        case 5:
            usort($restaurants_old, function($a, $b) {
                return $a->rating->general > $b->rating->general;
            });
            break;
        case 6:
            usort($restaurants_old, function($a, $b) {
                return $a->rating->general < $b->rating->general;
            });
            break;
        default:
            break;
    }

    $data = $restaurants_old;
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query did not return any results"]]);
    }
});
