<?php

//FORUMS

$router->map('GET', '/tags', function() {
    header('Content-Type: application/json');
    $tags = selectTable2("details", ["type" => "tag"]);
    $tag_list = [];
    foreach ($tags as $t) {
        if ($t["status"] == "active" && $t["name"] != "none"):
            array_push($tag_list, [
                "index" => $t["index"],
                "name" => $t["name"],
                "image" => $t["thumbnail"]
                    ]
            );
        endif;
    }
    $data = $tag_list;
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});
