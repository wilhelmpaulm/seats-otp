<?php

//FORUMS
$router->map('GET', '/users', function() {
    global $database2;
    header('Content-Type: application/json');
    $users = getUsers();
    echo $users ? json_encode(["response" => ["status" => "200", "data" => $users]]) : json_encode(["response" => ["status" => "404", "message" => "No users"]]);
});

$router->map('GET', '/users/[:mobile]/edit', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $data = [
            "password" => getGet("password") ? getGet("password") : $user["password"],
            "last_name" => getGet("last_name") ? getGet("last_name") : $user["last_name"],
            "first_name" => getGet("first_name") ? getGet("first_name") : $user["first_name"],
            "middle_name" => getGet("middle_name") ? getGet("middle_name") : $user["middle_name"],
            "designation" => getGet("designation") ? getGet("designation") : $user["designation"],
            "birthdate" => getGet("birthdate") ? getGet("birthdate") : $user["birthdate"],
            "gender" => getGet("gender") ? getGet("gender") : $user["gender"],
            "email" => getGet("email") ? getGet("email") : $user["email"],
            "thumbnail" => getGet("image") ? getGet("image") : $user["image"],
            "fb_id" => getGet("fb_id") ? getGet("fb_id") : $user["fb_id"],
            "fb_link" => getGet("fb_link") ? getGet("fb_link") : $user["fb_link"],
            "gcm" => getGet("gcm") ? getGet("gcm") : $user["gcm"],
            "status" => "active"
        ];
        if ($data["fb_id"]):
            $data["thumbnail"] = "https://graph.facebook.com/" . $data["fb_id"] . "/picture?type=normal";
        endif;
        updateTable2("users", $data, $user["id"]);
        if (getGet("mobile")):
            updateTable2("users", ["mobile" => urlencode(getGet("mobile"))], $user["id"]);
        endif;
        $user = getUsers(urlencode($mobile));
        echo json_encode(["response" => [
                "status" => "200",
                "data" => [$user]
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/add', function() {
    header('Content-Type: application/json');
    $mobile = urlencode(getGet("mobile"));
    $user = [
        "status" => "pending",
        "last_name" => getGet("last_name"),
        "first_name" => getGet("first_name"),
        "middle_name" => getGet("middle_name"),
        "designation" => getGet("designation"),
        "birthdate" => strlen(getGet("birthdate")) == 10 ? getGet("birthdate") . " 00:00:00" : getGet("birthdate"),
        "gender" => getGet("gender"),
        "email" => getGet("email"),
        "gcm" => getGet("gcm"),
//        "fb_id" => getGet("fb_id"),
        "fb_link" => getGet("fb_link"),
        "thumbnail" => getGet("image"),
        "mobile" => $mobile,
    ];
    if (getGet("fb_id")):
//        $user["thumbnail"] = "https://graph.facebook.com/" . getGet("fb_id") . "/picture?type=normal";
    endif;
    $old_user = null;

    if ($mobile != null) {
        $old_user = getTable2("users", ["mobile" => $mobile]);
    }
    if (!$old_user) {
        $user["mobile"] = $mobile;
        $user_id = insertTable2("users", $user);
        $user = getTable2("users", $user_id);
    } else {
        if (getGet("fb_id") && $old_user):
            updateTable2("users", $user, $old_user["id"]);
            $user = getTable2("users", $old_user["id"]);
        else:
            $user = $old_user;
            $message = "Mobile number has already been registered, please try to log in";
            echo json_encode(["response" => [
                    "status" => "404",
                    "message" => $message,
            ]]);
            die;
        endif;
    }
    $data = [
        "recipient" => $user["mobile"],
        "expiryDate" => null,
        "code" => null
    ];
    $data = json_encode($data);
    $response = sendOTP(urlencode(getGet("mobile")), getGet("fb_id") ? getGet("fb_id") : null);
    $id_raw_user = $response;

    if (empty($user["id_raw_user"])) {
        $data = [
            "id_raw_user" => $id_raw_user,
            "id_raw_user_temp" => $id_raw_user,
        ];
    } else {
        $data = [
            "id_raw_user_temp" => $id_raw_user,
        ];
    }
    updateTable2("users", $data, $user["id"]);
    $user = getTable2("users", $user["id"]);
    if ($user) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => [$user]
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Adding user failed"]]);
    }
});

$router->map('GET', '/users/[:mobile]', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => [$user]
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/delete', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        deleteTable2("users", $user["id"]);
        echo json_encode(["response" => [
                "status" => "200",
                "message" => "User deleted",
                "data" => [$user]
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:id]/fblogin', function($id) {
    header('Content-Type: application/json');
    $users = getUsers();
    $user = null;
    if ($users) {
        foreach ($users as $key => $val):
            if ($val["fb_id"] == $id):
                $user = $val;
                break;
            endif;
        endforeach;
        if ($user):
            echo json_encode(["response" => [
                    "status" => "200",
                    "data" => [$user]
            ]]);
        else:
            echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
        endif;
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No users found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/login', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $data = [
            "recipient" => $user["mobile"],
            "expiryDate" => null,
            "code" => null
        ];
        $data = json_encode($data);
        $response = sendOTP($user["mobile"]);
        $id_raw_user = $response;
        if (empty($user["id_raw_user"])) {
            $data = [
                "id_raw" => $id_raw_user,
                "id_raw_temp" => $id_raw_user,
            ];
        } else {
            $data = [
                "id_raw_temp" => $id_raw_user,
            ];
        }
        $data = updateTable2("users", $data, $user["id"]);
        echo json_encode(["response" => ["status" => "200", "message" => "OTP sent"]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/login/[:code]', function($mobile, $code) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $id_raw_user_temp = $user["id_raw_user_temp"];
        $code = urlencode($code);
        $response = confirmOTP($user["mobile"], $code);
        if ($response) {
            $data = [];
            $data["code"] = $code;
            $data["status"] = "active";
            updateTable2("users", $data, $user["id"]);
            $user = getUsers($user["id"]);
            echo json_encode(["response" => [
                    "status" => "200",
                    "data" => [$user]
            ]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "The code was invalid"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/ratings/add', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    $id_restaurant = getGet("id_raw_restaurant") ? getTable2("restaurants", ["id_raw" => getGet("id_raw_restaurant")])["id"] : null;
    if ($user) {
        $data = [
            "type" => "feedback",
            "status" => "pending",
            "id_user_raw" => $user["id_raw_user"],
            "id_restaurant" => $id_restaurant,
            "id_restaurant_raw" => getGet("id_raw_restaurant"),
            "id_reservation_raw" => getGet("id_raw_reservation"),
            "id_user" => $user["id"],
            "taste" => getGet("taste") ? getGet("taste") : 3,
            "value" => getGet("value") ? getGet("value") : 3,
            "cleanliness" => getGet("cleanliness") ? getGet("cleanliness") : 3,
            "service" => getGet("service") ? getGet("service") : 3,
            "ambience" => getGet("ambience") ? getGet("ambience") : 3,
            "general" => getGet("general") ? getGet("general") : 3,
            "review" => getGet("comments"),
        ];
        $id_rating = insertTable2("ratings", $data);
        $rating = getTable2("ratings", $id_rating);
        echo json_encode(["response" => [
                "status" => "200",
                "data" => [$rating]
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/ratings', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $data = getRatings();
        $ratings = [];
        foreach ($data as $key => $val):
            if ($val["id_raw_user"] == $user["id_raw_user"]):
                array_push($ratings, $val);
            endif;
        endforeach;
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/favorites/add', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    $favorite = getTable2("favorites", ["AND" => ["id_restaurant_raw" => getGet("id_raw_restaurant"), "id_user_raw" => $user["id_raw_user"]]]);
    if (!$favorite) {
        if (getRestaurantsNew(getGet("id_raw_restaurant"))) {
            if ($user) {
                $data = [
                    "id_user_raw" => $user["id_raw_user"],
                    "id_restaurant_raw" => getGet("id_raw_restaurant"),
                    "id_user" => $user["id"],
                    "comments" => getGet("comments"),
                ];
                insertTable2("favorites", $data);
                echo json_encode(["response" => [
                        "status" => "200",
                        "data" => [$data]
                ]]);
            } else {
                echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
            }
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Restaurant does not exist"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Favorite already exist"]]);
    }
});

$router->map('GET', '/users/[:mobile]/favorites', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $data = selectTable2("favorites", ["id_user_raw" => $user["id_raw_user"]]);
        $restaurants = getRestaurantsNew();
        $data_old = $data ? $data : [];
        $data = [];
        foreach ($data_old as $d) {
            $restaurant = null;
            foreach ($restaurants as $r) {
                if ($d["id_restaurant_raw"] == $r->id) {
                    $restaurant = $r;
                    break;
                }
            }
            $d["id_raw_restaurant"] = $r->id;
            $d["restaurant"] = $restaurant;
            if ($restaurant) :
                array_push($data, $d);
            endif;
        }
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/favorites/[:id]', function($mobile, $id) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $data = selectTable2("favorites", ["id_user_raw" => $user["id_raw_user"]]);
        $restaurants = getRestaurantsNew($id);
        $data_old = $data ? $data : [];
        $data = [];
        foreach ($data_old as $d) {

            $restaurant = null;
            foreach ($restaurants as $r) {
                if ($d["id_restaurant_raw"] == $r->id) {
                    $restaurant = $r;
                    break;
                }
            }
            $d["id_raw_restaurant"] = $r->id;
            $d["restaurant"] = $restaurant;
            if ($restaurant && $d["id_restaurant_raw"] == $id) :
                array_push($data, $d);
            endif;
        }
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/favorites/[:id]/delete', function($mobile, $id) {
    header('Content-Type: application/json');
//    var_dump(urlencode($mobile));
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $data = getTable2("favorites", ["AND" => ["id_restaurant_raw" => $id, "id_user_raw" => $user["id_raw_user"]]]);
        deleteTable2("favorites", $data["id"]);
        if ($data) {
            echo json_encode(["response" => [
                    "status" => "200",
                    "message" => "Favorite removed successfully",
                    "data" => [$data]
            ]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Restaurant not yet in favorites"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

