<?php

$router->map('GET', '/otp/[:mobile]/dispatch', function($mobile) {
    $mobile = urlencode($mobile);
    header('Content-Type: application/json');
    $data = [
        "recipient" => $mobile,
        "expiryDate" => null,
        "code" => null
    ];
    $data = json_encode($data);
    $response = curlyPut(getInit("api_url") . "tabledb-web/otp/dispatch", $data);
    echo($response ? $response : json_encode(["response" => ["status" => "404"]]));
});

$router->map('GET', '/otp/[:mobile]/confirm/[:code]', function($mobile, $code) {
    header('Content-Type: application/json');
    $user = getTable("users", ["mobile" => urlencode($mobile)]);
    if ($user != null) {
        $mobile = urlencode($mobile);
        $code = urlencode($code);
        $response = curlyPut(getInit("api_url") . "tabledb-web/otp/confirm/{$mobile}/{$code}");
        echo($response ? $response : json_encode(["response" => ["status" => "404"]]));
    } else {
        echo(json_encode(["response" => ["status" => "404"]]));
    }
});

$router->map('GET', '/otp', function() {
    header('Content-Type: application/json');
    $code = $_GET["code"];
    $mobile_no = $_GET["mobile"];
    $source = "SEATS";
    $account = "Dev8";
    $accountkey = "S7YLE2";
    $sms_message = 'Your One-Time Password(OTP) from SEATS is '.$code.'. Thank you!';
    $api = "http://162.209.21.247/broadcaster/?account={$account}&accountkey={$accountkey}&msisdn={$mobile_no}&message={$sms_message}&source={$source}&rcvd_transid=seats941802911106";
    echo file_get_contents($api);
    die();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api);
    curl_setopt($ch, CURLOPT_HEADER, false);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $api = curl_exec($ch);
    header('Content-Type: application/json');

    echo json_encode(strpos($api, "Successful"));
    curl_close($ch);
});
