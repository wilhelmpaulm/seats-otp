<?php

//FORUMS

$router->map('GET', '/ratings', function() {
    header('Content-Type: application/json');
    $users = selectTable2("users");
    $data = selectTable2("ratings");
    $data_old = $data;
    $data = [];
    foreach ($data_old as $d) {
        $user = [];
        foreach ($users as $u) {
            if ($d["id_raw_user"] == $u["id_raw_user"]) {
                $user = $u;
                break;
            }
        }
        $d["user"] = $user;
        array_push($data, $d);
    }
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No exisitng yet"]]);
    }
});

$router->map('GET', '/ratings/[i:id]', function($id) {
    header('Content-Type: application/json');
    $users = selectTable2("users");
    $data = selectTable2("ratings", ["id" => $id]);
    $data_old = $data;
    $data = [];

    foreach ($data_old as $d) {
        $user = [];
        foreach ($users as $u) {
            if ($d["id_raw_user"] == $u["id_raw_user"]) {
                $user = $u;
                break;
            }
        }
        $d["user"] = $user;
        $d["restaurant"] = getRestaurantsNew($d["id_raw_restaurant"])[0];
        array_push($data, $d);
    }
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No exisitng yet"]]);
    }
});

$router->map('GET', '/ratings/[i:id]/[:status]', function($id, $status) {
    header('Content-Type: application/json');
    if (in_array($status, ["approved", "pending", "rejected"])) {
        $data = selectTable("ratings", ["id" => $id]);
        updateTable("ratings", ["status" => $status], $id);
        if ($data) {
            echo json_encode(["response" => [
                    "status" => "200",
                    "message" => "Update successful"
            ]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "No exisitng ratings yet"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Invalid stauts"]]);
    }
});
