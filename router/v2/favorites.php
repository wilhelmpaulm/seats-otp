<?php

//FORUMS

$router->map('GET', '/favorites', function() {
    header('Content-Type: application/json');
    $data = selectTable("favorites");
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No exisitng favorites"]]);
    }
});