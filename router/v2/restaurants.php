<?php

$router->map('GET', '/restaurants', function() {
    header('Content-Type: application/json');
    $restaurants_sum = getRestaurantsNew();
    if ($restaurants_sum) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $restaurants_sum
        ]]);
        die();
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
    echo($restaurants_sum ? json_encode($restaurants_sum) : json_encode(["response" => ["status" => "404"]]));
});

$router->map('GET', '/restaurants/[:id]', function($id) {
    header('Content-Type: application/json');
    $restaurants_sum = getRestaurantsNew($id);
    if ($restaurants_sum) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $restaurants_sum
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No restaurant with the ID was found"]]);
    }
});

$router->map('GET', '/restaurants/[:id]/ratings', function($id) {
    global $database2;
    header('Content-Type: application/json');
    $users = getUsers();
    $data = getRatings(["id_restaurant_raw" => $id]);
    $data_old = $data;
    $data = [];
    foreach ($data_old as $d) {
        $user = [];
        foreach ($users as $u) {
            if ($d["id_raw_user"] == $u["id_raw_user"]) {
                $user = $u;
                break;
            }
        }
        $d["user"] = $user;
        array_push($data, $d);
    }
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Restaurant has no ratings yet"]]);
    }
});
//
//$router->map('GET', '/restaurants/[:id]/ratings/sum', function($id) {
//    header('Content-Type: application/json');
//    $ratings = selectTable("ratings", ["AND" => ["id_raw_restaurant" => $id, "status" => "approved"]]);
//    if ($ratings) {
//        $data = [
//            "id_raw_restaurant" => $id,
//            "taste" => 0,
//            "ambience" => 0,
//            "value" => 0,
//            "cleanliness" => 0,
//            "service" => 0,
//            "general" => 0,
//        ];
//        foreach ($ratings as $rating) {
//            $data["ambience"] += $rating["ambience"];
//            $data["taste"] += $rating["taste"];
//            $data["value"] += $rating["taste"];
//            $data["cleanliness"] += $rating["cleanliness"];
//            $data["service"] += $rating["service"];
//            $data["general"] += $rating["general"];
//        }
//        $ratings_size = count($ratings);
//        $data["ambience"] = ($data["ambience"] / $ratings_size);
//        $data["taste"] = ($data["taste"] / $ratings_size);
//        $data["value"] = ($data["value"] / $ratings_size);
//        $data["cleanliness"] = ($data["cleanliness"] / $ratings_size);
//        $data["service"] = ($data["service"] / $ratings_size);
//        $data["general"] = ($data["general"] / $ratings_size);
//
//        echo json_encode(["response" => [
//                "status" => "200",
//                "data" => [$data]
//        ]]);
//    } else {
//        echo json_encode(["response" => ["status" => "404", "message" => "Restaurant has no ratings yet"]]);
//    }
//});

$router->map('GET', '/restaurants/[:id]/availability', function($id) {
    header('Content-Type: application/json');
    $date = substr(currentdatetime(), 0, 10);
    $response = getAvailabilities($id);
    $data = $response;
    usort($data, function($a, $b) {
        return $a["timeSlotId"] > $b["timeSlotId"];
    });
    if ($data) {
        if (count($data) > 0) {
            echo json_encode(["response" => ["status" => "200", "data" => $data]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "No available slots for the date of {$date}"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});

$router->map('GET', '/restaurants/[:id]/availability/[:date]', function($id, $date = null) {
    header('Content-Type: application/json');
    $date = $date ? $date : substr(currentdatetime(), 0, 10);
    $response = getAvailabilities($id, $date);
    $data = $response;
    usort($data, function($a, $b) {
        return $a["timeSlotId"] > $b["timeSlotId"];
    });
    if ($data) {
        if (count($data) > 0) {
            echo json_encode(["response" => ["status" => "200", "data" => $data]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "No available slots for the date of {$date}"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});
