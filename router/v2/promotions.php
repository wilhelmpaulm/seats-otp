<?php

//FORUMS


$router->map('GET', '/promotions/old', function() {
    header('Content-Type: application/json');
    $data = selectTable("promotions");
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "query failed"]]);
    }
});

$router->map('GET', '/restaurants/[:id]/promotions/old', function($id) {
    header('Content-Type: application/json');
    $ratings = selectTable("ratings", ["id_raw_restaurant" => $id]);
    $data = $ratings;
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "no ratings"]]);
    }
});
//
//$router->map('GET', '/promotions', function() {
//    header('Content-Type: application/json');
//    $restaurants = getRestaurantsNew();
//    $response = curlyGet(getInit("api_url") . "/tabledb-web/promotions/future");
//    var_dump($response);
//    die;
//    $data = json_decode($response)->response->data;
//    $data_old = $data;
//    $data = [];
//    foreach ($data_old as $d) {
//        $images = [];
//        $exists = false;
//        $d->restaurant = [];
//        foreach ($restaurants as $r) {
//            if ($r->id == $d->restaurantId) {
//                $exists = true;
//                $d->available = 100;
//                $d->images = $r->images;
//                $d->restaurant = $r;
//                array_push($data, $d);
//                break;
//            }
//        }
//    }
//    if ($data) {
//        echo json_encode(["response" => [
//                "status" => "200",
//                "data" => $data
//        ]]);
//    } else {
//        echo json_encode(["response" => ["status" => "404", "message" => "No existing promotions"]]);
//    }
//});

$router->map('GET', '/promotions', function() {
    header('Content-Type: application/json');
    $restaurants = getRestaurantsNew();
    $data = getPromotions();

    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No existing promotions"]]);
    }
});

$router->map('GET', '/promotions/[:promo]', function($promo) {
    header('Content-Type: application/json');
    $data = getPromotions($promo);
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No existing promotions"]]);
    }
});

$router->map('GET', '/restaurants/{id}/promotions/[:promo]', function($id, $promo) {
    header('Content-Type: application/json');
    $restaurants = getRestaurants();
    $promotions = getPromotions();
    $data = [];

    foreach ($promotions as $key => $val) :
        if ($val["restaurantId"] == $id && $promo == $val["ids"]):
            array_push($data, $val);
        endif;
    endforeach;
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});

$router->map('GET', '/promotions/[:promo]/delete', function($promo) {
    header('Content-Type: application/json');
    $data = getTable("promotions", ["id_raw_promotion" => $promo]);
    if ($data) {
        deleteTable("promotions", ["id_raw_promotion" => $promo]);
        echo json_encode(["response" => [
                "status" => "200",
                "message" => "Promotion was successfully deleted",
                "data" => [$data]
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});



$router->map('GET', '/restaurants/[:id]/promotions/[:promo]/delete', function($id, $promo) {
    header('Content-Type: application/json');
    $data = getTable("promotions", ["id_raw_promotion" => $promo]);
    if ($data) {
        deleteTable("promotions", ["id_raw_promotion" => $promo]);
        echo json_encode(["response" => [
                "status" => "200",
                "message" => "promotion deleted",
                "data" => [$data]
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});

$router->map('GET', '/restaurants/[:id]/promotions', function($id) {
    header('Content-Type: application/json');
    $restaurants = getRestaurants();
    $promotions = getPromotions();
    $data = [];

    foreach ($promotions as $key => $val) :
        if ($val["restaurantId"] == $id):
            array_push($data, $val);
        endif;
    endforeach;
    if ($data) {
        echo json_encode(["response" => [
                "status" => "200",
                "data" => $data
        ]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
    }
});

$router->map('POST', '/restaurants/[:id]/promotions/add', function($id) {
    header('Content-Type: application/json');
    $restaurant = curlyGet(getInit("api_url") . "tabledb-web/restaurant/{$id}/" . getInit("partner_code") . "/" . getInit("partner_auth_code"));

    if ($restaurant) {
        if (getPost("title") != null && getPost("description") != null) {
            $data = [
                "id_raw_restaurant" => $id,
                "id_raw_promotion" => newMongoId(),
                "title" => getPost("title") ? getPost("title") : null,
                "description" => getPost("description") ? getPost("description") : null,
                "duration" => getPost("duration") ? getPost("duration") : 30,
                "start_date" => getPost("start_date") ? getPost("start_date") : substr(currentdatetime(), 0, 10),
                "end_date" => getPost("end_date") ? getPost("end_date") : substr(currentdatetime("+1 day"), 0, 10),
                "start_time" => getPost("start_time") ? getPost("start_time") : "0000",
                "end_time" => getPost("end_time") ? getPost("end_time") : "2400",
                "party_size_max" => getPost("party_size_max") ? getPost("party_size_max") : 50,
                "party_size_min" => getPost("party_size_min") ? getPost("party_size_min") : 2,
                "cover_max" => getPost("cover_max") ? getPost("cover_max") : 50,
                "days_of_week" => getPost("days_of_week") ? getPost("days_of_week") : "1,2,3,4,5,6,7",
                "deal_availability" => getPost("deal_availability") ? getPost("deal_availability") : "all",
                "image" => getPost("image") ? getPost("image") : null,
            ];
            $promotion = insertTable("promotions", $data);
            $promotion = getTable("promotions", ["id" => $promotion]);
            if ($promotion) {
                echo json_encode(["response" => [
                        "status" => "200",
                        "data" => [$promotion]
                ]]);
            } else {
                echo json_encode(["response" => ["status" => "404", "message" => "Failed to add promotion"]]);
            }
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Please include title and description"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Restaurant does not exist"]]);
    }
});

$router->map('GET', '/restaurants/[:id]/promotions/add', function($id) {
    header('Content-Type: application/json');
    $restaurant = curlyGet(getInit("api_url") . "tabledb-web/restaurant/{$id}/" . getInit("partner_code") . "/" . getInit("partner_auth_code"));

    if ($restaurant) {
        if (getGet("title") != null && getGet("description") != null) {
            $data = [
                "id_raw_restaurant" => $id,
                "id_raw_promotion" => newMongoId(),
                "title" => getGet("title") ? getGet("title") : null,
                "description" => getGet("description") ? getGet("description") : null,
                "duration" => getGet("duration") ? getGet("duration") : 30,
                "start_date" => getGet("start_date") ? getGet("start_date") : substr(currentdatetime(), 0, 10),
                "end_date" => getGet("end_date") ? getGet("end_date") : substr(currentdatetime("+1 day"), 0, 10),
                "start_time" => getGet("start_time") ? getGet("start_time") : "0000",
                "end_time" => getGet("end_time") ? getGet("end_time") : "2400",
                "party_size_max" => getGet("party_size_max") ? getGet("party_size_max") : 50,
                "party_size_min" => getGet("party_size_min") ? getGet("party_size_min") : 2,
                "cover_max" => getGet("cover_max") ? getGet("cover_max") : 50,
                "days_of_week" => getGet("days_of_week") ? getGet("days_of_week") : "1,2,3,4,5,6,7",
                "deal_availability" => getGet("deal_availability") ? getGet("deal_availability") : "all",
                "image" => getGet("image") ? getGet("image") : null,
            ];
            $promotion = insertTable("promotions", $data);
            $promotion = getTable("promotions", ["id" => $promotion]);
            if ($promotion) {
                echo json_encode(["response" => [
                        "status" => "200",
                        "data" => [$promotion]
                ]]);
            } else {
                echo json_encode(["response" => ["status" => "404", "message" => "Failed to add promotion"]]);
            }
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Please include title and description"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Restaurant does not exist"]]);
    }
});


$router->map('POST', '/restaurants/[:id]/promotions/edit/[:promo]', function($id, $promo) {
    header('Content-Type: application/json');
    $promotion = getTable("promotions", ["id_raw_promotion" => $promo]);
    if ($promotion) {
        $data = [
            "title" => getPost("title") ? getPost("title") : $promotion["title"],
            "description" => getPost("description") ? getPost("description") : $promotion["description"],
            "duration" => getPost("duration") ? getPost("duration") : $promotion["duration"],
            "start_date" => getPost("start_date") ? getPost("start_date") : $promotion["start_date"],
            "end_date" => getPost("end_date") ? getPost("end_date") : $promotion["end_date"],
            "start_time" => getPost("start_time") ? getPost("start_time") : $promotion["start_time"],
            "end_time" => getPost("end_time") ? getPost("end_time") : $promotion["end_time"],
            "party_size_max" => getPost("party_size_max") ? getPost("party_size_max") : $promotion["party_size_max"],
            "party_size_min" => getPost("party_size_min") ? getPost("party_size_min") : $promotion["party_size_min"],
            "cover_max" => getPost("cover_max") ? getPost("cover_max") : $promotion["cover_max"],
            "days_of_week" => getPost("days_of_week") ? getPost("days_of_week") : $promotion["days_of_week"],
            "deal_availability" => getPost("deal_availability") ? getPost("deal_availability") : $promotion["deal_availability"],
            "image" => getPost("image") ? getPost("image") : $promotion["image"],
        ];
        $promotion = updateTable("promotions", $data, ["id_raw_promotion" => $promo]);
        if ($promotion) {
            echo json_encode(["response" => [
                    "status" => "200",
                    "data" => [$promotion]
            ]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Failed to edit promotion"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Promotion does not exist"]]);
    }
});
