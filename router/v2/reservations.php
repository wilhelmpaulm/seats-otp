<?php

$router->map('GET', '/users/[:mobile]/reservations/add', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    $restaurants_raw = selectTable2("restaurants");
    $users_raw = selectTable2("users");
    $availabilities = selectTable2("restaurant_availabilities");
    $promotions = selectTable2("restaurant_promotions");


    $restaurant = (array) getRestaurantsNew(getGet("restaurant_id"))[0];
    $rand = getRand();
    if (!$user) :
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
        die();
    endif;
    if (!$restaurant || !getGet("restaurant_id")) :
        echo json_encode(["response" => ["status" => "404", "message" => "Restaurant does not exist"]]);
        die();
    endif;

    $id_restaurant = "";
    $id_user = "";
    $id_availability = "";

    foreach ($restaurants_raw as $key => $val):
        if ($restaurant["id"] == $val["id_raw"]):
            $id_restaurant = $val["id"];
            break;
        endif;
    endforeach;

    foreach ($users_raw as $key2 => $val2):
        if ($user["id_raw_user"] == $val2["id_raw"]):
            $id_user = $val2["id"];
            break;
        endif;
    endforeach;

    foreach ($availabilities as $key2 => $val2):
        $min = date("Hi", strtotime($val2["time_slot_start"]));
        $max = date("Hi", strtotime($val2["time_slot_end"]));
        if ($val2["id_restaurant_raw"] == $restaurant["id"] && $min <= getGet("timeslot_id") && $max >= getGet("timeslot_id") && $val2["day"] == strtolower(date("l", strtotime(getGet("date") ? getGet("date") : (substr(currentdatetime(), 0, 10)))))):
            $id_availability = $val2["id"];
            break;
        endif;
    endforeach;

    $reservation = [
        "id_availability" => $id_availability,
        "id_restaurant" => $id_restaurant,
        "id_restaurant_raw" => $restaurant["id"],
        "id_user" => $id_user,
        "id_user_raw" => $user["id_raw_user"],
        "status" => "active",
        "reservation_date" => getGet("date") ? getGet("date") : (substr(currentdatetime(), 0, 10)),
        "date" => getGet("date") ? getGet("date") : (substr(currentdatetime(), 0, 10)),
        "reservation_time" => date("h:i A", strtotime(intval(getGet("timeslot_id")) . "")),
        "time_slot" => intval(getGet("timeslot_id")) . "",
        "adult_seats" => (getGet("adult_seats") ? intval(getGet("adult_seats")) : 2),
        "child_seats" => (getGet("child_seats") ? intval(getGet("child_seats")) : 0),
        "has_rating" => 0,
        "remarks" => getGet("notes") ? getGet("notes") : "",
        "id_raw" => $rand
    ];
    if (getGet("promotion_id") != null && getGet("promotion_id") != "" && getGet("promotion_id") != "null") {
        foreach ($promotions as $keyp => $valp):
            if (getGet("promotion_id") == $valp["id_raw"]):
                $reservation["id_promotion"] = $valp["id"];
                break;
            endif;
        endforeach;
        $reservation["id_promotion_raw"] = getGet("promotion_id") ? getGet("promotion_id") : "";
    }
    $id_reservation = insertTable2("reservations", $reservation);
    $admin_mobiles = selectTable2("merchant_notification_details", ["restaurant_id" => $id_restaurant]);
    if ($id_reservation) {
        $message = "Your reservation for " . ($reservation["adult_seats"] + $reservation["child_seats"]) . " at " . $restaurant["name"] . " on " . $reservation["date"] . " at " . date("h:i A", strtotime($reservation["time_slot"])) . " is CONFIRMED. Thank you for booking through SEATS! ";
        $id_reservation_raw = getTable2("reservations", $id_reservation)["id_raw"];
        $reservation2 = getReservations($id_reservation_raw)[0];

        smsSuccess($user["mobile"] . "", $message);
        sendMailSuccessUser($reservation2, $user["email"]);
        if ($admin_mobiles):
            foreach ($admin_mobiles as $key5 => $val5):
                if ($val5["type"] == "sms"):
                    $message = "Reservation - " . $restaurant["name"] . ",%0a";
                    $message .= $user["first_name"] . " " . $user["last_name"] . ",%0a";
                    $message .= $user["mobile"] ? "%2B" . $user["mobile"] . ",%0a" : "";
                    $message .= $reservation["adult_seats"] . "%2B" . $reservation["child_seats"] . " Pax,%0a";
                    $message .= date("d-m-Y", strtotime($reservation["date"])) . ",%0a" . date("h:i A", strtotime($reservation["time_slot"]));
                    $message .= $reservation["remarks"] ? ",%0a" . $reservation["remarks"] : "";
                    smsSuccessAdmin($val5["address"] . "", $message);
                endif;
                if ($val5["type"] == "email"):
                    sendMailSuccessMerchant($reservation2, $val5["address"]);
                endif;
            endforeach;
        endif;

        $data = getReservations($rand);
        echo json_encode(["response" => ["status" => "200", "data" => $data]]);
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "Reservation failed"]]);
    }
});

$router->map('GET', '/users/[:mobile]/reservations', function($mobile) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $ratings = selectTable2("ratings", ["id_user_raw" => $user["id_raw_user"]]);
        $reservations = getReservations();
        $return = [];
        if ($reservations) {
            foreach ($reservations as $key => $val):
                $val["hasRating"] = false;
                if (("+63" . $val["diner"]["phones"][0]["value"] == $user["mobile"]) || ($val["dinerId"] == $user["id_raw_user"] && $val["dinerId"] != null)):
                    foreach ($ratings as $key2 => $val2):
                        if ($val2["id_reservation_raw"] == $val["id"]):
                            $val["hasRating"] = true;
                            break;
                        endif;
                    endforeach;
                    array_push($return, $val);
                endif;
            endforeach;
            echo json_encode(["response" => ["status" => "200", "data" => $return]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/reservations', function() {
    header('Content-Type: application/json');
    $users = getUsers();
    $reservations = getReservations();
    $ratings = selectTable2("ratings");
    if ($users) {
        $return = [];
        if ($reservations) {
            foreach ($reservations as $key => $val):
                $user = null;
                $val["hasRating"] = false;
                foreach ($users as $key2 => $val2):
                    if ($val["dinerId"] == $val2["id_raw_user"]):
                        $user = $val2;
                        break;
                    endif;
                endforeach;

                foreach ($ratings as $key3 => $val3):
                    if ($val["dinerId"] == $user["id_raw_user"] && $val3["id_reservation_raw"] == $val["id"]):
                        $val["hasRating"] = true;
                        break;
                    endif;
                endforeach;

                if ($user):
                    array_push($return, $val);
                endif;
            endforeach;
            echo json_encode(["response" => ["status" => "200", "data" => $return]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/reservations/[:type]/[:date_start]/[:date_end]', function($type, $date_start, $date_end) {
    header('Content-Type: application/json');
    $users = getUsers();
    $reservations = getReservations();
    $ratings = selectTable2("ratings");
    if ($users) {
        $return = [];
        if ($reservations) {
            foreach ($reservations as $key => $val):
                if ((($type == "reservation_date" && $date_start <= date("Y-m-d", $val["date"] / 1000 + 28800) && $date_end >= date("Y-m-d", $val["date"] / 1000 + 28800))) || (($type == "date_created" && $date_start <= date("Y-m-d", $val["createdDate"] / 1000 + 28800) && $date_end >= date("Y-m-d", $val["createdDate"] / 1000 + 28800)))):
                    $user = null;
                    $val["hasRating"] = false;
                    foreach ($users as $key2 => $val2):
                        if ($val["dinerId"] == $val2["id_raw_user"]):
                            $user = $val2;
                            break;
                        endif;
                    endforeach;

                    foreach ($ratings as $key3 => $val3):
                        if ($val["dinerId"] == $user["id_raw_user"] && $val3["id_reservation_raw"] == $val["id"]):
                            $val["hasRating"] = true;
                            break;
                        endif;
                    endforeach;

                    if ($user):
                        array_push($return, $val);
                    endif;
                endif;
            endforeach;
            echo json_encode(["response" => ["status" => "200", "data" => $return]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/reservations/[:reservation_id]', function($mobile, $reservation_id) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $ratings = selectTable2("ratings", ["id_user_raw" => $user["id_raw_user"]]);
        $reservations = getReservations($reservation_id);
        $return = [];
        if ($reservations) {
            foreach ($reservations as $key => $val):
                $val["hasRating"] = false;
                if ($val["dinerId"] == $user["id_raw_user"]):
                    foreach ($ratings as $key2 => $val2):
                        if ($val2["id_reservation_raw"] == $val["id"]):
                            $val["hasRating"] = true;
                            break;
                        endif;
                    endforeach;
                endif;
                array_push($return, $val);
            endforeach;
            echo json_encode(["response" => ["status" => "200", "data" => $return]]);
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});

$router->map('GET', '/users/[:mobile]/reservations/[:reservation_id]/cancel', function($mobile, $reservation_id) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $reservations = selectTable2("reservations", ["id_raw" => $reservation_id]);
        if ($reservations) {
            $reservation = [];
            $reservation = $reservations[0];
            if ($reservation != null) {
                updateTable2("reservations", ["status" => "cancelled"], $reservation["id"]);
                $reservation = getTable2("reservations", $reservation["id"]);
                if ($reservation["status"] == "cancelled") {
                    $reservation = getReservations($reservation["id_raw"])[0];
                    smsSuccess($user["mobile"], urlencode("Your reservation at " . $reservation["restaurant"]["name"] . " has been cancelled."));
                    sendMailCanceledUser($reservation, $user["email"]);
                    $id_restaurant = getTable("restaurants", ["id_raw" => $reservation["restaurantId"]])["id"];
                    $admin_mobiles = selectTable2("merchant_notification_details", ["restaurant_id" => $id_restaurant]);
                    if ($admin_mobiles):
                        foreach ($admin_mobiles as $key5 => $val5):
                            if ($val5["type"] == "sms"):
                                $message = "Cancellation - " . $reservation["restaurant"]["name"] . ",%0a";
                                $message .= $user["first_name"] . " " . $user["last_name"] . ",%0a";
                                $message .= $user["mobile"] ? "%2B" . $user["mobile"] . ",%0a" : "";
                                $message .= ($reservation["adultSeats"]) . "%2B0 Pax,%0a";
                                $message .= (date("d-m-Y", $reservation["date"] / 1000 + (8 * 60 * 60))) . ",%0a" . date("h:i A", strtotime($reservation["timeSlotId"]));
                                smsSuccessAdmin($val5["address"] . "", $message);
                            endif;
                            if ($val5["type"] == "email"):
                                sendMailCanceledMerchant($reservation, $val5["address"]);
                            endif;
                        endforeach;
                    endif;
                    echo json_encode(["response" => ["status" => "200", "message" => "Reservation was successfully canceled"]]);
                } else {
                    echo json_encode(["response" => ["status" => "404", "message" => "Reservation cancelation failed"]]);
                }
            } else {
                echo json_encode(["response" => ["status" => "404", "message" => "Reservation does not exist"]]);
            }
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "No reservations found"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile bumber was found"]]);
    }
});

//
//$router->map('GET', '/users/[:mobile]/reservations/[:reservation_id]/cancel', function($mobile, $reservation_id) {
//    header('Content-Type: application/json');
//    $user = getUsers(urlencode($mobile));
//    if ($user) {
//        $reservations = selectTable2("reservations", ["id_user_raw" => $user["id_raw_user"]]);
//        if ($reservations) {
//            $reservation = [];
//            foreach ($reservations as $key => $val):
//                if ($val["id_raw"] == $reservation_id):
//                    $reservation = $val;
//                    break;
//                endif;
//            endforeach;
//            if ($reservation != null) {
//                $id_reservation = updateTable2("reservations", ["status" => "cancelled"], $reservation["id"]);
//                smsCancel($user["mobile"]);
//                $reservation = getTable2("reservations", $reservation["id"]);
//                if ($reservation["status"] == "cancelled") {
//                    echo json_encode(["response" => ["status" => "200", "message" => "Reservation was successfully canceled"]]);
//                } else {
//                    echo json_encode(["response" => ["status" => "404", "message" => "Reservation cancelation failed"]]);
//                }
//            } else {
//                echo json_encode(["response" => ["status" => "404", "message" => "Reservation does not exist"]]);
//            }
//        } else {
//            echo json_encode(["response" => ["status" => "404", "message" => "No reservations found"]]);
//        }
//    } else {
//        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile bumber was found"]]);
//    }
//});




$router->map('GET', '/users/[:mobile]/reservations/[:reservation_id]/edit', function($mobile, $reservation_id) {
    header('Content-Type: application/json');
    $user = getUsers(urlencode($mobile));
    if ($user) {
        $response = getReservations($reservation_id);
        if ($response) {
            $reservation = $response[0];
            if ($reservation != null) {
                $data = [];
                if (getGet("date")):
                    $data["date"] = getGet("date");
                endif;

                if (getGet("timeslot_id")):
                    $data["time_slot"] = getGet("timeslot_id") . "";
                endif;


                if (getGet("promotion_id") != null && getGet("promotion_id") != "" && getGet("promotion_id") != "null") {
                    $promotions = selectTable2("restaurant_promotions");
                    foreach ($promotions as $keyp => $valp):
                        if (getGet("promotion_id") == $valp["id_raw"]):
                            $data["id_promotion"] = $valp["id"];
                            break;
                        endif;
                    endforeach;
                    $data["id_promotion_raw"] = getGet("promotion_id") ? getGet("promotion_id") : "";
                }

                if (getGet("promotion_id") == "null") {
                    $data["id_promotion_raw"] = null;
                    $data["id_promotion"] = null;
                }
//                if (getGet("promotion_id")):
//                    $data["id_promotion"] = getGet("promotion_id");
//                endif;

                if (getGet("adult_seats")):
                    $data["adult_seats"] = getGet("adult_seats");
                endif;

                if (getGet("child_seats")):
                    $data["child_seats"] = getGet("child_seats");
                endif;
                if ($data):
                    $success = updateTable2("reservations", $data, ["id_raw" => $reservation_id]);
                else:
                    $success = false;
                endif;
                $reservation = getReservations($reservation_id)[0];
                $id_restaurant = getTable("restaurants", ["id_raw" => $reservation["restaurant"]["id"]])["id"];
                if ($success) {
                    $admin_mobiles = selectTable2("merchant_notification_details", ["restaurant_id" => $id_restaurant]);
                    $message = "You have successfully updated the details of your reservation at " . $reservation["restaurant"]["name"] . ". Thank you for using SEATS!";
                    smsSuccess($user["mobile"] . "", $message);
                    sendMailModifiedUser($reservation, $user["email"]);
                    if ($admin_mobiles):
                        foreach ($admin_mobiles as $key5 => $val5):
                            if ($val5["type"] == "sms"):
                                $message = "Edited Reservation - " . $reservation["restaurant"]["name"] . ",%0a";
                                $message .= $user["first_name"] . " " . $user["last_name"] . ",%0a";
                                $message .= $user["mobile"] ? "%2B" . $user["mobile"] . ",%0a" : "";
                                $message .= ($reservation["adultSeats"]) . "%2B0 Pax,%0a";
                                $message .= (date("d-m-Y", $reservation["date"] / 1000 + (8 * 60 * 60))) . ",%0a" . date("h:i A", strtotime($reservation["timeSlotId"]));
                                $message .= $reservation["note"] ? ",%0a" . $reservation["note"] : "";
                                smsSuccessAdmin($val5["address"] . "", $message);
                            endif;
                            if ($val5["type"] == "email"):
                                sendMailModifiedMerchant($reservation, $val5["address"]);
                            endif;
                        endforeach;
                    endif;

                    echo json_encode(["response" => ["status" => "200", "message" => "Reservation was successfully edited", "data" => [$reservation]]]);
                } else {
                    echo json_encode(["response" => ["status" => "404", "message" => "Reservation edit failed"]]);
                }
            } else {
                echo json_encode(["response" => ["status" => "404", "message" => "Reservation does not exist"]]);
            }
        } else {
            echo json_encode(["response" => ["status" => "404", "message" => "Query failed"]]);
        }
    } else {
        echo json_encode(["response" => ["status" => "404", "message" => "No user with that mobile number was found"]]);
    }
});
