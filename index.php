<?php

########################################################
#place all the libraries here on the top
ini_set('max_execution_time', 600);
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Manila');
include './vendor/autoload.php';
include './lib/config.php';
include './lib/db.php';
include './lib/helper.php';
include './lib/curly.php';
include './lib/seats.php';
include './lib/seats2.php';
include './lib/gcm.php';
//include './lib/social.php';
#end of libraries
########################################################
#place all the models here
//include './model/user.php';
#end of models
########################################################
#place all the classes you're going to use here
//include './controller/districts.php';
#end of classes
########################################################
#you should never touch this 
include './router/template/router_header.php';
include './router/_base.php';
include './router/template/router_footer.php';
#ever
########################################################
