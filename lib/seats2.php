<?php

function getDetails() {
    global $database2;
    $details = $database2->select("details", "*");
    $array = [];
    foreach ($details as $key => $val):
        if (!key_exists($val["type"], $array)):
            $array[$val["type"]] = [];
        endif;
        array_push($array[$val["type"]], $val);
    endforeach;
    return $array;
}

function filterLists($array, $detail) {
    $return = [];
    foreach ($array as $key => $val):
        $in = null;
        if ($val["type"] == $detail && $val["status"] == "active"):
            $in = $val["name"];
        endif;
        $in ? array_push($return, $in) : null;
    endforeach;
    return $return;
}

function filterDetails($array, $id_restaurant, $detail) {
    $return = [];
    foreach ($array as $key => $val):
        $in = null;
        if (($val["id"] == $id_restaurant || $val["id_restaurant_raw"] == $id_restaurant) && ($detail == "banner" ? "image" : $detail ) == $val["type"] && $val["status"] == "active"):
            switch ($detail):
                case ("banner"):
                    if ($detail == "banner" && $val["option"] == "banner"):
                        $in = $val["thumbnail"];
                    endif;
                    break;
                case ("image"):
                    if ($val["option"] != "banner"):
                        $in = $val["thumbnail"];
                    endif;
                    break;
                case ("payment_option" || "additional_detail" || "cuisine" || "tag" || "occasion"):
                    $in = trim($val["name"]);
                    break;
                default:
                    break;
            endswitch;
            $in ? array_push($return, $in) : null;
        endif;
    endforeach;
    return $return;
}

function samplers() {

    $user = [
        "id" => "109",
        "created_at" => "2015-11-13 06:39:57",
        "updated_at" => "2015-11-24 07:47:25",
        "status" => "active",
        "type" => "user",
        "id_raw_user" => "564584cde4b07514216ec117",
        "id_raw_user_temp" => "56541745e4b0541e61becaf1",
        "code" => "7526",
        "first_name" => "Wilhelm Paul",
        "middle_name" => null,
        "last_name" => "Martinez",
        "designation" => null,
        "email" => "wilhelmpaulm@gmail.com",
        "mobile" => "+639279655572",
        "birthdate" => null,
        "gender" => null,
        "password" => null,
        "verified" => "false",
        "points" => "0",
        "image" => "https://graph.facebook.com/870234823068154/picture?type=normal",
        "fb_id" => null,
        "fb_link" => null,
        "gcm" => null
    ];

    $restaurant = [
        "id" => "",
        "createdBy" => "",
        "createdDate" => "",
        "lastModifiedBy" => "",
        "lastModifiedDate" => "",
        "version" => "",
        "name" => "",
        "localName" => "",
        "dressCode" => "",
        "diningStyle" => "",
        "cuisines" => "",
        "neighborhood" => "",
        "crossStreet" => "",
        "menu" => "",
        "price" => "",
        "website" => "",
        "hoursOfOperation" => "",
        "paymentOptions" => [],
        "acceptWalkins" => "",
        "additionalInfo" => "",
        "publicTransit" => "",
        "parking" => "",
        "parkingDetails" => "",
        "catering" => "",
        "specialEventAndPromotions" => "",
        "address" => "",
        "localAddress" => "",
        "city" => "",
        "localCity" => "",
        "country" => "",
        "postalCode" => "",
        "specialties" => "",
        "chef" => "",
        "description" => "",
        "phone" => "",
        "email" => "",
        "latitude" => "",
        "longitude" => "",
        "images" => [],
        "publicTransport" => "",
        "corkageDetails" => "",
        "eventFacilities" => "",
        "additionalDetails" => [],
        "fullPhoneNumber" => "",
        "featured" => "",
        "featuredDate" => "",
        "gmtMinutesOffset" => "-480",
        "groupId" => "",
        "planType" => "covers",
        "reservationEmail" => "",
        "facebookImage" => "",
        "tzid" => "",
        "hoiioNumber" => "",
        "ipadAppVersion" => "",
        "logoUrl" => "",
        "chefRestaurant" => "",
        "config" => "",
        "listingUrl" => "",
        "replyTo" => "",
        "checkSize" => "",
        "restaurantSlug" => "",
        "ccCheckStartTimestamp" => "",
        "ccCheckEndTimestamp" => "",
        "ccCheckCovers" => "",
        "ccCheckCoversMessage" => "",
        "premiumbooking" => "",
        "accessPoints" => "",
        "street" => "",
        "thumbnail" => "",
        "enableNewletter" => "",
        "disabledNoPromotion" => "",
        "extRestaurantId" => "",
        "showPromoField" => "",
        "gmtminutesOffset" => "",
        "rating" => "",
        "cuisine" => [],
        "tags" => [],
        "occasions" => [],
        "promotions" => [],
    ];

    $availability = [
        "timeSlotId" => 1130,
        "remainingTimeSlotCapacity" => 50,
        "remainingShiftCapacity" => 50,
        "maxNumberOfSeatsForSingleReservation" => 25,
        "minNumberOfSeatsForSingleReservation" => 2,
        "shiftId" => 3,
        "cutOffTime" => null,
        "minPartySizeBooking" => null,
        "maxPartySizeBooking" => null
    ];

    $reservation = [
        "id" => "56527e1ee4b0541e61becab8",
        "createdBy" => null,
        "createdDate" => 1448246814001,
        "lastModifiedBy" => null,
        "lastModifiedDate" => 1448246814001,
        "version" => 0,
        "dinerId" => "55c46ea3e4b07e4ac935570e",
        "diner" => [
            "id" => "55c46ea3e4b07e4ac935570e",
            "createdBy" => null,
            "createdDate" => 1438936739549,
            "lastModifiedBy" => null,
            "lastModifiedDate" => 1448246813999,
            "version" => 2,
            "firstName" => "Wilhelm Paul",
            "lastName" => "Martinez",
            "companyName" => null,
            "position" => null,
            "restaurantId" => "556bb3b3e4b0032e07899488",
            "webUserId" => "559e3e24e4b04bf2709b49d0",
            "birthday" => null,
            "anniversary" => null,
            "note" => null,
            "phones" => [
                [
                    "label" => "Mobile",
                    "value" => "9279655572",
                    "countryCode" => "+63",
                    "isPrimary" => true
                ]
            ],
            "emails" => [
                [
                    "label" => "Email",
                    "value" => "wilhelmpaulm@gmail.com",
                    "isPrimary" => true
                ]
            ],
            "addresses" => null,
            "tagIds" => null,
            "relationships" => null,
            "allergies" => null,
            "salutation" => "Mr",
            "lastVisitDateTime" => null,
            "primaryMobileNumber" => "+639279655572",
            "groupId" => "556bb3b3e4b0032e07899487",
            "otherRestaurantIds" => [
                "55ded7a8e4b0032e0789b486"
            ],
            "zipCode" => null,
            "status" => "active",
            "walkin" => false,
            "newsletter" => false,
            "unwanted" => false,
            "countReservation" => null
        ],
        "timeSlotId" => 1100,
        "note" => "",
        "tagIds" => null,
        "tables" => null,
        "date" => 1448236800000,
        "mode" => 2,
        "numberOfSeats" => 2,
        "status" => 1,
        "templateSlotId" => null,
        "overrideTemplateSlotId" => "56527e1de4b0541e61becab7",
        "seatedTime" => null,
        "completedTime" => null,
        "waitingCreatedTime" => null,
        "restaurantId" => "55ded7a8e4b0032e0789b486",
        "restaurant" => [
            "id" => "55ded7a8e4b0032e0789b486",
            "createdBy" => null,
            "createdDate" => 1440667560851,
            "lastModifiedBy" => null,
            "lastModifiedDate" => 1446025356157,
            "version" => 25,
            "name" => "SEATS BISTRO",
            "localName" => null,
            "dressCode" => null,
            "diningStyle" => null,
            "cuisines" => null,
            "neighborhood" => "",
            "crossStreet" => null,
            "menu" => null,
            "price" => "600",
            "website" => "",
            "hoursOfOperation" => "Daily 11am to 10pm",
            "paymentOptions" => [],
            "acceptWalkins" => null,
            "additionalInfo" => null,
            "publicTransit" => null,
            "parking" => null,
            "parkingDetails" => "",
            "catering" => null,
            "specialEventAndPromotions" => null,
            "address" => "Burgos Circle, BGC",
            "localAddress" => null,
            "city" => "Taguig City",
            "localCity" => null,
            "country" => "PH",
            "postalCode" => "1634",
            "specialties" => "Freebies & GC's",
            "chef" => "",
            "description" => "* Demo restaurant<br /><br />Get freebies and a chance to win Gift Certificates!<br /><br />Visit our booth to get freebies and a chance to win GCs!<br /><br />Here&rsquo;s how: <br />1. Make a reservation under the Seats Bistro restaurant<br />2. Visit the Seats booth&nbsp;<br />3. Provide the host with your name and claim your goodies<br /><br />* Each reservation from a device entitles user to one freebie and one raffle entry to win Gift Certificates.",
            "phone" => "9175144723",
            "email" => "info@seats.com.ph",
            "latitude" => 14.5523837,
            "longitude" => 121.0442479,
            "images" => [
                "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/seatsbs1_20150901_1600.jpg",
                "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/seatsbs2_20150901_7875.jpg",
                "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/seatsbs3_20150901_8304.jpg",
                "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/11_20151028_8480.png",
                "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/111_20151028_2717.png",
                "https://tabledb-seats.s3.amazonaws.com/seats/rest/55ded7a8e4b0032e0789b486/1-1_20151028_9586.jpg"
            ],
            "publicTransport" => "",
            "corkageDetails" => "",
            "eventFacilities" => "",
            "additionalDetails" => [],
            "fullPhoneNumber" => "+63 9175144723",
            "featured" => true,
            "featuredDate" => null,
            "gmtMinutesOffset" => -480,
            "groupId" => "556bb3b3e4b0032e07899487",
            "planType" => "covers",
            "reservationEmail" => null,
            "facebookImage" => null,
            "tzid" => "Asia/Manila",
            "hoiioNumber" => null,
            "ipadAppVersion" => null,
            "logoUrl" => null,
            "chefRestaurant" => null,
            "config" => null,
            "listingUrl" => null,
            "replyTo" => null,
            "checkSize" => null,
            "restaurantSlug" => "seats-bistro",
            "ccCheckStartTimestamp" => null,
            "ccCheckEndTimestamp" => null,
            "ccCheckCovers" => null,
            "ccCheckCoversMessage" => null,
            "premiumbooking" => null,
            "accessPoints" => null,
            "street" => "",
            "thumbnail" => "https://tabledb-seats.s3.amazonaws.com/seats/rest/thumb_55ded7a8e4b0032e0789b486.jpg",
            "enableNewletter" => false,
            "disabledNoPromotion" => false,
            "extRestaurantId" => null,
            "showPromoField" => null,
            "gmtminutesOffset" => -480
        ],
        "restaurantReservationNote" => null,
        "floorplanId" => null,
        "configId" => "5629e3b8e4b0a31f0086f3b0",
        "shiftId" => 3,
        "quotedTime" => null,
        "campaignId" => "organic",
        "pending" => null,
        "verificationKey" => "13966b4c-d94c-4031-abe7-cd248096b848",
        "widgetId" => "54728d3355bda4ff32479560",
        "widget" => [
            "id" => "54728d3355bda4ff32479560",
            "createdBy" => null,
            "createdDate" => null,
            "lastModifiedBy" => null,
            "lastModifiedDate" => 1447659858909,
            "version" => 6,
            "domainName" => "",
            "restaurantId" => "",
            "vendorType" => "SINGLE",
            "chargeable" => true,
            "description" => "",
            "partnerCode" => ""
        ],
        "resetWaitingCreatedTime" => null,
        "resetCompletedTime" => null,
        "resetSeatedTime" => null,
        "hasSlotChanged" => false,
        "modifyingDiner" => false,
        "statusChanged" => false,
        "spendingAmount" => null,
        "turnoverTime" => null,
        "promotion" => null,
        "promotionId" => null,
        "adultSeats" => 2,
        "childSeats" => 0,
        "flexDayConfigId" => null,
        "flexTableIds" => null,
        "flexIsJoinedTable" => null,
        "messageSent" => null,
        "emailSent" => null,
        "paymentTransactionId" => null,
        "staffName" => null,
        "staffId" => null,
        "originalBookedDate" => null,
        "originalTimeslot" => null,
        "historyTimeNotes" => null,
        "extBookingId" => null,
        "extCancelUrl" => null,
        "originalAdult" => null,
        "originalChild" => null,
        "promocodeId" => null,
        "waitingList" => false,
        "hasRating" => false
    ];
}

function getPromotions($id = null) {
    $reservations = selectTable2("reservations");
    if ($id):
        $promotions = selectTable2("restaurant_promotions", ["id_raw" => $id]);
    else:
        $promotions = selectTable2("restaurant_promotions");
    endif;
    $restaurants = getRestaurantsNewClean();
    $return = [];
    foreach ($promotions as $key => $val):
        if ($val["status"] == "active" || $val["status"] == "live" && ( $val["date_end"] . "" >= date("Y-m-d") )):
            $available = $val["max_cover"] ? $val["max_cover"] : 50;
            foreach ($reservations as $key3 => $val3):
                if ($val3["id_promotion"] == $val["id_raw"]):
                    $available--;
                endif;
            endforeach;
            $res = [];
            foreach ($restaurants as $key2 => $val2):
                if ($val["id_restaurant_raw"] == $val2->id):
                    $res = $val2;
                    break;
                endif;
            endforeach;
            if ($res):
                $data = [
                    "id" => $val["id_raw"],
                    "id_raw_promotion" => $val["id_raw"],
                    "createdBy" => null,
                    "createdDate" => strtotime($val["created_at"]) * 1000 + (8 * 60 * 60),
                    "lastModifiedBy" => null,
                    "lastModifiedDate" => strtotime($val["updated_at"]) * 1000 + (8 * 60 * 60),
                    "version" => 1,
                    "promotionType" => null,
                    "firstTimeSlotId" => $val["time_slot_start"],
                    "lastTimeSlotId" => $val["time_slot_end"],
                    "start_time" => $val["time_slot_start"],
                    "end_time" => $val["time_slot_end"],
                    "duration" => 100,
                    "maximumCover" => $val["max_cover"],
                    "cover_max" => intval($val["max_cover"]),
                    "maximumPartySize" => $val["max_party"],
                    "party_size_max" => intval($val["max_party"]),
                    "minimumPartySize" => $val["min_party"],
                    "party_size_min" => intval($val["min_party"]),
                    "title" => $val["title"],
                    "description" => $val["description"],
                    "dinerTypes" => [
                        [
                            "id" => "4fcf10c23467c7de36000001",
                            "createdBy" => null,
                            "createdDate" => null,
                            "lastModifiedBy" => null,
                            "lastModifiedDate" => null,
                            "version" => null,
                            "name" => "All",
                            "description" => "This promotion will be available for booking.",
                            "postURL" => "http =>  //localhost =>  2345",
                            "always" => true,
                            "pushLastMinuteOnly" => false
                        ]
                    ],
                    "dinerTypeIds" => [
                        "4fcf10c23467c7de36000001"
                    ],
                    "restaurantStatus" => null,
                    "endTimestamp" => strtotime($val["date_end"]) * 1000 + (8 * 60 * 60),
                    "startTimestamp" => strtotime($val["date_start"]) * 1000 + (8 * 60 * 60),
                    "start_date" => $val["date_start"],
                    "end_date" => $val["date_end"],
                    "durationDays" => null,
                    "daysOfWeek" => explode(",", $val["days"]),
                    "days_of_week" => explode(",", $val["days"]),
                    "blockedDates" => [],
                    "live" => ($val["status"] == "live" ? true : false),
                    "lastMinute" => false,
                    "shiftIds" => [],
                    "currentCover" => 0,
                    "restaurantId" => $val["id_restaurant_raw"],
                    "id_raw_restaurant" => $val["id_restaurant_raw"],
                    "withdrawn" => false,
                    "filter" => null,
                    "editable" => true,
                    "partnerCode" => null,
                    "price" => null,
                    "specialPrice" => null,
                    "perDayCover" => 0,
                    "dailyCoversLimits" => null,
                    "displayInHGW" => false,
                    "featuredDeal" => 0,
                    "dealTypeIds" => null,
                    "dealTypes" => null,
                    "dealTypeId" => null,
                    "dealType" => null,
                    "promotionImage" => null,
                    "promotionSummary" => null,
                    "promotionTermsAndConditions" => null,
                    "socialSharingTexts" => null,
                    "countryCode" => null,
                    "internalRemarks" => null,
                    "restaurant" => $res,
                    "available" => $available,
                    "images" => $res->images
                ];

                array_push($return, $data);
            endif;
        endif;
    endforeach;
    return $return;
}

function getPromotionsClean($id = null) {
    $reservations = selectTable2("reservations");
    $restaurants = selectTable2("restaurants");
    $restos = getRestaurantsNewClean();
    if ($id):
        $promotions = selectTable2("restaurant_promotions", ["id_raw" => $id]);
    else:
        $promotions = selectTable2("restaurant_promotions");
    endif;
    $return = [];
    foreach ($promotions as $key => $val):
        if ($val["status"] == "active" || $val["status"] == "live" && ( $val["date_end"] . "" >= date("Y-m-d") )):
            $available = $val["max_cover"] ? $val["max_cover"] : 50;
            foreach ($reservations as $key3 => $val3):
                if ($val3["id_promotion"] == $val["id_raw"]):
                    $available--;
                endif;
            endforeach;
            foreach ($restaurants as $key4 => $val4):
                if ($val4["id_raw"] == $val["id_restaurant_raw"] && ($val4["status"] == "live" || $val4["status"] == "active")):
                    $res = null;
                    $images = [];
                    foreach ($restos as $keya => $vala):
                        if ($vala->id == $val["id_restaurant_raw"]):
                            $res = $vala;
                            $images = $vala->images;
                        endif;
                    endforeach;
                    $data = [
                        "id" => $val["id_raw"],
                        "id_raw_promotion" => $val["id_raw"],
                        "createdBy" => null,
                        "createdDate" => strtotime($val["created_at"]) * 1000 + (8 * 60 * 60),
                        "lastModifiedBy" => null,
                        "lastModifiedDate" => strtotime($val["updated_at"]) * 1000 + (8 * 60 * 60),
                        "version" => 1,
                        "promotionType" => null,
                        "firstTimeSlotId" => $val["time_slot_start"],
                        "lastTimeSlotId" => $val["time_slot_end"],
                        "start_time" => $val["time_slot_start"],
                        "end_time" => $val["time_slot_end"],
                        "duration" => 100,
                        "maximumCover" => $val["max_cover"],
                        "cover_max" => intval($val["max_cover"]),
                        "maximumPartySize" => $val["max_party"],
                        "party_size_max" => intval($val["max_party"]),
                        "minimumPartySize" => $val["min_party"],
                        "party_size_min" => intval($val["min_party"]),
                        "title" => $val["title"],
                        "description" => $val["description"],
                        "dinerTypes" => [
                            [
                                "id" => "4fcf10c23467c7de36000001",
                                "createdBy" => null,
                                "createdDate" => null,
                                "lastModifiedBy" => null,
                                "lastModifiedDate" => null,
                                "version" => null,
                                "name" => "All",
                                "description" => "This promotion will be available for booking.",
                                "postURL" => "http =>  //localhost =>  2345",
                                "always" => true,
                                "pushLastMinuteOnly" => false
                            ]
                        ],
                        "dinerTypeIds" => [
                            "4fcf10c23467c7de36000001"
                        ],
                        "restaurantStatus" => null,
                        "endTimestamp" => strtotime($val["date_end"]) * 1000 + (8 * 60 * 60),
                        "startTimestamp" => strtotime($val["date_start"]) * 1000 + (8 * 60 * 60),
                        "start_date" => $val["date_start"],
                        "end_date" => $val["date_end"],
                        "durationDays" => null,
                        "daysOfWeek" => explode(",", $val["days"]),
                        "days_of_week" => explode(",", $val["days"]),
                        "blockedDates" => [],
                        "live" => ($val["status"] == "live" ? true : false),
                        "lastMinute" => false,
                        "shiftIds" => [],
                        "currentCover" => 0,
                        "restaurantId" => $val["id_restaurant_raw"],
                        "id_raw_restaurant" => $val["id_restaurant_raw"],
                        "withdrawn" => false,
                        "filter" => null,
                        "editable" => true,
                        "partnerCode" => null,
                        "price" => null,
                        "specialPrice" => null,
                        "perDayCover" => 0,
                        "dailyCoversLimits" => null,
                        "displayInHGW" => false,
                        "featuredDeal" => 0,
                        "dealTypeIds" => null,
                        "dealTypes" => null,
                        "dealTypeId" => null,
                        "dealType" => null,
                        "promotionImage" => null,
                        "promotionSummary" => null,
                        "promotionTermsAndConditions" => null,
                        "socialSharingTexts" => null,
                        "countryCode" => null,
                        "internalRemarks" => null,
                        "restaurant" => $res,
                        "available" => $available,
                        "image" => $val4["thumbnail"],
                        "images" => $images
                    ];
                    array_push($return, $data);
                    break;
                endif;
            endforeach;
        endif;
    endforeach;
    return $return;
}

function getPromotionsCleaner($id = null) {
    $reservations = selectTable2("reservations");
    $restaurants = selectTable2("restaurants");
    if ($id):
        $promotions = selectTable2("restaurant_promotions", ["id_raw" => $id]);
    else:
        $promotions = selectTable2("restaurant_promotions");
    endif;
    $return = [];
    foreach ($promotions as $key => $val):
        if ($val["status"] == "active" || $val["status"] == "live" && ( $val["date_end"] . "" >= date("Y-m-d") )):
            $available = $val["max_cover"] ? $val["max_cover"] : 50;
            foreach ($reservations as $key3 => $val3):
                if ($val3["id_promotion"] == $val["id_raw"]):
                    $available--;
                endif;
            endforeach;
            foreach ($restaurants as $key4 => $val4):
                if ($val4["id_raw"] == $val["id_restaurant_raw"] && ($val4["status"] == "live" || $val4["status"] == "active")):
                    $res = null;
                    $images = [];
                    $data = [
                        "id" => $val["id_raw"],
                        "id_raw_promotion" => $val["id_raw"],
                        "createdBy" => null,
                        "createdDate" => strtotime($val["created_at"]) * 1000 + (8 * 60 * 60),
                        "lastModifiedBy" => null,
                        "lastModifiedDate" => strtotime($val["updated_at"]) * 1000 + (8 * 60 * 60),
                        "version" => 1,
                        "promotionType" => null,
                        "firstTimeSlotId" => $val["time_slot_start"],
                        "lastTimeSlotId" => $val["time_slot_end"],
                        "start_time" => $val["time_slot_start"],
                        "end_time" => $val["time_slot_end"],
                        "duration" => 100,
                        "maximumCover" => $val["max_cover"],
                        "cover_max" => intval($val["max_cover"]),
                        "maximumPartySize" => $val["max_party"],
                        "party_size_max" => intval($val["max_party"]),
                        "minimumPartySize" => $val["min_party"],
                        "party_size_min" => intval($val["min_party"]),
                        "title" => $val["title"],
                        "description" => $val["description"],
                        "dinerTypes" => [
                            [
                                "id" => "4fcf10c23467c7de36000001",
                                "createdBy" => null,
                                "createdDate" => null,
                                "lastModifiedBy" => null,
                                "lastModifiedDate" => null,
                                "version" => null,
                                "name" => "All",
                                "description" => "This promotion will be available for booking.",
                                "postURL" => "http =>  //localhost =>  2345",
                                "always" => true,
                                "pushLastMinuteOnly" => false
                            ]
                        ],
                        "dinerTypeIds" => [
                            "4fcf10c23467c7de36000001"
                        ],
                        "restaurantStatus" => null,
                        "endTimestamp" => strtotime($val["date_end"]) * 1000 + (8 * 60 * 60),
                        "startTimestamp" => strtotime($val["date_start"]) * 1000 + (8 * 60 * 60),
                        "start_date" => $val["date_start"],
                        "end_date" => $val["date_end"],
                        "durationDays" => null,
                        "daysOfWeek" => explode(",", $val["days"]),
                        "days_of_week" => explode(",", $val["days"]),
                        "blockedDates" => [],
                        "live" => ($val["status"] == "live" ? true : false),
                        "lastMinute" => false,
                        "shiftIds" => [],
                        "currentCover" => 0,
                        "restaurantId" => $val["id_restaurant_raw"],
                        "id_raw_restaurant" => $val["id_restaurant_raw"],
                        "withdrawn" => false,
                        "filter" => null,
                        "editable" => true,
                        "partnerCode" => null,
                        "price" => null,
                        "specialPrice" => null,
                        "perDayCover" => 0,
                        "dailyCoversLimits" => null,
                        "displayInHGW" => false,
                        "featuredDeal" => 0,
                        "dealTypeIds" => null,
                        "dealTypes" => null,
                        "dealTypeId" => null,
                        "dealType" => null,
                        "promotionImage" => null,
                        "promotionSummary" => null,
                        "promotionTermsAndConditions" => null,
                        "socialSharingTexts" => null,
                        "countryCode" => null,
                        "internalRemarks" => null,
                        "restaurant" => $res,
                        "available" => $available,
                        "image" => $val4["thumbnail"],
                        "images" => $images
                    ];
                    array_push($return, $data);
                    break;
                endif;
            endforeach;
        endif;
    endforeach;
    return $return;
}

function getTotalRatings() {
    $restaurants = selectTable2("restaurants");
    $ratings = selectTable2("ratings", ["status" => "live"]);
    $return = [];
    foreach ($restaurants as $key => $val) :
        $count = 0;
        $data = [
            "id_raw_restaurant" => $val["id_raw"],
            "rating" => [
                "taste" => 0,
                "ambiance" => 0,
                "value" => 0,
                "cleanliness" => 0,
                "service" => 0,
                "general" => 0
            ]
        ];
        foreach ($ratings as $key2 => $val2) :
            if ($val2["id_restaurant_raw"] == $val["id_raw"]):
                $data["rating"]["taste"] += $val2["taste"];
                $data["rating"]["ambiance"] += $val2["ambience"];
                $data["rating"]["value"] += $val2["value"];
                $data["rating"]["cleanliness"] += $val2["cleanliness"];
                $data["rating"]["service"] += $val2["service"];
                $data["rating"]["general"] += $val2["general"];
                $count += 1;
            endif;
        endforeach;
        if ($count > 1):
            $data["rating"]["taste"] = round($data["rating"]["taste"] / $count, 2);
            $data["rating"]["ambiance"] = round($data["rating"]["ambience"] / $count, 2);
            $data["rating"]["value"] = round($data["rating"]["value"] / $count, 2);
            $data["rating"]["cleanliness"] = round($data["rating"]["cleanliness"] / $count, 2);
            $data["rating"]["service"] = round($data["rating"]["service"] / $count, 2);
            $data["rating"]["general"] = round($data["rating"]["general"] / $count, 2);
        endif;
        array_push($return, $data);
    endforeach;
    return $return;
}

function getRestaurantsNew($id = null) {
    header('Content-Type: application/json');
    global $database2;
    $temp = null;
    $promotions = getPromotionsClean();
    $ratings = getTotalRatings();
    if ($id):
        $restaurants = selectTable2("restaurants", ["AND" => ["status" => "active", "id_raw" => $id]]);
    else:
        $restaurants = selectTable2("restaurants", ["status" => "active"]);
    endif;
    $restaurant_details = $database2->select("restaurant_details", "*");
    $return = [];
    foreach ($restaurants as $key => $res):
        if ($res["status"] != "pending"):
            $pro = [];
            $rating = [];
            foreach ($ratings as $rat):
                if ($rat["id_raw_restaurant"] == $res["id_raw"]):
                    $rating = $rat["rating"];
                    break;
                endif;
            endforeach;
            $tags = filterDetails($restaurant_details, $res["id_raw"], "tag");
            if ($key > count($restaurants) - 11):
                array_push($tags, "new on seats");
            endif;
            foreach ($promotions as $key5 => $val5):
                if ($val5["restaurantId"] == $res["id_raw"]):
                    array_push($pro, $val5);
                endif;
            endforeach;

            $rating = $output = [
                "id" => $res["id_raw"],
                "createdBy" => null,
                "createdDate" => strtotime($res["created_at"]) * 1000 + (8 * 60 * 60),
                "lastModifiedBy" => null,
                "lastModifiedDate" => strtotime($res["updated_at"]) * 1000 + (8 * 60 * 60),
                "version" => 1,
                "name" => $res["name"],
                "localName" => $res["name"],
                "dressCode" => $res["dress_code"],
                "diningStyle" => $res["dining_style"],
                "cuisines" => null,
                "neighborhood" => $res["neighborhood"],
                "crossStreet" => "",
                "menu" => $res["menu"] ? $res["menu"] : "",
                "price" => $res["price"],
                "website" => $res["website"],
                "hoursOfOperation" => $res["operating_hours"],
                "paymentOptions" => filterDetails($restaurant_details, $res["id_raw"], "payment_option"),
                "acceptWalkins" => $res["has_walkins"] ? true : false,
                "additionalInfo" => [],
                "publicTransit" => "",
                "parking" => $res["has_parking"] ? true : false,
                "parkingDetails" => "",
                "catering" => $res["has_catering"] ? true : false,
                "specialEventAndPromotions" => "",
                "address" => $res["address"],
                "localAddress" => $res["address"],
                "city" => $res["city"],
                "localCity" => $res["city"],
                "country" => $res["country"],
                "postalCode" => $res["postal_code"],
                "specialties" => $res["specialties"],
                "chef" => $res["chef"],
                "description" => $res["description"],
                "phone" => $res["phone"],
                "email" => $res["email"],
                "latitude" => $res["latitude"] ? $res["latitude"] : 0.0,
                "longitude" => $res["longitude"] ? $res["longitude"] : 0.0,
                "images" => filterDetails($restaurant_details, $res["id_raw"], "image"),
                "banners" => filterDetails($restaurant_details, $res["id_raw"], "banner"),
                "publicTransport" => "",
                "corkageDetails" => $res["corkage_details"],
                "eventFacilities" => "",
                "additionalDetails" => filterDetails($restaurant_details, $res["id_raw"], "additional_detail"),
                "fullPhoneNumber" => $res["phone"],
                "featured" => "",
                "featuredDate" => "",
                "gmtMinutesOffset" => -480,
                "groupId" => $res["id_group_raw"],
                "planType" => $res["plan_type"],
                "reservationEmail" => "",
                "facebookImage" => "",
                "tzid" => "",
                "hoiioNumber" => "",
                "ipadAppVersion" => "",
                "logoUrl" => "",
                "chefRestaurant" => "",
                "config" => "",
                "listingUrl" => "",
                "replyTo" => "",
                "checkSize" => "",
                "restaurantSlug" => $res["slug"],
                "ccCheckStartTimestamp" => "",
                "ccCheckEndTimestamp" => "",
                "ccCheckCovers" => "",
                "ccCheckCoversMessage" => "",
                "premiumbooking" => "",
                "accessPoints" => "",
                "street" => "",
                "thumbnail" => $res["thumbnail"],
                "enableNewletter" => "",
                "disabledNoPromotion" => "",
                "extRestaurantId" => "",
                "showPromoField" => "",
                "gmtminutesOffset" => "",
                "rating" => $rating,
                "cuisine" => filterDetails($restaurant_details, $res["id_raw"], "cuisine"),
                "tags" => $tags,
                "occasions" => filterDetails($restaurant_details, $res["id_raw"], "occasion"),
                "promotions" => $pro,
                "status" => $res["status"],
            ];
            array_push($return, $output);
        endif;
    endforeach;

    return json_decode(json_encode($return));
}

function getRestaurantsNewClean($id = null) {
    header('Content-Type: application/json');
    global $database2;
    $temp = null;
    $ratings = getTotalRatings();
    if ($id):
        $restaurants = $database2->select("restaurants", "*", ["id_raw" => $id]);
    else:
        $restaurants = $database2->select("restaurants", "*");
    endif;
    $restaurant_details = $database2->select("restaurant_details", "*");
    $return = [];
    foreach ($restaurants as $key => $res):
        if ($res["status"] != "pending"):
            $pro = [];
            $rating = [];
            foreach ($ratings as $rat):
                if ($rat["id_raw_restaurant"] == $res["id_raw"]):
                    $rating = $rat["rating"];
                    break;
                endif;
            endforeach;
            $tags = filterDetails($restaurant_details, $res["id_raw"], "tag");
            if ($key > count($restaurants) - 11):
                array_push($tags, "new on seats");
            endif;


            $rating = $output = [
                "id" => $res["id_raw"],
                "createdBy" => null,
                "createdDate" => strtotime($res["created_at"]) * 1000 + (8 * 60 * 60),
                "lastModifiedBy" => null,
                "lastModifiedDate" => strtotime($res["updated_at"]) * 1000 + (8 * 60 * 60),
                "version" => 1,
                "name" => $res["name"],
                "localName" => $res["name"],
                "dressCode" => $res["dress_code"],
                "diningStyle" => $res["dining_style"],
                "cuisines" => null,
                "neighborhood" => $res["neighborhood"],
                "crossStreet" => "",
                "menu" => $res["menu"] ? $res["menu"] : "",
                "price" => $res["price"],
                "website" => $res["website"],
                "hoursOfOperation" => $res["operating_hours"],
                "paymentOptions" => filterDetails($restaurant_details, $res["id_raw"], "payment_option"),
                "acceptWalkins" => $res["has_walkins"] ? true : false,
                "additionalInfo" => [],
                "publicTransit" => "",
                "parking" => $res["has_parking"] ? true : false,
                "parkingDetails" => "",
                "catering" => $res["has_catering"] ? true : false,
                "specialEventAndPromotions" => "",
                "address" => $res["address"],
                "localAddress" => $res["address"],
                "city" => $res["city"],
                "localCity" => $res["city"],
                "country" => $res["country"],
                "postalCode" => $res["postal_code"],
                "specialties" => $res["specialties"],
                "chef" => $res["chef"],
                "description" => $res["description"],
                "phone" => $res["phone"],
                "email" => $res["email"],
                "latitude" => $res["latitude"],
                "longitude" => $res["longitude"],
                "images" => filterDetails($restaurant_details, $res["id_raw"], "gallery"),
                "banners" => filterDetails($restaurant_details, $res["id_raw"], "banner"),
                "publicTransport" => "",
                "corkageDetails" => $res["corkage_details"],
                "eventFacilities" => "",
                "additionalDetails" => filterDetails($restaurant_details, $res["id_raw"], "additional_detail"),
                "fullPhoneNumber" => "",
                "featured" => "",
                "featuredDate" => "",
                "gmtMinutesOffset" => -480,
                "groupId" => $res["id_group_raw"],
                "planType" => $res["plan_type"],
                "reservationEmail" => "",
                "facebookImage" => "",
                "tzid" => "",
                "hoiioNumber" => "",
                "ipadAppVersion" => "",
                "logoUrl" => "",
                "chefRestaurant" => "",
                "config" => "",
                "listingUrl" => "",
                "replyTo" => "",
                "checkSize" => "",
                "restaurantSlug" => $res["slug"],
                "ccCheckStartTimestamp" => "",
                "ccCheckEndTimestamp" => "",
                "ccCheckCovers" => "",
                "ccCheckCoversMessage" => "",
                "premiumbooking" => "",
                "accessPoints" => "",
                "street" => "",
                "thumbnail" => $res["thumbnail"],
                "enableNewletter" => "",
                "disabledNoPromotion" => "",
                "extRestaurantId" => "",
                "showPromoField" => "",
                "gmtminutesOffset" => "",
                "rating" => $rating,
                "cuisine" => filterDetails($restaurant_details, $res["id_raw"], "cuisine"),
                "tags" => $tags,
                "occasions" => filterDetails($restaurant_details, $res["id_raw"], "occasion"),
                "promotions" => null,
                "status" => $res["status"],
            ];
            array_push($return, $output);
        endif;
    endforeach;

    return json_decode(json_encode($return));
}

function getUsers($id = null) {
    global $database2;
    if (is_array($id)):
        $users = $database2->select("users", "*", $id);
    else:
        $users = $database2->select("users", "*");
    endif;
    $return = [];
    foreach ($users as $key => $use):
        $bday = $use["birthdate"];
        if ($use["birthdate"] == null):
            $bday = "0000-00-00 00:00:00";
        endif;

        $user = [
            "id" => $use["id"],
            "created_at" => $use["created_at"],
            "updated_at" => $use["updated_at"],
            "status" => "active",
            "type" => "user",
            "id_raw_user" => $use["id_raw"],
            "id_raw_user_temp" => $use["id_raw_temp"],
            "code" => $use["code"],
            "first_name" => $use["first_name"],
            "middle_name" => $use["middle_name"],
            "last_name" => $use["last_name"],
            "designation" => $use["designation"],
            "email" => $use["email"],
            "mobile" => $use["mobile"],
            "birthdate" => strlen($bday) == 10 ? $bday . " 00:00:00" : $bday,
            "gender" => $use["gender"],
            "password" => $use["password"],
            "verified" => $use["verified"] ? 1 : 0,
            "points" => 0,
            "image" => $use["thumbnail"],
            "thumbnail" => $use["thumbnail"],
            "fb_id" => $use["fb_id"],
            "fb_link" => $use["fb_link"],
            "gcm" => $use["gcm"]
        ];
        if ($id && !is_array($id)):
            if ($id . "" == $use["mobile"] || $id . "" == $use["id_raw"] || $id . "" == $use["id"]):
                return $user;
            endif;
        else:
            array_push($return, $user);
        endif;
    endforeach;
    return $return;
}

function getAvailabilities($id, $date = null) {
//    $dates = selectTable2("restaurant_availabilities", ["id_restaurant_raw" => $id]);
//    $res = getTable2("restaurants", ["id_raw" => $id]);
//    if ($res["status"] != "active"):
//        return [];
//    endif;
    $dates0 = selectTable2("restaurant_availabilities", ["id_restaurant_raw" => $id]);
    $dtls = selectTable2("restaurant_availabilities_dtls");
    $dates2 = [];
    $availabilities = [];
    $return = [];
    $date = $date ? $date : substr(currentdatetime(), 0, 10);

    foreach ($dates0 as $key => $val):
        $in = true;
        foreach ($dtls as $key2 => $val2):
            if ($val2["id_availability"] == $val["id"] && $val2["online_reservation"] == "off"):
                $in = false;
                break;
            endif;
        endforeach;
        $in ? array_push($dates2, $val) : null;
    endforeach;

    foreach ($dates2 as $key => $val):
        if ($val["online_reservation"] == "on" && strtolower(date("l", strtotime($date))) == $val["day"]):
            if ((substr(currentdatetime(), 0, 10) == $date && ($val["cutoff_time"] ? date("Hi", strtotime($val["cutoff_time"])) . "" > date("Hi") . "" : true)) || ($date > substr(currentdatetime(), 0, 10))):
                array_push($availabilities, $val);
            endif;
        endif;
    endforeach;
//    var_dump($date);
//    var_dump($availabilities);
//    die;
    foreach ($availabilities as $key => $val):
        $min = date("Hi", strtotime($val["time_slot_start"]));
        $max = date("Hi", strtotime($val["time_slot_end"]));
        $time_slots = [];
        array_push($time_slots, intval($min) . "");
        while ($min < $max):
            if (substr($min, -2) != "45"):
                $min = intval($min) + 15;
            else:
                $min = intval($min) - 45 + 100;
            endif;
            $min .="";
            array_push($time_slots, $min);
        endwhile;
        foreach ($time_slots as $val5):
            if ($date > substr(currentdatetime(), 0, 10) || ($date == substr(currentdatetime(), 0, 10) && $val5 > date("Hi") . "")):
                $data = [
                    "timeSlotId" => $val5,
                    "remainingTimeSlotCapacity" => 50,
                    "remainingShiftCapacity" => 50,
                    "maxNumberOfSeatsForSingleReservation" => $val["max_single"],
                    "minNumberOfSeatsForSingleReservation" => $val["min_single"],
                    "shiftId" => $val["shift"],
                    "cutOffTime" => $val["cutoff_time"],
                    "minPartySizeBooking" => $val["max_party"],
                    "maxPartySizeBooking" => $val["min_party"]
                ];
                array_push($return, $data);
            endif;
        endforeach;
    endforeach;
    return $return;
}

function getAvailabilitiesOriginal($id, $date = null) {
//    $res = getTable2("restaurants", ["id_raw" => $id]);
//    if ($res["status"] != "active"):
//        return [];
//    endif;
    $dates = selectTable2("restaurant_availabilities3", ["id_restaurant_raw" => $id]);
    $availabilities = [];
    $return = [];
    $date = $date ? $date : substr(currentdatetime(), 0, 10);
    foreach ($dates as $key => $val):
        if (strtolower(date("l", strtotime($date))) == $val["day"]):
            array_push($availabilities, $val);
        endif;
    endforeach;

    foreach ($availabilities as $key => $val):
        $data = [
            "timeSlotId" => $val["time_slot_start"],
            "remainingTimeSlotCapacity" => 50,
            "remainingShiftCapacity" => 50,
            "maxNumberOfSeatsForSingleReservation" => $val["max_single"],
            "minNumberOfSeatsForSingleReservation" => $val["min_single"],
            "shiftId" => $val["shift"],
            "cutOffTime" => $val["time_slot_end"],
            "minPartySizeBooking" => $val["max_party"],
            "maxPartySizeBooking" => $val["min_party"]
        ];
        array_push($return, $data);
    endforeach;
    return $return;
}

function getRatings($id = null) {
    $users = getUsers();
    if ($id):
        $ratings = selectTable2("ratings", $id);
    else:
        $ratings = selectTable2("ratings");
    endif;
    $return = [];
    foreach ($ratings as $key => $val):
        $data = $val;
        if ($val["id_user_raw"]):
            $data["id_raw_user"] = $val["id_user_raw"];
        else:
            $id_raw = getUsers($val["id_user"]);
            $data["id_raw_user"] = $id_raw["id_raw_user"];
        endif;
        $data["id_raw_restaurant"] = $val["id_restaurant_raw"];
        $data["id_raw_reservation"] = $val["id_reservation_raw"];
        $data["comments"] = $val["review"];
        array_push($return, $data);
    endforeach;
    return $return;
}

function getReservations($id = null) {
    $users = getUsers();
    $restaurants = (array) getRestaurantsNew();
    $promotions = getPromotionsCleaner();
    $user = [];
    $return = [];
    $pro = null;
    if ($id):
        $reservations = selectTable2("reservations", ["id_raw" => $id]);
    else:
        $reservations = selectTable2("reservations");
    endif;
    foreach ($reservations as $key => $val):
        foreach ($users as $key2 => $val2):
            if ($val2["id_raw_user"] == $val["id_user_raw"]):
                $user = $val2;
                break;
            endif;
        endforeach;
        foreach ($restaurants as $key3 => $val3):
            $val3 = (array) $val3;
            if ($val3["id"] == $val["id_restaurant_raw"]):
                $res = $val3;
                break;
            endif;
        endforeach;

        $pro = null;
        if ($val["id_promotion_raw"] != null):
            foreach ($promotions as $key6 => $val6):
                $val6 = (array) $val6;
                if ($val6["id"] == $val["id_promotion_raw"]):
                    $pro = $val6;
                    break;
                endif;
            endforeach;
        endif;

        $reservation = [
            "id" => $val["id_raw"],
            "createdBy" => null,
            "createdDate" => strtotime($val["created_at"]) * 1000 + (8 * 60 * 60),
            "lastModifiedBy" => null,
            "lastModifiedDate" => strtotime($val["created_at"]) * 1000 + (8 * 60 * 60),
            "version" => 1,
            "dinerId" => $val["id_user_raw"],
            "diner" => [
                "id" => $val2["id_raw_user"],
                "createdBy" => null,
                "createdDate" => strtotime($val2["created_at"]) * 1000 + (8 * 60 * 60),
                "lastModifiedBy" => null,
                "lastModifiedDate" => strtotime($val2["created_at"]) * 1000 + (8 * 60 * 60),
                "version" => 1,
                "firstName" => $val2["first_name"],
                "lastName" => $val2["last_name"],
                "companyName" => null,
                "position" => null,
                "restaurantId" => $val["id_restaurant_raw"],
                "webUserId" => "559e3e24e4b04bf2709b49d0",
                "birthday" => null,
                "anniversary" => null,
                "note" => null,
                "phones" => [
                    [
                        "label" => "Mobile",
                        "value" => substr($val2["mobile"], 3),
                        "countryCode" => "+63",
                        "isPrimary" => true
                    ]
                ],
                "emails" => [
                    [
                        "label" => "Email",
                        "value" => $val2["email"],
                        "isPrimary" => true
                    ]
                ],
                "addresses" => null,
                "tagIds" => null,
                "relationships" => null,
                "allergies" => null,
                "salutation" => "Mr",
                "lastVisitDateTime" => null,
                "primaryMobileNumber" => $val2["mobile"],
                "groupId" => $res["groupId"],
                "otherRestaurantIds" => [],
                "zipCode" => null,
                "status" => "active",
                "walkin" => false,
                "newsletter" => false,
                "unwanted" => false,
                "countReservation" => null
            ],
            "timeSlotId" => $val["time_slot"],
            "note" => $val["remarks"] ? $val["remarks"] : "",
            "tagIds" => null,
            "tables" => null,
            "date" => strtotime($val["date"]) * 1000 + (8 * 60 * 60),
            "mode" => 2,
            "numberOfSeats" => ($val["adult_seats"] ? $val["adult_seats"] : 0) + ($val["child_seats"] ? $val["child_seats"] : 0),
            "status" => ($val["status"] == "active" ? 1 : 4),
            "templateSlotId" => null,
            "overrideTemplateSlotId" => "56527e1de4b0541e61becab7",
            "seatedTime" => null,
            "completedTime" => null,
            "waitingCreatedTime" => null,
            "restaurantId" => $res["id"],
            "restaurant" => $res,
            "restaurantReservationNote" => null,
            "floorplanId" => null,
            "configId" => "5629e3b8e4b0a31f0086f3b0",
            "shiftId" => 3,
            "quotedTime" => null,
            "campaignId" => "organic",
            "pending" => null,
            "verificationKey" => "13966b4c-d94c-4031-abe7-cd248096b848",
            "widgetId" => "54728d3355bda4ff32479560",
            "widget" => [
                "id" => "54728d3355bda4ff32479560",
                "createdBy" => null,
                "createdDate" => null,
                "lastModifiedBy" => null,
                "lastModifiedDate" => 1447659858909,
                "version" => 6,
                "domainName" => "",
                "restaurantId" => "",
                "vendorType" => "SINGLE",
                "chargeable" => true,
                "description" => "",
                "partnerCode" => ""
            ],
            "resetWaitingCreatedTime" => null,
            "resetCompletedTime" => null,
            "resetSeatedTime" => null,
            "hasSlotChanged" => false,
            "modifyingDiner" => false,
            "statusChanged" => false,
            "spendingAmount" => null,
            "turnoverTime" => null,
            "promotion" => $pro,
            "promotionId" => $val["id_promotion_raw"] ? $val["id_promotion_raw"] : null,
            "adultSeats" => $val["adult_seats"] ? $val["adult_seats"] : 0,
            "childSeats" => $val["child_seats"] ? $val["child_seats"] : 0,
            "flexDayConfigId" => null,
            "flexTableIds" => null,
            "flexIsJoinedTable" => null,
            "messageSent" => null,
            "emailSent" => null,
            "paymentTransactionId" => null,
            "staffName" => null,
            "staffId" => null,
            "originalBookedDate" => null,
            "originalTimeslot" => null,
            "historyTimeNotes" => null,
            "extBookingId" => null,
            "extCancelUrl" => null,
            "originalAdult" => null,
            "originalChild" => null,
            "promocodeId" => null,
            "waitingList" => false,
            "hasRating" => false
        ];
        array_push($return, $reservation);
    endforeach;
    return $return;
}

function getReservationsClean($id = null) {
    $users = getUsers();
    $restaurants = (array) getRestaurantsNewClean();
    $promotions = getPromotionsCleaner();
    $user = [];
    $return = [];
    $pro = null;
    if ($id):
        $reservations = selectTable2("reservations", ["id_raw" => $id]);
    else:
        $reservations = selectTable2("reservations");
    endif;
    foreach ($reservations as $key => $val):
        foreach ($users as $key2 => $val2):
            if ($val2["id_raw_user"] == $val["id_user_raw"]):
                $user = $val2;
                break;
            endif;
        endforeach;
        foreach ($restaurants as $key3 => $val3):
            $val3 = (array) $val3;
            if ($val3["id"] == $val["id_restaurant_raw"]):
                $res = $val3;
                break;
            endif;
        endforeach;

        $pro = null;
        if ($val["id_promotion_raw"] != null):
            foreach ($promotions as $key6 => $val6):
                $val6 = (array) $val6;
                if ($val6["id"] == $val["id_promotion_raw"]):
                    $pro = $val6;
                    break;
                endif;
            endforeach;
        endif;

        $reservation = [
            "id" => $val["id_raw"],
            "createdBy" => null,
            "createdDate" => strtotime($val["created_at"]) * 1000 + (8 * 60 * 60),
            "lastModifiedBy" => null,
            "lastModifiedDate" => strtotime($val["created_at"]) * 1000 + (8 * 60 * 60),
            "version" => 1,
            "dinerId" => $val["id_user_raw"],
            "diner" => [
                "id" => $val2["id_raw_user"],
                "createdBy" => null,
                "createdDate" => strtotime($val2["created_at"]) * 1000 + (8 * 60 * 60),
                "lastModifiedBy" => null,
                "lastModifiedDate" => strtotime($val2["created_at"]) * 1000 + (8 * 60 * 60),
                "version" => 1,
                "firstName" => $val2["first_name"],
                "lastName" => $val2["last_name"],
                "companyName" => null,
                "position" => null,
                "restaurantId" => $val["id_restaurant_raw"],
                "webUserId" => "559e3e24e4b04bf2709b49d0",
                "birthday" => null,
                "anniversary" => null,
                "note" => null,
                "phones" => [
                    [
                        "label" => "Mobile",
                        "value" => substr($val2["mobile"], 3),
                        "countryCode" => "+63",
                        "isPrimary" => true
                    ]
                ],
                "emails" => [
                    [
                        "label" => "Email",
                        "value" => $val2["email"],
                        "isPrimary" => true
                    ]
                ],
                "addresses" => null,
                "tagIds" => null,
                "relationships" => null,
                "allergies" => null,
                "salutation" => "Mr",
                "lastVisitDateTime" => null,
                "primaryMobileNumber" => $val2["mobile"],
                "groupId" => $res["groupId"],
                "otherRestaurantIds" => [],
                "zipCode" => null,
                "status" => "active",
                "walkin" => false,
                "newsletter" => false,
                "unwanted" => false,
                "countReservation" => null
            ],
            "timeSlotId" => $val["time_slot"],
            "note" => $val["remarks"] ? $val["remarks"] : "",
            "tagIds" => null,
            "tables" => null,
            "date" => strtotime($val["date"]) * 1000 + (8 * 60 * 60),
            "mode" => 2,
            "numberOfSeats" => ($val["adult_seats"] ? $val["adult_seats"] : 0) + ($val["child_seats"] ? $val["child_seats"] : 0),
            "status" => ($val["status"] == "active" ? 1 : 4),
            "templateSlotId" => null,
            "overrideTemplateSlotId" => "56527e1de4b0541e61becab7",
            "seatedTime" => null,
            "completedTime" => null,
            "waitingCreatedTime" => null,
            "restaurantId" => $res["id"],
            "restaurant" => $res,
            "restaurantReservationNote" => null,
            "floorplanId" => null,
            "configId" => "5629e3b8e4b0a31f0086f3b0",
            "shiftId" => 3,
            "quotedTime" => null,
            "campaignId" => "organic",
            "pending" => null,
            "verificationKey" => "13966b4c-d94c-4031-abe7-cd248096b848",
            "widgetId" => "54728d3355bda4ff32479560",
            "widget" => [
                "id" => "54728d3355bda4ff32479560",
                "createdBy" => null,
                "createdDate" => null,
                "lastModifiedBy" => null,
                "lastModifiedDate" => 1447659858909,
                "version" => 6,
                "domainName" => "",
                "restaurantId" => "",
                "vendorType" => "SINGLE",
                "chargeable" => true,
                "description" => "",
                "partnerCode" => ""
            ],
            "resetWaitingCreatedTime" => null,
            "resetCompletedTime" => null,
            "resetSeatedTime" => null,
            "hasSlotChanged" => false,
            "modifyingDiner" => false,
            "statusChanged" => false,
            "spendingAmount" => null,
            "turnoverTime" => null,
            "promotion" => $pro,
            "promotionId" => $val["id_promotion_raw"] ? $val["id_promotion_raw"] : null,
            "adultSeats" => $val["adult_seats"] ? $val["adult_seats"] : 0,
            "childSeats" => $val["child_seats"] ? $val["child_seats"] : 0,
            "flexDayConfigId" => null,
            "flexTableIds" => null,
            "flexIsJoinedTable" => null,
            "messageSent" => null,
            "emailSent" => null,
            "paymentTransactionId" => null,
            "staffName" => null,
            "staffId" => null,
            "originalBookedDate" => null,
            "originalTimeslot" => null,
            "historyTimeNotes" => null,
            "extBookingId" => null,
            "extCancelUrl" => null,
            "originalAdult" => null,
            "originalChild" => null,
            "promocodeId" => null,
            "waitingList" => false,
            "hasRating" => false
        ];
        array_push($return, $reservation);
    endforeach;
    return $return;
}

function getRand() {
    $rand = substr(md5(sha1(rand(0, 9999999999999999))), 0, 24);
    return $rand;
}

function sendOTP($mobile, $fb_id = null) {
    $code = rand(1000, 9999);
    $data = [
        "code" => "$code",
        "mobile" => "$mobile",
        "status" => "pending"
    ];
    $fb_id ? $data["fb_id"] = $fb_id : null;
    if ($fb_id == null):
        $ds = selectTable2("otps", ["mobile" => $mobile]);
        if ($ds):
            foreach ($ds as $key => $val):
                if ($val["fb_id"] != null):
                    $data["fb_id"] = $val["fb_id"];
                    break;
                endif;
            endforeach;
        endif;
    endif;
    insertTable2("otps", $data);
    $mobile2 = strlen($mobile) == 13 ? substr($mobile, 3) : $mobile;
    curlyGet("http://54.255.167.219/otp/?mobile=" . $mobile2 . "&code=$code");
    return $rand = getRand();
}

function smsCancel($mobile) {
    $mobile2 = strlen($mobile) == 13 ? substr($mobile, 3) : $mobile;
    $message = urlencode("You have successfully cancelled your reservation. Thank you for using SEATS.");
    curlyGet("http://54.255.167.219/seats-cancel/?mobile=" . $mobile2 . "&message=$message");
}

function smsSuccess($mobile, $message) {
    $mobile2 = strlen($mobile) == 13 ? substr($mobile, 3) : $mobile;
    $message = urlencode("" . $message);
    return curlyGet("http://54.255.167.219/seats-cancel/?mobile=" . $mobile2 . "&message=$message");
}

function smsSuccessAdmin($mobile, $message) {
    $mobile2 = strlen($mobile) == 13 ? substr($mobile, 3) : $mobile;
    $message = urlencode("" . $message);
    return curlyGet("http://54.255.167.219/seats-cancel/?mobile=" . $mobile2 . "&message=$message");
}

function confirmOTP($mobile, $code) {
    $opts = getTable2("otps", ["AND" => ["mobile" => $mobile, "code" => $code, "status" => "pending"]]);
    if ($opts):
        if ($opts["fb_id"]):
            $user = getUsers($opts["mobile"]);
            $data["fb_id"] = $opts["fb_id"];
            $data["thumbnail"] = "https://graph.facebook.com/" . $opts["fb_id"] . "/picture?type=normal";
            updateTable2("users", $data, $user["id"]);
        endif;
        deleteTable2("otps", ["mobile" => $mobile]);
        return true;
    else:
        return false;
    endif;
}

function getListDefaults2($type = null, $index = null) {
    $details = selectTable2("details");
    $tags = ["none"];
    foreach (filterLists($details, "tag") as $key => $val):
        array_push($tags, $val);
    endforeach;
    $data = [
        [
            "type" => "tags",
            "list" => $tags
        ],
        [
            "type" => "sort",
            "list" => filterLists($details, "sort")
        ],
        [
            "type" => "dining_style",
            "list" => filterLists($details, "dining_style")
        ],
        [
            "type" => "dress_code",
            "list" => filterLists($details, "dress_code")
        ],
        [
            "type" => "occasions",
            "list" => filterLists($details, "occasion")
        ],
        [
            "type" => "cuisine",
            "list" => filterLists($details, "cuisine")
        ],
        [
            "type" => "payment_options",
            "list" => filterLists($details, "payment_option")
        ],
        [
            "type" => "neighborhood",
            "list" => filterLists($details, "neighborhood")
        ],
        [
            "type" => "additional_details",
            "list" => filterLists($details, "additional_detail")
        ],
        [
            "type" => "price",
            "default" => "150",
            "min" => "0",
            "max" => "5000",
        ],
    ];

    if ($type != null) {
        foreach ($data as $d) {
            if ($d["type"] == $type) {
                $data = $d["list"];
            }
        }
    } else if ($type != null && $index != null) {
        foreach ($data as $d) {
            if ($d["type"] == $type) {
                $data = $d["list"][$index];
            }
        }
    }
    return $data;
}

function array_to_csv_download($array, $filename = "export.csv", $delimiter = ";") {
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');

    // open the "output" stream
    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
    $f = fopen('php://output', 'w');

    foreach ($array as $line) {
        fputcsv($f, $line, $delimiter);
    }
}

function sendMailSuccess($reservation) {
    $res = json_decode(json_encode($reservation));
    $email = $res->diner->emails[0]->value ? $res->diner->emails[0]->value : null;
    if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) :
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "seats.globe@gmail.com";                   // SMTP username
        $mail->Password = "adminseats123";               // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        $mail->SMTPDebug = 2;                                    // TCP port to connect to
        $mail->From = "SEATS";
        $mail->FromName = "SEATS";
        $mail->addAddress($email);     // Add a recipient
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "SEATS " . $res->diner->firstName . " " . $res->diner->lastName . " - new reservation";
        ob_start();
        include linkPage("email/success");
        $mail->Body = ob_get_contents();
        $mail->AltBody = ob_get_contents();
        ob_end_clean();

        if (!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo '<pre>';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo '</pre>';
//            var_dump($mail);
            return null;
        } else {
//            var_dump($mail);
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo 'Message has been sent';
//            die();
            return true;
        }
//    echo 'SENDING EMAIL ALERT';
    else :
//        echo 'Message has been sent';
        return null;
    endif;
}

function sendMailSuccessMerchant($reservation, $email) {
    $res = json_decode(json_encode($reservation));
    $email = $email ? $email : null;
    if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) :
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "seats.globe@gmail.com";                   // SMTP username
        $mail->Password = "adminseats123";               // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
//        $mail->SMTPDebug = 2;                                    // TCP port to connect to
        $mail->From = "SEATS";
        $mail->FromName = "SEATS";
        $mail->addAddress($email);     // Add a recipient
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "SEATS " . $res->diner->firstName . " " . $res->diner->lastName . " - new reservation";
        ob_start();
        include linkPage("email/merchant_success");
        $mail->Body = ob_get_contents();
        $mail->AltBody = ob_get_contents();
        ob_end_clean();

        if (!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo '<pre>';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo '</pre>';
//            var_dump($mail);
            return null;
        } else {
//            var_dump($mail);
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo 'Message has been sent';
//            die();
            return true;
        }
//    echo 'SENDING EMAIL ALERT';
    else :
//        echo 'Message has been sent';
        return null;
    endif;
}

function sendMailModifiedMerchant($reservation, $email) {
    $res = json_decode(json_encode($reservation));
    $email = $email ? $email : null;
    if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) :
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "seats.globe@gmail.com";                   // SMTP username
        $mail->Password = "adminseats123";               // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
//        $mail->SMTPDebug = 2;                                    // TCP port to connect to
        $mail->From = "SEATS";
        $mail->FromName = "SEATS";
        $mail->addAddress($email);     // Add a recipient
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "SEATS " . $res->diner->firstName . " " . $res->diner->lastName . " - modified reservation";
        ob_start();
        include linkPage("email/merchant_modified");
        $mail->Body = ob_get_contents();
        $mail->AltBody = ob_get_contents();
        ob_end_clean();

        if (!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo '<pre>';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo '</pre>';
//            var_dump($mail);
            return null;
        } else {
//            var_dump($mail);
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo 'Message has been sent';
//            die();
            return true;
        }
//    echo 'SENDING EMAIL ALERT';
    else :
//        echo 'Message has been sent';
        return null;
    endif;
}

function sendMailCanceledMerchant($reservation, $email) {
    $res = json_decode(json_encode($reservation));
    $email = $email ? $email : null;
    if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) :
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "seats.globe@gmail.com";                   // SMTP username
        $mail->Password = "adminseats123";               // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
//        $mail->SMTPDebug = 2;                                    // TCP port to connect to
        $mail->From = "SEATS";
        $mail->FromName = "SEATS";
        $mail->addAddress($email);     // Add a recipient
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "SEATS " . $res->diner->firstName . " " . $res->diner->lastName . " - canceled reservation";
        ob_start();
        include linkPage("email/merchant_canceled");
        $mail->Body = ob_get_contents();
        $mail->AltBody = ob_get_contents();
        ob_end_clean();

        if (!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo '<pre>';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo '</pre>';
//            var_dump($mail);
            return null;
        } else {
//            var_dump($mail);
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo 'Message has been sent';
//            die();
            return true;
        }
//    echo 'SENDING EMAIL ALERT';
    else :
//        echo 'Message has been sent';
        return null;
    endif;
}

function sendMailSuccessUser($reservation, $email) {
    $res = json_decode(json_encode($reservation));
    $email = $email ? $email : null;
    if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) :
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "seats.globe@gmail.com";                   // SMTP username
        $mail->Password = "adminseats123";               // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
//        $mail->SMTPDebug = 2;                                    // TCP port to connect to
        $mail->From = "SEATS";
        $mail->FromName = "SEATS";
        $mail->addAddress($email);     // Add a recipient
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "SEATS " . $res->diner->firstName . " " . $res->diner->lastName . " - new reservation";
        ob_start();
        include linkPage("email/user_success");
        $mail->Body = ob_get_contents();
        $mail->AltBody = ob_get_contents();
        ob_end_clean();

        if (!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo '<pre>';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo '</pre>';
//            var_dump($mail);
            return null;
        } else {
//            var_dump($mail);
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo 'Message has been sent';
//            die();
            return true;
        }
//    echo 'SENDING EMAIL ALERT';
    else :
//        echo 'Message has been sent';
        return null;
    endif;
}

function sendMailModifiedUser($reservation, $email) {
    $res = json_decode(json_encode($reservation));
    $email = $email ? $email : null;
    if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) :
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "seats.globe@gmail.com";                   // SMTP username
        $mail->Password = "adminseats123";               // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
//        $mail->SMTPDebug = 2;                                    // TCP port to connect to
        $mail->From = "SEATS";
        $mail->FromName = "SEATS";
        $mail->addAddress($email);     // Add a recipient
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "SEATS " . $res->diner->firstName . " " . $res->diner->lastName . " - modified reservation";
        ob_start();
        include linkPage("email/user_modified");
        $mail->Body = ob_get_contents();
        $mail->AltBody = ob_get_contents();
        ob_end_clean();

        if (!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo '<pre>';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo '</pre>';
//            var_dump($mail);
            return null;
        } else {
//            var_dump($mail);
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo 'Message has been sent';
//            die();
            return true;
        }
//    echo 'SENDING EMAIL ALERT';
    else :
//        echo 'Message has been sent';
        return null;
    endif;
}

function sendMailCanceledUser($reservation, $email) {
    $res = json_decode(json_encode($reservation));
    $email = $email ? $email : null;
    if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) :
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "seats.globe@gmail.com";                   // SMTP username
        $mail->Password = "adminseats123";               // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
//        $mail->SMTPDebug = 2;                                    // TCP port to connect to
        $mail->From = "SEATS";
        $mail->FromName = "SEATS";
        $mail->addAddress($email);     // Add a recipient
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "SEATS " . $res->diner->firstName . " " . $res->diner->lastName . " - canceled reservation";
        ob_start();
        include linkPage("email/user_canceled");
        $mail->Body = ob_get_contents();
        $mail->AltBody = ob_get_contents();
        ob_end_clean();

        if (!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo '<pre>';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo '</pre>';
//            var_dump($mail);
            return null;
        } else {
//            var_dump($mail);
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo 'Message has been sent';
//            die();
            return true;
        }
//    echo 'SENDING EMAIL ALERT';
    else :
//        echo 'Message has been sent';
        return null;
    endif;
}
