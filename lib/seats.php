<?php

function newMongoId() {
    return (new MongoId()) . "";
}

function getRestaurants($id = null) {
    $restaurants_sum = [];
    $list_additional_details = getListDefaults("additional_details");
    $list_payment_options = getListDefaults("payment_options");
    $list_dining_style = getListDefaults("dining_style");
    $list_dress_code = getListDefaults("dress_code");

    $list_occasions = getListDefaults("occasions");
    $list_cuisines = getListDefaults("cuisine");
    $list_tags = selectTable("tags");


    if ($id != null) {
        $response = curlyGet(getInit("api_url") . "tabledb-web/restaurant/{$id}/" . getInit("partner_code") . "/" . getInit("partner_auth_code"));
    } else {
        $response = curlyGet(getInit("api_url") . "tabledb-web/restaurants/featured");
    }
    $restaurants = json_decode($response)->response->data;
    $ratings = selectTable("ratings", ["status" => "approved"]);

    $details = selectTable("restaurants");
    $list_promotions = selectTable("promotions");

    $restaurants_count = count($restaurants);

    foreach ($restaurants as $ind => $restaurant) {

        $cuisine = [];
        $occasions = [];
        $tags = [];
        $promotions = [];

        $data = [];
        $data = [
            "taste" => 0,
            "ambiance" => 0,
            "value" => 0,
            "cleanliness" => 0,
            "service" => 0,
            "general" => 0,
        ];
        $ratings_size = 0;
        foreach ($ratings as $rating) {
            if ($restaurant->id == $rating["id_raw_restaurant"]) {
                $data["taste"] += $rating["taste"];
                $data["ambiance"] += $rating["ambiance"];
                $data["value"] += $rating["taste"];
                $data["cleanliness"] += $rating["cleanliness"];
                $data["service"] += $rating["service"];
                $data["general"] += $rating["general"];
                $ratings_size++;
            }
        }
        if ($ratings_size != 0) {
            $data["taste"] = ($data["taste"] / $ratings_size);
            $data["ambiance"] = ($data["ambiance"] / $ratings_size);
            $data["value"] = ($data["value"] / $ratings_size);
            $data["cleanliness"] = ($data["cleanliness"] / $ratings_size);
            $data["service"] = ($data["service"] / $ratings_size);
            $data["general"] = ($data["general"] / $ratings_size);
        }
        $additionalDetails = [];
        foreach ($restaurant->additionalDetails as $ad) {
            array_push($additionalDetails, $list_additional_details[$ad - 1]);
        }
        $paymentOptions = [];
        foreach ($restaurant->paymentOptions as $ap) {
            array_push($paymentOptions, $list_payment_options[$ap - 1]);
        }


        foreach ($details as $d) {
            if ($d["id_raw_restaurant"] == $restaurant->id) {
                foreach (explode(",", str_replace(" ", "", $d["cuisines"])) as $c) {
                    if($list_cuisines[$c] != null):
                        array_push($cuisine, $list_cuisines[$c]);
                    endif;
                }
                foreach (explode(",", str_replace(" ", "", $d["occasions"])) as $o) {
                    array_push($occasions, $list_occasions[$o]);
                }
                foreach (explode(",", str_replace(" ", "", $d["tags"])) as $t) {
                    foreach ($list_tags as $tag) {
                        if ($tag["index"] == $t) {
                            array_push($tags, $tag["name"]);
                        }
                    }
                }
            }
        }
        foreach ($list_promotions as $lp) {
            if ($lp["id_raw_restaurant"] == $restaurant->id) {
                array_push($promotions, $lp);
            }
        }

        ###################################################
        #YOLO##############################################
        ###################################################

        if ($data["general"] != 0):
            array_push($tags, "most popular");
        endif;
        if (($restaurants_count - 11) < $ind):
            array_push($tags, "new on seats");
        endif;

        ###################################################

        $restaurant->rating = $data;
        $restaurant->paymentOptions = $paymentOptions;
        $restaurant->additionalDetails = $additionalDetails;
        $restaurant->diningStyle ? $restaurant->diningStyle = $list_dining_style[$restaurant->diningStyle - 1] : null;
        $restaurant->dressCode ? $restaurant->dressCode = $list_dress_code[$restaurant->dressCode - 1] : null;

        $restaurant->cuisine = $cuisine;
        $restaurant->tags = $tags;
        $restaurant->occasions = $occasions;
        $restaurant->promotions = $promotions;


        array_push($restaurants_sum, $restaurant);
    }

    return json_decode(json_encode($restaurants_sum));
}

function getRestaurantsCache($id = null) {
    $old = json_decode(file_get_contents("./public/seats/restaurants.json"))->response;
    $time_old = date('Y-m-d H:i:s', strtotime($old->update . ' + 1 minute'));
    $time_now = currentdatetime();
    if ($time_now > $time_old) {
        $restaurants = getRestaurants();
        $myfile = fopen("./public/seats/restaurants.json", "w") or die("Unable to open file!");
        $txt = json_encode([
            "response" => [
                "status" => "200",
                "update" => currentdatetime(),
                "data" => $restaurants
            ]
        ]);
        fwrite($myfile, $txt);
        fclose($myfile);
    }
    return json_decode(file_get_contents("./public/seats/restaurants.json"))->response->data;
}

function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
    $earth_radius = 6371;

    $dLat = deg2rad($latitude2 - $latitude1);
    $dLon = deg2rad($longitude2 - $longitude1);

    $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
    $c = 2 * asin(sqrt($a));
    $d = $earth_radius * $c;

    return $d * 1000;
}

function getListDefaults($type = null, $index = null) {
    $tags = selectTable("tags");
    $tag_list = [];
    $tag_list[0] = "none";
    foreach ($tags as $t) {
        if($t["status"] == "active"){
            $tag_list[$t["index"]] = $t["name"];
        }
    }
    $data = [
        [
            "type" => "tags",
            "list" => $tag_list
        ],
        [
            "type" => "sort",
            "list" => [
                "0" => "Most Relevant",
                "1" => "Price : Ascending",
                "2" => "Price : Descending",
                "3" => "Alphabetical : Ascending",
                "4" => "Alphabetical : Descending",
                "5" => "Ratings : Ascending",
                "6" => "Ratings : Descending"
            ]
        ],
        [
            "type" => "dining_style",
            "list" => [
                "0" => "Casual",
                "1" => "Casual Elegant",
                "2" => "Fine Dining",
            ]
        ],
        [
            "type" => "dress_code",
            "list" => [
                "0" => "Casual",
                "1" => "Smart Casual",
                "2" => "Jacket Required",
                "3" => "Jacket and Tie Required",
                "4" => "Formal",
            ]
        ],
        [
            "type" => "occasions",
            "list" => [
                "0" => "Cocktails",
                "1" => "Group Dining",
                "2" => "Nightlife",
                "3" => "Romantic",
                "4" => "Buffet",
                "5" => "After Work",
                "6" => "BBQ",
                "7" => "Brunch",
                "8" => "Wine",
                "9" => "Lunch ",
                "10" => "Breakfast",
                "11" => "Ala Carte",
                "12" => "Birthday",
            ]
        ],
        [
            "type" => "cuisine",
            "list" => [
                "0" => "Swiss",
                "1" => "Thai",
                "2" => "Celebrations",
                "3" => "Vietnamese",
                "4" => "Western",
                "5" => "Cantonese",
                "6" => "Filipino",
                "7" => "Australian",
                "8" => "Desserts",
                "9" => "Singaporean",
                "10" => "Globe",
                "11" => "Turkish",
                "12" => "Spanish",
                "13" => "Beer",
                "14" => "Continental",
                "15" => "Vietnamese",
                "16" => "Mongolian",
                "17" => "Burgers",
                "18" => "Indian",
                "19" => "Breakfast",
                "20" => "Wine",
                "21" => "Lunch",
                "22" => "Pizza",
                "23" => "Lunch",
                "24" => "Scandinavian",
                "25" => "Comfort Food",
                "26" => "International",
                "27" => "American",
                "28" => "Japanese",
                "29" => "Brunch",
                "30" => "Yugoslavian",
                "31" => "English",
                "32" => "Greek",
                "33" => "Dimsum",
                "34" => "Korean",
                "35" => "Barbecue",
                "36" => "Dinner",
                "37" => "Fusion",
                "38" => "Buffet",
                "39" => "Traditional",
                "40" => "French",
                "41" => "Seafood",
                "42" => "Steak",
                "43" => "Malaysian",
                "44" => "Shanghai",
                "45" => "Cocktails",
                "46" => "Noodles",
                "47" => "Persian",
                "48" => "Italian",
                "49" => "Portuguese",
                "50" => "Modern",
                "51" => "Pastries",
                "52" => "Chinese",
                "53" => "Pasta",
                "54" => "Brazilian",
                "55" => "Sushi",
                "56" => "Salcedo Village",
                "57" => "Mexican",
                "58" => "Coffee",
                "59" => "Family Friendly",
                "60" => "African",
                "61" => "Caribbean",
                "62" => "Asian",
                "63" => "European",
            ]
        ],
        [
            "type" => "payment_options",
            "list" => [
                "0" => "Cash",
                "1" => "Visa",
                "2" => "MasterCard",
                "3" => "JCB",
                "4" => "American Express",
                "5" => "Cash on Delivery",
                "6" => "Debit",
                "7" => "NETS",
                "8" => "UnionPay",
                "9" => "Other",
                "10" => "Diners Club",
            ]
        ],
        [
            "type" => "neighborhood",
            "list" => [
                "0" => "Ortigas Center",
                "1" => "Greenhills",
                "2" => "Makati",
                "3" => "Bonifacio Global City",
                "4" => "Salcedo Village",
                "5" => "Legaspi Village",
                "6" => "Paranaque",
                "7" => "San Juan",
                "8" => "Quezon City",
                "9" => "Rockwell",
                "10" => "Mall of Asia Complex",
                "11" => "Alabang"
            ]
        ],
        [
            "type" => "additional_details",
            "list" => [
                "0" => "Alfresco",
                "1" => "Bar",
                "2" => "Child Friendly",
                "3" => "Catering",
                "4" => "Corporate Events",
                "5" => "Delivery",
                "6" => "Free Wi-Fi",
                "7" => "Full Bar",
                "8" => "Gluten-Free Menu",
                "9" => "Large Groups",
                "10" => "Live Music",
                "11" => "Friendly",
                "12" => "Private Rooms",
                "13" => "Scenic View",
                "14" => "Smoking Area",
                "15" => "Non-smoking Restaurant",
                "16" => "Takeaway",
                "17" => "Waterfront",
                "18" => "Weddings",
                "19" => "Weekend Brunch",
                "20" => "Wheelchair Friendly",
                "21" => "Wine List",
            ]
        ],
        [
            "type" => "price",
            "default" => "150",
            "min" => "0",
            "max" => "5000",
        ],
    ];

    if ($type != null) {
        foreach ($data as $d) {
            if ($d["type"] == $type) {
                $data = $d["list"];
            }
        }
    } else if ($type != null && $index != null) {
        foreach ($data as $d) {
            if ($d["type"] == $type) {
                $data = $d["list"][$index];
            }
        }
    }
    return $data;
}
