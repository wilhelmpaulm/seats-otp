<?php
//stopSession();
//session_unset();
//session_start();
//session_destroy();
//session_start();

require_once('lib/facebook-php-sdk-v4-4.0-dev/autoload.php');
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/HttpClients/FacebookHttpable.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/HttpClients/FacebookCurl.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/Entities/AccessToken.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/Entities/SignedRequest.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookSession.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookRedirectLoginHelper.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookRequest.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookResponse.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookSDKException.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookRequestException.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookOtherException.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookAuthorizationException.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/GraphObject.php' );
require_once( 'lib/facebook-php-sdk-v4-4.0-dev/src/Facebook/GraphSessionInfo.php' );

use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\Entities\AccessToken;
use Facebook\Entities\SignedRequest;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookOtherException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;

// init app with app id (APPID) and secret (SECRET)
//FacebookSession::setDefaultApplication('773438402693854', 'db38ded49e58a1d071fc8a2294be4ff4');
FacebookSession::setDefaultApplication('1379749325679871', 'ed3b4732665edb007559817969bb3c88');
$helper = new FacebookRedirectLoginHelper('http://' . getInit('localhost') . '/register/fb');


try {
    $session = $helper->getSessionFromRedirect();
    var_dump($session);
} catch (FacebookRequestException $ex) {
    var_dump($ex);
    die();
} catch (Exception $ex) {
    var_dump($ex);
    die();
}

if (isset($_REQUEST['error'])) {
    sendTo("login");
}

if (isset($session)) {

    $request = new FacebookRequest($session, 'GET', '/me');
    $response = $request->execute();
    $graphObject = $response->getGraphObject();

    $fbData = array();
    $fbData['fb_id'] = $graphObject->getProperty('id');
    $fbData['email'] = $graphObject->getProperty('email');
    $fbData['sex'] = $graphObject->getProperty('gender');
    $fbData['first_name'] = $graphObject->getProperty('first_name');
    $fbData['last_name'] = $graphObject->getProperty('last_name');
    $fbData['fb_link'] = $graphObject->getProperty('link');
    $fbData['fb_locale'] = $graphObject->getProperty('locale');
    $fbData['fb_timezone'] = $graphObject->getProperty('timezone');
    $fbData['fb_last_update'] = $graphObject->getProperty('updated_time');
    //    $fbData['fb_name'] = $graphObject->getProperty('name');
    //https://graph.facebook.com/{UID}/picture
    $user = "";
    $user = getTable("users", ["email" => $fbData["email"]]);  
    if ($user == false) {
        $user = [
            "fb_id" => $graphObject->getProperty('id'),
            "username" => $graphObject->getProperty('id'),
            "email" => $graphObject->getProperty('email'),
            "gender" => $graphObject->getProperty('gender'),
            "first_name" => $graphObject->getProperty('first_name'),
            "last_name" => $graphObject->getProperty('last_name'),
        ];
        
        $insert  = insertTable("users", $user);
        toSession("user", $user);
        sendTo("me");
    }else{
        toSession("user", $user);
        sendTo("me");
    }
} else {
    
    header("Location: " . $helper->getLoginUrl(array('email', 'user_friends',  'public_profile')));
//    header("Location: " . $helper->getLoginUrl(array('email', 'user_friends',  'public_profile')));
}


