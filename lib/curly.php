<?php

function curly() {
    $url = 'http://domain.com/get-post.php';
    $fields = array(
        'lname' => urlencode($last_name),
        'fname' => urlencode($first_name),
        'title' => urlencode($title),
        'company' => urlencode($institution),
        'age' => urlencode($age),
        'email' => urlencode($email),
        'phone' => urlencode($phone)
    );

//url-ify the data for the POST
    foreach ($fields as $key => $value) {
        $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');

//open connection
    $ch = curl_init();

//set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

//execute post
    $result = curl_exec($ch);

//close connection
    curl_close($ch);
}

function curlyPost($url, $data = []) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept-Type: application/json'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    if ($data != null) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    $response = curl_exec($ch);
    if (!$response) {
        return false;
    }
    return $response;
}

function curlyPut($url, $data = []) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept-Type: application/json'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    if ($data != null) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }

    $response = curl_exec($ch);
    if (!$response) {
        return false;
    }
    return $response;
}

function curlyGet($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept-Type: application/json'));
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

    $response = curl_exec($ch);
    if (!$response) {
        return false;
    }
    return $response;
}

function curlyDelete($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept-Type: application/json'));
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

    $response = curl_exec($ch);
    if (!$response) {
        return false;
    }
    return $response;
}
