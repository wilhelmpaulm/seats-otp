<?php

function getSocial($name = null, $url = null) {
    $list = ["fb", "twitter", "likes", "shares", "tweets"];
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if ($url == null) {
        $url = $actual_link;
    }
//        $url = $actual_link;
//    $url = "https://www.facebook.com/";
//    $url = "http://128.199.208.242:1234/me/events/";
    $list = [
        "url" => $url,
        "tweets" => 0,
        "shares" => 0,
        "likes" => 0,
        "linkedin" => 0,
        "google" => 0,
    ];
    $count = 0;
    switch ($name):

        case "tweets":
            $json_string = file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);
            $json = json_decode($json_string, true);
            $count = intval($json['count']);
            break;
        case "shares":
            $fql = "SELECT share_count";
            $fql .= " FROM link_stat WHERE url = '$url'";
            $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);
            $response = file_get_contents($fqlURL);
            $count = 0;
            if (json_decode($response)) {
                $count = intval(json_decode($response)[0]->share_count);
            }
            break;
        case "likes":
            $fql = "SELECT like_count";
            $fql .= " FROM link_stat WHERE url = '$url'";
            $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);
            $response = file_get_contents($fqlURL);
            $count = intval(json_decode($response)[0]->like_count);
            break;
        case "linkedin":
            $json_string = file_get_contents("http://www.linkedin.com/countserv/count/share?url=$url&format=json");
            $json = json_decode($json_string, true);
            $count = intval($json['count']);
            break;

        default :
            $json_string = file_get_contents("http://urls.api.twitter.com/1/urls/count.json?url='" . $url . "'");
            $json = json_decode($json_string, true);
            $list["tweets"] = intval($json['count']);
            $fql = "SELECT like_count, share_count";
            $fql .= " FROM link_stat WHERE url = '$url'";
            $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);
            $response = file_get_contents($fqlURL);
            $list["likes"] = 0;
            $list["shares"] = 0;
            if (json_decode($response)) {
                $list["likes"] = intval(json_decode($response)[0]->like_count);
                $list["shares"] = intval(json_decode($response)[0]->share_count);
            }

            break;
    endswitch;


    if ($name == null):
        return $list;
    else:
        return $count;
    endif;
}

function getJsonSocial($name = null, $url = null) {
    $list = ["fb", "twitter", "likes", "shares", "tweets"];
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if ($url == null) {
        $url = $actual_link;
    }
//        $url = $actual_link;
//    $url = "https://www.facebook.com/";
//    $url = "http://128.199.208.242:1234/me/events/";
    $list = [
        ["tweets", 0],
        ["shares", 0],
        ["likes", 0],
        ["others", 1],
    ];
    $count = 0;
    switch ($name):

        case "tweets":
            $json_string = file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);
            $json = json_decode($json_string, true);
            $count = intval($json['count']);
            break;
        case "shares":
            $fql = "SELECT share_count";
            $fql .= " FROM link_stat WHERE url = '$url'";
            $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);
            $response = file_get_contents($fqlURL);
            $count = 0;
            if (json_decode($response)) {
                $count = intval(json_decode($response)[0]->share_count);
            }
            break;
        case "likes":
            $fql = "SELECT like_count";
            $fql .= " FROM link_stat WHERE url = '$url'";
            $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);
            $response = file_get_contents($fqlURL);
            $count = intval(json_decode($response)[0]->like_count);
            break;
        case "linkedin":
            $json_string = file_get_contents("http://www.linkedin.com/countserv/count/share?url=$url&format=json");
            $json = json_decode($json_string, true);
            $count = intval($json['count']);
            break;

        default :
            $json_string = file_get_contents("http://urls.api.twitter.com/1/urls/count.json?url='" . $url . "'");
            $json = json_decode($json_string, true);
            $list[0][1] = intval($json['count']);
            $fql = "SELECT like_count, share_count";
            $fql .= " FROM link_stat WHERE url = '$url'";
            $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);
            $response = file_get_contents($fqlURL);
            if (json_decode($response)) {
                $list[1][1] = intval(json_decode($response)[0]->like_count);
                $list[2][1] = intval(json_decode($response)[0]->share_count);
            }

            break;
    endswitch;


    if ($name == null):
        return json_encode($list);
    else:
        return json_encode($count);
    endif;
}
