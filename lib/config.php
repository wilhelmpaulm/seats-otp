<?php

global $database;
global $database2;

$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => getInit("db_name"),
    'server' => getInit("db_host"),
    'username' => getInit("db_username"),
    'password' => getInit("db_password"),
    'charset' => 'utf8',
        ]);


$database2 = new medoo([
    'database_type' => 'mysql',
    'database_name' => getInit("db_name2"),
    'server' => getInit("db_host"),
    'username' => getInit("db_username"),
    'password' => getInit("db_password"),
    'charset' => 'utf8',
        ]);

function getInit($id = null) {
//    $status = "offline";
//    $status = "online";
    $status = "prod";
    $init = [];
    if ($status == "offline") {
        $init = [
            "status" => "offline",
            "errors" => "on",
            "db_host" => "localhost",
            "db_name" => "seats",
            "db_name2" => "seats2",
            "db_username" => "root",
            "db_password" => "",
            "queue_url" => "localhost:7778",
            "api_url" => "http://webapi.seats.com.ph/",
            "partner_code" => "seats",
            "partner_auth_code" => "C01CF784-794D-4CAA-A4C3-37AE69E631E7",
            "widget_id" => "54728d3355bda4ff32479560",
            "gcm_api_key" => "AIzaSyBJj0h4iUv9Kc0haPR92BLXqMteChGnd54",
        ];
    } else if ($status == "online") {
        $init = [
            "status" => "online",
            "errors" => "off",
             "db_host" => "52.76.78.23",
            "db_name" => "seats",
            "db_name2" => "seats",
            "db_username" => "root",
            "db_password" => "p@ssword",
            "status" => "online",
            "queue_url" => "localhost:7778",
            "api_url" => "http://webapi.seats.com.ph/",
//            "api_url" => "http://stagingapi.seats.com.ph/",
            "partner_code" => "seats",
            "partner_auth_code" => "C01CF784-794D-4CAA-A4C3-37AE69E631E7",
            "widget_id" => "54728d3355bda4ff32479560",
            "gcm_api_key" => "AIzaSyBJj0h4iUv9Kc0haPR92BLXqMteChGnd54",
        ];
    } else if ($status == "prod") {
        $init = [
            "status" => "online",
            "errors" => "off",
            "db_host" => "54.169.59.66",
            "db_name" => "seats",
            "db_name2" => "seats_prod",
            "db_username" => "root",
            "db_password" => "p@ssword",
            "status" => "online",
            "queue_url" => "localhost:7778",
            "api_url" => "http://webapi.seats.com.ph/",
//            "api_url" => "http://stagingapi.seats.com.ph/",
            "partner_code" => "seats",
            "partner_auth_code" => "C01CF784-794D-4CAA-A4C3-37AE69E631E7",
            "widget_id" => "54728d3355bda4ff32479560",
            "gcm_api_key" => "AIzaSyBJj0h4iUv9Kc0haPR92BLXqMteChGnd54",
        ];
    } else {
        $init = [
            "status" => "offline",
            "localhost" => "localhost",
            "dir" => $_SERVER['DOCUMENT_ROOT'],
            "disquis" => "wilhelmpaulm-test",
            "errors" => "on",
        ];
    }
    $init["localhost"] = $_SERVER["HTTP_HOST"];
    $init["dir"] = $_SERVER['DOCUMENT_ROOT'];

    if ($id == null) {
        return $init;
    } else {
        return $init[$id];
    }
}
