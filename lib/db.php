<?php

function selectTable($table, $data = null) {
    global $database;

    if ($data == null) {
        $data = $database->select($table, "*");
    } else {
        $data = $database->select($table, "*", $data);
    }
    if (getInit("status") == "errors") {
        var_dump($database->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function deleteTable($table, $data) {
    global $database;

    if (is_array($data)) {
        $data = $database->delete($table, $data);
    } else {
        $data = $database->delete($table, ["id" => $data]);
    }
    if (getInit("status") == "errors") {
        var_dump($database->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function getTable($table, $data) {
    global $database;
    try {
        if (is_array($data)) {
            $data = $database->get($table, "*", $data);
        } else {
            $data = $database->get($table, "*", ["id" => $data]);
        }
    } catch (Exception $exc) {
//        echo $exc->getTraceAsString();
    }
    if (getInit("status") == "errors") {
        var_dump($database->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function insertTable($table, $data) {
    global $database;
    $data = $database->insert($table, $data);
    if (getInit("status") == "errors") {
        var_dump($database->error());
    }
//    die();
    if (!isset($data)) {
        return null;
    } else {
        return $data;
    }
}

function updateTable($table, $data, $id) {
    global $database;
//    $data["date_updated"] = currentdatetime();
    if (is_array($data) && !is_array($id)) {
        $data = $database->update($table, $data, ["id" => $id]);
    } else if (is_array($data) && is_array($id)) {
        $data = $database->update($table, $data, $id);
    } else {
        $data = "invalid parameters";
    }
    if (getInit("status") == "errors") {
        var_dump($database->error());
    }
//    die();
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function selectTable2($table, $data = null) {
    global $database2;

    if ($data == null) {
        $data = $database2->select($table, "*");
    } else {
        $data = $database2->select($table, "*", $data);
    }
    if (getInit("status") == "errors") {
        var_dump($database2->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function deleteTable2($table, $data) {
    global $database2;

    if (is_array($data)) {
        $data = $database2->delete($table, $data);
    } else {
        $data = $database2->delete($table, ["id" => $data]);
    }
    if (getInit("status") == "errors") {
        var_dump($database2->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function getTable2($table, $data) {
    global $database2;
    try {
        if (is_array($data)) {
            $data = $database2->get($table, "*", $data);
        } else {
            $data = $database2->get($table, "*", ["id" => $data]);
        }
    } catch (Exception $exc) {
//        echo $exc->getTraceAsString();
    }
    if (getInit("status") == "errors") {
        var_dump($database2->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function insertTable2($table, $data) {
    global $database2;
    $data = $database2->insert($table, $data);
    if (getInit("status") == "errors") {
        var_dump($database2->error());
    }
//    die();
    if (!isset($data)) {
        return null;
    } else {
        return $data;
    }
}

function updateTable2($table, $data, $id) {
    global $database2;
//    $data["date_updated"] = currentdatetime();
    if (is_array($data) && !is_array($id)) {
        $data = $database2->update($table, $data, ["id" => $id]);
    } else if (is_array($data) && is_array($id)) {
        $data = $database2->update($table, $data, $id);
    } else {
        $data = "invalid parameters";
    }
    if (getInit("status") == "errors") {
        var_dump($database2->error());
    }
//    die();
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}
