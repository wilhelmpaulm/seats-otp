<?php

function gate($type = "login") {
    $u = user();
    $val = true;
    if (empty($u)) {
        $val = false;
    }
    if ($val) {
        return $val;
    } else {
        sessionStop();
        sendTo("admin/login");
    }
}

function flash($name){
    $var = session($name);
    session($name, "unset");
    return $var;
}
function hashString($str) {
    return md5(sha1($str));
}

function htmlClean($data) {
    return htmlentities(htmlspecialchars(htmlentities(htmlspecialchars($data))));
}

function htmlUnclean($data) {
    return html_entity_decode(htmlspecialchars_decode(html_entity_decode(htmlspecialchars_decode($data))));
}

function getUser($key = null) {
    $user = session("user");
    if (isset($user)) {
        if ($key == null) {
            return $user;
        } else {
            if (isset($user[$key])) {
                return $user[$key];
            } else {
                return null;
            }
        }
    } else {
        return null;
    }
}

//Explicit Commands
function info() {
    global $database;
    print_r($database->info());
}

function currentdatetime($query = null) {
    date_default_timezone_set('Asia/Manila');
    if ($query == null) {
        return date("Y-m-d H:i:s");
    } else {
        $date = new DateTime($query);
        return $date->format('Y-m-d H:i:s');
    }
}

function allowOrDie($data) {
    $user = fromSession("user");
    if ($data == "user") {
        if ($user == null) {
            sendTo("logout");
        }
    } else if (!in_array($user["position"], $data)) {
        sendTo("logout");
    }
}

function linkTo($data) {
    if ($data == "back") {
        return $_SERVER['HTTP_REFERER'];
    } else {
        return "http://" . getInit('localhost') . "/" . $data;
    }
}

function sendTo($data) {
    if ($data == "back") {
        echo "<script>location.href = document.referrer;</script>";
//        echo "<script>history.back();</script>";
        die();
    } else {
        header("Location: http://" . getInit('localhost') . "/" . $data);
    }
    die();
}

function getPage($data) {
    $dir = $_SERVER['DOCUMENT_ROOT'];
    include $dir . "/view/" . $data . ".php";
}

function linkPage($data, $php = true) {
    $dir = $_SERVER['DOCUMENT_ROOT'];
    if ($php) {
        return $dir . "/view/" . $data . ".php";
    } else {
        return $dir . "/view/" . $data;
    }
}

function linkPublic($data) {
    $dir = $_SERVER['DOCUMENT_ROOT'];
    return "http://" . getInit("localhost") . "/public/" . $data;
}

//////////////////////////
// ADMIN FUNCTIONS
//////////////////////////

function randomKey() {
    $key = "";
    $salt = rand(100000, 999999);
    $x = 0;
    while ($x < 1) {
        $key = md5(sha1(crypt($key, $salt)));
        $x++;
    }
    return $key;
}

//////////////////////////
// HELPER FUNCTIONS
//////////////////////////

function session($name = null, $data = null) {
    if (session_status() == 1) :
        session_start();
    endif;
    if ($name != null && $data == "unset"):
        $_SESSION[$name] = null;
        return $_SESSION[$name];
    endif;
    if ($name != null && $data != null):
        $_SESSION[$name] = $data;
        return $_SESSION[$name];
    elseif (isset($name)):
        if (is_array($name) && !empty($name)):
            foreach ($name as $key => $value) :
                $_SESSION[$key] = $value;
            endforeach;
            return $_SESSION;
        elseif ($name != null && $data == null):
            if (array_key_exists($name, $_SESSION)) :
                return $_SESSION[$name];
            else:
                return null;
            endif;
        else:
            return $_SESSION;
        endif;
    else:
        return $_SESSION;
    endif;
}

function user($key = null) {
    if (session_status() == 1) :
        session_start();
    endif;
    if ($key == null):
        return session("user");
    else:
        return session("user")[$key];
    endif;
}

function sessionStop() {
    session(["user", null]);
    session_cache_expire();
    if (session_status() != 1) :
        session_destroy();
    endif;
}

function getGet($key = null) {
    if (isset($_GET)) {
        $g = $_GET;
        if ($key != null) {
            if (isset($_GET[$key])) {
                return $g[$key];
            } else {
                return null;
            }
        } else {
            return $g;
        }
    } else {
        return [];
    }
}

function getPost($key = null) {
    if (isset($_POST)) {
        $p = $_POST;
        if ($key != null) {
            if (isset($_POST[$key])) {
                return $p[$key];
            } else {
                return null;
            }
        } else {
            return $p;
        }
    } else {
        return [];
    }
}

function fileUpload($dir, $file, $id) {
    $ext = explode('.', $file['name']);
//    var_dump($ext);
//    die();

    $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/public/" . $dir;
    $salt = randomKey(1000, 9999);
    $name = md5(rand(10000, 20000));

    $filename = $name . '.' . $ext[1];
    $location = $file["tmp_name"];

    $uploadfile = $target_dir . $filename;
//    $uploadfile = $target_dir . $id . "_" . $salt . "_" . basename($file["name"]);
//    $uploadfile = $target_dir . $name;
//    $fileType = pathinfo($uploadfile, PATHINFO_EXTENSION);
    try {
//        if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
        if (move_uploaded_file($location, $uploadfile)) {
//            echo "File is valid, and was successfully uploaded.\n";
        } else {
//            echo "Possible file upload attack!\n";
        }
    } catch (Exception $exc) {
        print_r($_FILES);
//        echo $exc->getTraceAsString();
    }

    return $filename;
}

function upload($name, $folder = "", $filename = null) {
    $target_dir = "public/" . $folder;
    $target_file = $target_dir . basename($_FILES["$name"]["name"]);
    $uploadOk = 1;
//    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
//    if (isset($_POST["submit"])) {
//        $check = getimagesize($_FILES["$name"]["tmp_name"]);
//        if ($check !== false) {
//            echo "File is an image - " . $check["mime"] . ".";
//            $uploadOk = 1;
//        } else {
//            echo "File is not an image.";
//            $uploadOk = 0;
//        }
//    }
// Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
// Check file size
    if ($_FILES["$name"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
//    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
//        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
//        $uploadOk = 0;
//    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        if ($filename != null) {
            $ext = explode('.', $_FILES["$name"]['name']);
            $filename = $filename . '.' . $ext[1];
            $return = $filename;
            $filename = $target_dir . $filename;
            $target_file = $filename;
//            var_dump($ext);
        }
        if (move_uploaded_file($_FILES["$name"]["tmp_name"], $target_file)) {
            echo "The file " . basename($_FILES["$name"]["name"]) . " has been uploaded.";
            return $return;
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}

function url() {
    return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

function diverse_array($vector) {
    $result = array();
    foreach ($vector as $key1 => $value1)
        foreach ($value1 as $key2 => $value2)
            $result[$key2][$key1] = $value2;
    return $result;
}

function dateFormat($date) {
    return date_format(date_create($date), "Y-m-d");
}

function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100), array(11, 12, 13))) {
        switch ($num % 10) {
            // Handle 1st, 2nd, 3rd
            case 1: return $num . 'st';
            case 2: return $num . 'nd';
            case 3: return $num . 'rd';
        }
    }
    return $num . 'th';
}

#, most popular, big groups, buffet, cafe,lunch